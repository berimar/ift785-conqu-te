package jfxtras.internal.scene.control.skin.agenda.base24hour;

import ca.usherbrooke.domus.conq.commons.fx.util.FAGlyphs;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import jfxtras.scene.control.agenda.Agenda;
import org.controlsfx.glyphfont.FontAwesome;

/**
 * date 13/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class DeleteAppointmentButton extends Button {
    private final Agenda.Appointment appointment;
    private final LayoutHelp layoutHelp;

    public DeleteAppointmentButton(Agenda.Appointment appointment, LayoutHelp layoutHelp){
        this.appointment = appointment;
        this.layoutHelp = layoutHelp;
        layoutHelp.setupMouseOverAsBusy(this);
        getStyleClass().addAll("MenuButton","deleteButton");
        setGraphic(FAGlyphs.getIcon(FontAwesome.Glyph.BAN, Color.RED));
        setTooltip(new Tooltip("Supprimer l'activité"));
        setOnAction((event)-> layoutHelp.skinnable.appointments().remove(appointment));
    }
}
