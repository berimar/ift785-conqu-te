package jfxtras.internal.scene.control.skin.agenda.base24hour;

import ca.usherbrooke.domus.conq.commons.fx.util.FAGlyphs;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import jfxtras.scene.control.agenda.Agenda;
import org.controlsfx.glyphfont.FontAwesome;

/**
 * date 13/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class DetailsAppointmentButton extends Button {

    private final Agenda.Appointment appointment;
    private final LayoutHelp layoutHelp;

    public DetailsAppointmentButton(Agenda.Appointment appointment, LayoutHelp layoutHelp){
        this.appointment = appointment;
        this.layoutHelp = layoutHelp;
        layoutHelp.setupMouseOverAsBusy(this);
        getStyleClass().addAll("MenuButton","detailsButton");
        setTooltip(new Tooltip("Voir l'activité"));
        setGraphic(FAGlyphs.getIcon(FontAwesome.Glyph.SEARCH_PLUS, Color.LAWNGREEN));
        setOnAction((event)-> layoutHelp.skinnable.getDetailsAppointmentCallback().accept(appointment));
    }
}
