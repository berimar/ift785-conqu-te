package jfxtras.internal.scene.control.skin.agenda.base24hour;

import javafx.scene.control.Label;
import javafx.scene.shape.Rectangle;

/**
 * For whiting-out an appointment pane
 */
class HistoricalVisualizer extends Label
{
	public HistoricalVisualizer(AppointmentAbstractPane pane) // TBEE: pane must implement something to show its begin and end datetime, so this class knowns how to render itself?
	{
		// 100% overlay the pane
		setMouseTransparent(true);
//		widthProperty().bind(pane.prefWidthProperty());
//		heightProperty().bind(pane.prefHeightProperty());
		setVisible(false);
		getStyleClass().add("History");
        setStyle("-fx-border-color: #0066FF;-fx-border-width: 5px;");

	}
}