package jfxtras.internal.scene.control.skin.agenda.base24hour;

import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import jfxtras.scene.control.agenda.Agenda.Appointment;

/**
 * Used by DayBodyPane
 */
public class AppointmentTaskBodyPane extends AppointmentAbstractTrackedPane {

	public AppointmentTaskBodyPane(Appointment appointment, LayoutHelp layoutHelp) {
		super(appointment.getStartLocalDateTime().toLocalDate(), appointment, layoutHelp);
		// add summary
		if (appointment.getSummary() != null) {
            HBox wrappingHbox = new HBox();
            Text summaryText = createSummaryText();
            wrappingHbox.getChildren().add(summaryText);
			body.setTop(wrappingHbox);
            BorderPane.setAlignment(wrappingHbox, Pos.TOP_CENTER);
		}
	}

	private Text createSummaryText() {
        Text summaryText = new Text(appointment.getSummary().replace("\n", ""));
		summaryText.setWrappingWidth(0);
		summaryText.getStyleClass().add("AppointmentLabel");
		summaryText.wrappingWidthProperty().bind(widthProperty());
		summaryText.setTextAlignment(TextAlignment.CENTER);
		return summaryText;
	}
}
