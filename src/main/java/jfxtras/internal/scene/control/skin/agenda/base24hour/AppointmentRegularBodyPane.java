package jfxtras.internal.scene.control.skin.agenda.base24hour;

import java.time.LocalDate;

import javafx.geometry.Pos;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import jfxtras.scene.control.agenda.Agenda.Appointment;

public class AppointmentRegularBodyPane extends AppointmentAbstractTrackedPane {

    public AppointmentRegularBodyPane(LocalDate localDate, Appointment appointment, LayoutHelp layoutHelp) {
        super(localDate, appointment, layoutHelp);

        // strings
        this.startAsString = layoutHelp.timeDateTimeFormatter.format(this.startDateTime);
        this.endAsString = layoutHelp.timeDateTimeFormatter.format(this.endDateTime);

        // add the duration as text
        Text lTimeText = new Text((firstPaneOfAppointment ? startAsString : "") + " \u2192 " + (lastPaneOfAppointment ? endAsString : ""));
            lTimeText.getStyleClass().add("AppointmentPeriod");
//			lTimeText.setX( layoutHelp.paddingProperty.get() );
//			lTimeText.setY(lTimeText.prefHeight(0));
            lTimeText.setFill(Color.WHITE);
            lTimeText.setTextAlignment(TextAlignment.CENTER);
//			layoutHelp.clip(lTimeText, widthProperty().subtract( layoutHelp.paddingProperty.get() ), heightProperty());
//            lTimeText.rotateProperty().setValue(90);
            footer.getChildren().add(lTimeText);

        // add summary
        Text lSummaryText = new Text(appointment.getSummary());
        lSummaryText.getStyleClass().add("AppointmentLabel");
//        lSummaryText.setX( layoutHelp.paddingProperty.get() );
//        lSummaryText.setY( lTimeText.getY() + layoutHelp.textHeightProperty.get());
        lSummaryText.wrappingWidthProperty().bind(widthProperty().subtract( layoutHelp.paddingProperty.get() ));
//			layoutHelp.clip(lSummaryText, widthProperty(), heightProperty().subtract(layoutHelp.paddingProperty.get()));
        body.setTop(lSummaryText);
        lSummaryText.setTextAlignment(TextAlignment.CENTER);
        body.setAlignment(lSummaryText, Pos.TOP_CENTER);


        // add the duration dragger
        layoutHelp.skinnable.allowResizeProperty().addListener( (observable) -> {
            setupDurationDragger();
        });
        setupDurationDragger();
    }
    private String startAsString;
    private String endAsString;

    /**
     *
     */
    private void setupDurationDragger() {
        if (lastPaneOfAppointment && layoutHelp.skinnable.getAllowResize()) {
            if (durationDragger == null) {
                durationDragger = new DurationDragger(this, appointment, layoutHelp);
            }
            footer.getChildren().add(durationDragger);
        }
        else {
            footer.getChildren().remove(durationDragger);
        }
    }
    private DurationDragger durationDragger = null;
}
