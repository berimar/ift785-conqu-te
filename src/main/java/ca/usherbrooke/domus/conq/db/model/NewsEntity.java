package ca.usherbrooke.domus.conq.db.model;

import ca.usherbrooke.domus.conq.db.DatabaseConfig;
import ca.usherbrooke.domus.conq.db.HibernateDAO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Représente les notifications apparaissant sur la partie droite de l'app.
 * Lorsqu'une personne se connecte, les notifications liées à l'utilisateur sont affichées.
 * <br />
 * date 08/03/15 <br/>
 *
 * @author nabil_diab kondipakmey
 */


@Entity
@Table(name = "News")
@ToString
@Getter
public class NewsEntity {

    public static final HibernateDAO<NewsEntity> DAO = new HibernateDAO<>(NewsEntity.class, DatabaseConfig.MODEL);


    @Getter
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @Column(name = "createDate")
    @Setter
    private String createDate;

    @Column(name = "object")
    @Setter
    private String object;

    @Column(name = "description")
    @Setter
    private String description;

    @Override
    public final int hashCode() {
        return id == null ? 1 : id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsEntity other = (NewsEntity) o;

        return id != null && id.equals(other.id);

    }

//    @Column(name="idApp")
//    @Setter
//    private String idApp;
//
//
//    @LazyCollection(LazyCollectionOption.FALSE)
//    @ManyToMany(cascade = {CascadeType.ALL})
//    @JoinTable(name="UserNews",
//            joinColumns={@JoinColumn(referencedColumnName = "PersonnelID", name="idUser")},
//            inverseJoinColumns={@JoinColumn(referencedColumnName = "id", name="idNews")})
//    private Collection<UserEntity> userEntities;

//    @OneToMany(mappedBy = "residentEntity")
//    private Set<ContactsResidentsEntity> contactsResidentsEntities = new HashSet<>();
}
