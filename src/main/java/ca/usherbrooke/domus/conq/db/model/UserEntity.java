package ca.usherbrooke.domus.conq.db.model;

import ca.usherbrooke.domus.conq.db.DatabaseConfig;
import ca.usherbrooke.domus.conq.db.HibernateDAO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe qui représente l'entité utilisateur.
 * Un utilisateur est forcément enregistré comme personnel. Un utilisateur peut se connecter à l'application grâce à
 * son pseudo et mot de passe
 *
 * @author Claire
 */
@Entity
@Table(name = "User")
@Getter
@ToString
public class UserEntity {

    public static final HibernateDAO<UserEntity> DAO = new HibernateDAO<>(UserEntity.class, DatabaseConfig.MODEL);

    @Id
    @Setter
    @Column(name = "idPersonnel")
    private Integer id;

    @Setter
    @Column(name = "pseudo")
    private String pseudo;

    @Setter
    @Column(name = "password")
    private String password;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntity other = (UserEntity) o;

        return id != null && id.equals(other.id);

    }

    @Override
    public int hashCode() {
        return id == null ? 1 : id.hashCode();
    }

}
