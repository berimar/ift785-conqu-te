package ca.usherbrooke.domus.conq.db.model;

import ca.usherbrooke.domus.conq.db.DatabaseConfig;
import ca.usherbrooke.domus.conq.db.HibernateDAO;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Entité qui représente un résident.
 * Le résident possède une liste de {@link ca.usherbrooke.domus.conq.db.model.ContactEntity} qui lui sont associés
 * <br />
 * Created by ranj2004 on 15-04-03.
 */
@Entity
@Table(name = "Resident")
@Getter
@Setter
public class ResidentEntity extends Person {

    public static final HibernateDAO<ResidentEntity> DAO = new HibernateDAO<>(ResidentEntity.class, DatabaseConfig.MODEL);

    @JoinColumn(name = "apartmentNumber")
    @OneToOne(fetch = FetchType.EAGER, targetEntity=ApartmentEntity.class , cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private ApartmentEntity apartmentEntity;

    @Column(name = "birthday")
    private String birthday;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(cascade = {CascadeType.PERSIST})
    @JoinTable(name = "ContactsResidents",
            joinColumns = {@JoinColumn(referencedColumnName = "id", name = "idResident")},
            inverseJoinColumns = {@JoinColumn(referencedColumnName = "id", name = "idContact")})
    private Set<ContactEntity> contactEntities = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResidentEntity other = (ResidentEntity) o;

        return id != null && id.equals(other.id);

    }

    @Override
    public String getDisplayableType() {
        return "résident";
    }

}
