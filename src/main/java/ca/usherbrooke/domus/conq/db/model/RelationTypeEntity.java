package ca.usherbrooke.domus.conq.db.model;

import ca.usherbrooke.domus.conq.db.WrappedListDAO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * Entité qui représente le type de relation entre un contact et un résident.
 * Hérite de GestionTypeEntity pour permettre la mise en place d'un patron de conception Template
 * <br />
 * date 07/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@ToString
@Entity
@Table(name = "RelationType")
public class RelationTypeEntity extends GestionTypeEntity {

    /**
     * Instancie une nouvelle entité .
     *
     * @implNote Pas de DAO accessible directement depuis la classe, il faut utiliser le DataProvider
     * De même pour l'instanciation, il faut utiliser newInstance() depuis la liste disponible dans le DataProvider
     * @see ca.usherbrooke.domus.conq.db.DataProvider
     * @see WrappedListDAO
     */
    RelationTypeEntity() {
    }

    @Getter
    @Id
    @GeneratedValue
    private Integer id;

    @Getter
    @Setter
    @Column(name = "type")
    private String type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RelationTypeEntity other = (RelationTypeEntity) o;
        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return id == null ? 1 : id.hashCode();
    }
}
