package ca.usherbrooke.domus.conq.db.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * date 10/04/15 <br/>
 * Classe abstraite pour factoriser les champs en commun avec de nombreuses
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 * @implNote Ceci n'est pas une entité
 * @see javax.persistence.MappedSuperclass
 */
@Getter
@Setter
@MappedSuperclass
public abstract class Person {

    @Id
    @GeneratedValue
    @Column(name = "id")
    protected Integer id;

    @Column(name = "lastName")
    protected String lastName;

    @Column(name = "firstName")
    protected String firstName;

    @Column(name = "photoUri")
    protected String photoUri;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    @Override
    public final int hashCode() {
        return id == null ? 1 : id.hashCode();
    }

    /**
     * On force la réimplémentation
     *
     * @inheritDoc
     */
    @Override
    public abstract boolean equals(Object o);

    public abstract String getDisplayableType();

    /**
     * @return une chaine de caractère sous la forme "nom" + "prénom"
     */
    public String toDisplayableName() {
        return firstName + " " + lastName;
    }

    @Override
    public final String toString() {
        return toDisplayableName();
    }

}
