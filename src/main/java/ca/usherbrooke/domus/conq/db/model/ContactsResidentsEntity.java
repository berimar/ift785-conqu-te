package ca.usherbrooke.domus.conq.db.model;

import ca.usherbrooke.domus.conq.db.DatabaseConfig;
import ca.usherbrooke.domus.conq.db.HibernateDAO;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Représente la liaison entre un contact et un résident.
 * <br />
 * Created by ranj2004 on 15-04-03.
 */
@Entity
@Table(name = "ContactsResidents")
@Getter
@Setter
@ToString
public class ContactsResidentsEntity implements Serializable {

    public static final HibernateDAO<ContactsResidentsEntity> DAO = new HibernateDAO<>(ContactsResidentsEntity.class, DatabaseConfig.MODEL);


    @Id
    @NonNull
    @ManyToOne(cascade = {CascadeType.REMOVE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "idContact")
    private ContactEntity contactEntity;

    @Id
    @NonNull
    @ManyToOne(cascade = {CascadeType.REMOVE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "idResident")
    private ResidentEntity residentEntity;

    @NonNull
    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "idRelation")
    private RelationTypeEntity relationType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactsResidentsEntity other = (ContactsResidentsEntity) o;

        if (!contactEntity.equals(other.contactEntity)) return false;
        if (!relationType.equals(other.relationType)) return false;
        return residentEntity.equals(other.residentEntity);

    }

    @Override
    public int hashCode() {
        int result = contactEntity.hashCode();
        result = 31 * result + residentEntity.hashCode();
        result = 31 * result + relationType.hashCode();
        return result;
    }
}
