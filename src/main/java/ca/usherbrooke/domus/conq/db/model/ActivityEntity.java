package ca.usherbrooke.domus.conq.db.model;

import ca.usherbrooke.domus.conq.commons.TimeUtils;
import ca.usherbrooke.domus.conq.db.DatabaseConfig;
import ca.usherbrooke.domus.conq.db.HibernateDAO;
import ca.usherbrooke.domus.conq.uapps.jfxtagenda.JFXActivity;
import jfxtras.scene.control.agenda.Agenda;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.hibernate.Session;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.classic.Lifecycle;

import javax.persistence.*;
import java.io.Serializable;
import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Représente une activité dans l'app Agenda.
 * Une activité est associée à du personnel et des résidents qui y participent
 * <br />
 * date 06/04/15 <br/>
 *
 * @author sv3inburn3
 */
@Table(name = "Activity")
@Entity
@Log
public class ActivityEntity implements Lifecycle {

    public static final HibernateDAO<ActivityEntity> DAO = new HibernateDAO<>(ActivityEntity.class, DatabaseConfig.MODEL);

    @Id
    @GeneratedValue
    @Setter
    private Integer id;

    @Column(name = "idOwner")
    private Integer idOwner = 1;     //TODO gérer idOwner

    @Column(name = "dateEnd")
    private String dateEnd_s;

    @Column(name = "dateBegin")
    private String dateBegin_s;

    @Column(name = "description")
    private String description;

    @Column(name = "location")
    private String location;

    @Column(name = "summary")
    private String summary;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(cascade = {CascadeType.DETACH})
    @JoinTable(name = "ResidentActivities",
            joinColumns = {@JoinColumn(referencedColumnName = "id", name = "idActivity")},
            inverseJoinColumns = {@JoinColumn(referencedColumnName = "id", name = "idResident")})
    private List<ResidentEntity> residentEntities = new ArrayList<>();

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(cascade = {CascadeType.DETACH})
    @JoinTable(name = "PersonnelActivities",
            joinColumns = {@JoinColumn(referencedColumnName = "id", name = "idActivity")},
            inverseJoinColumns = {@JoinColumn(referencedColumnName = "id", name = "idPersonnel")})
    private List<PersonnelEntity> personnelEntities = new ArrayList<>();

    @Transient
    @Getter
    @Setter
    private JFXActivity boundJFXActivity;

    private ZonedDateTime extractZonedDateTime(String formatedDate) {
        try {
            return TimeUtils.extractLocalDateTime(formatedDate);
        } catch (ParseException e) {
            log.severe("Le format de date en base de donnée est innatendu ");
            return null;
        }
    }


    @Override
    public String toString() {
        return "ActivityEntity{" +
                "id=" + id +
                ", dateBegin_s='" + dateBegin_s + '\'' +
                ", dateEnd_s='" + dateEnd_s + '\'' +
                ", description='" + description + '\'' +
                ", location='" + location + '\'' +
                ", summary='" + summary + '\'' +
                '}';
    }

    @Override
    public boolean onSave(Session s) {
        onSaveOrUpdate();
        return false;
    }

    @Override
    public boolean onUpdate(Session s) {
        onSaveOrUpdate();
        return false;
    }

    @Override
    public boolean onDelete(Session s) {
        return false;
    }

    @Override
    public void onLoad(Session s, Serializable id) {
        onLoading();
        s.persist(this);
    }

    private void onSaveOrUpdate() {
        dateBegin_s = TimeUtils.formatZonedDateTime(boundJFXActivity.getStartZonedDateTime());
        dateEnd_s = TimeUtils.formatZonedDateTime(boundJFXActivity.getEndZonedDateTime());
        location = boundJFXActivity.getLocation();
        description = boundJFXActivity.getDescription();
        summary = boundJFXActivity.getSummary();
        residentEntities = boundJFXActivity.getResidents();
        personnelEntities = boundJFXActivity.getPersonnels();
    }

    private void onLoading() {
        boundJFXActivity = (JFXActivity) JFXActivity.fromDB(this)
                .withResidents(this.residentEntities)
                .withPersonnels(this.personnelEntities)
                .withDescription(this.description)
                .withLocation(this.location)
                .withWholeDay(false)
                .withSummary(this.summary)
                .withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass("group0"));
        boundJFXActivity.setEndZonedDateTime(extractZonedDateTime(dateEnd_s));
        boundJFXActivity.setStartZonedDateTime(extractZonedDateTime(dateBegin_s));
    }


    /**
     * Lie l'activity associée à l'interface graphique avec une version persistente.
     * L'appel à cette méthode provoque une sauvegarde en base de donnée.
     *
     * @param jfxActivity l'activity associée à l'interface graphique
     * @return la version persistente
     */
    public static ActivityEntity bind(JFXActivity jfxActivity) {
        ActivityEntity activityEntity = new ActivityEntity();
        activityEntity.boundJFXActivity = jfxActivity;
        //inutile de stoquer les dates grace à la méthode onSave
        activityEntity.description = jfxActivity.getDescription();
        activityEntity.summary = jfxActivity.getSummary();
        activityEntity.location = jfxActivity.getLocation();
        activityEntity.id = null;
        DAO.persist(activityEntity);
        return activityEntity;
    }


    //Très important pour le Set
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActivityEntity other = (ActivityEntity) o;

        return id.equals(other.id);

    }

    @Override
    public int hashCode() {
        return id == null ? 1 : id.hashCode();
    }
}
