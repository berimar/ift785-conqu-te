package ca.usherbrooke.domus.conq.db.model;

import ca.usherbrooke.domus.conq.db.DatabaseConfig;
import ca.usherbrooke.domus.conq.db.HibernateDAO;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Représente l'entité contact. Un contact est lié à au moins un résident.
 * <br />
 * Created by ranj2004 on 15-04-03.
 */
@Entity
@Table(name = "Contact")
public class ContactEntity extends ConnectedPerson {

    public static final HibernateDAO<ContactEntity> DAO = new HibernateDAO<>(ContactEntity.class, DatabaseConfig.MODEL);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContactEntity other = (ContactEntity) o;
        return id != null && id.equals(other.id);
    }

    @Override
    public String getDisplayableType() {
        return "contact";
    }
}
