package ca.usherbrooke.domus.conq.db.model;

import ca.usherbrooke.domus.conq.db.DatabaseConfig;
import ca.usherbrooke.domus.conq.db.HibernateDAO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Entité qui représente les différents type de personnels (Employé, Externe)
 *
 * @author Claire
 */
@Entity
@Table(name = "PersonnelType")
public class PersonnelTypeEntity {

    public static final HibernateDAO<PersonnelTypeEntity> DAO = new HibernateDAO<>(PersonnelTypeEntity.class, DatabaseConfig.MODEL);

    @Getter
    @Id
    @GeneratedValue
    private Integer id;

    @Getter
    @Setter
    @Column(name = "type")
    private String type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonnelTypeEntity other = (PersonnelTypeEntity) o;
        return id != null && id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return id == null ? 1 : id.hashCode();
    }

    @Override
    public String toString() {
        return type;
    }
}
