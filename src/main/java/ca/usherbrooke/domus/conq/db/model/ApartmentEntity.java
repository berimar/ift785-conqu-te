package ca.usherbrooke.domus.conq.db.model;

import ca.usherbrooke.domus.conq.db.WrappedListDAO;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;

/**
 * Représente les entités appartements.
 * Sous-classe de GestionTypeEntity pour permettre la mise en place d'un patron de conception Template
 * <br />
 * date 07/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */

@Entity
@Table(name="Apartment")
public class ApartmentEntity extends GestionTypeEntity {

    /**
     * Instancie une nouvelle entité .
     *
     * @implNote Pas de DAO accessible directement depuis la classe, il faut utiliser le DataProvider
     * De même pour l'instanciation, il faut utiliser newInstance() depuis le Set disponible dans le DataProvider
     * @see ca.usherbrooke.domus.conq.db.DataProvider
     * @see WrappedListDAO
     */
    ApartmentEntity() {}

    @Id
    @Getter
    @Setter
    @NonNull
    private Integer apartmentNumber;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApartmentEntity other = (ApartmentEntity) o;
        return apartmentNumber.equals(other.apartmentNumber);
    }

    @Override
    public int hashCode() {
        return apartmentNumber.hashCode();
    }
}
