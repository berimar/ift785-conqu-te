package ca.usherbrooke.domus.conq.db.model;

import ca.usherbrooke.domus.conq.db.DatabaseConfig;
import ca.usherbrooke.domus.conq.db.HibernateDAO;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entité représentant le personnel (employés et externes).
 * Un employé peut être un utilisateur ou non.
 * <br />
 * date 10/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Entity
@Table(name = "Personnel")
public class PersonnelEntity extends ConnectedPerson {

    public static final HibernateDAO<PersonnelEntity> DAO = new HibernateDAO<>(PersonnelEntity.class, DatabaseConfig.MODEL);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonnelEntity other = (PersonnelEntity) o;

        return id != null && id.equals(other.id);

    }

    @Getter
    @Setter
    @Column(name = "personnelTypeId")
    protected String personnelTypeId;

    @Override
    public String getDisplayableType() {
        return PersonnelTypeEntity.DAO.getByCriteria("id", Integer.parseInt(personnelTypeId)).toString();
    }

}
