package ca.usherbrooke.domus.conq.db;

import lombok.extern.java.Log;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * date : 24-03-15 <br>
 * Cette classe permet de renvoyer les connexions aux bases de données à la demande. À chaque fois qu'une uApp veut accèder
 * à une base de données elle peut demander la connexion en appelant la méthode getConnection avec comme paramètre un DatabaseName.
 * Cette classe garde des instances uniques des connexions vers les bases de données.
 * <p>
 * @author louw2901
 */
@Log
public class DatabaseConnection {

    private DatabaseConnection(){}

    private static final String BASE_PATH = DatabaseConnection.class.getPackage().getName().replace(".", "/") + File.separator;

    private static final Map<DatabaseConfig, SessionFactory> SESSIONS = new HashMap<>();

    /**
     * Cette méthode renvoie la connexion vers la base de données indiquée en paramètre.
     *
     * @param databaseConfig ce paramètre contient le chemin du fichier de configuration à utiliser pour établir la connexion.
     *                     Ce fichier est mis dans les ressources.
     * @return une sessionFactory qui permet d'ouvrir une session pour pouvoir exécuter des requêtes SQL
     */
    protected static synchronized SessionFactory getConnection(DatabaseConfig databaseConfig) {
        try {
            if (!SESSIONS.containsKey(databaseConfig)) {
                Configuration configuration = new Configuration();
                configuration.configure(BASE_PATH + databaseConfig.getValue());

                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
                SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);

                SESSIONS.put(databaseConfig, sessionFactory);
            }

            return SESSIONS.get(databaseConfig);
        } catch (Throwable ex) {
            log.info("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
}
