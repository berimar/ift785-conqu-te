package ca.usherbrooke.domus.conq.db;

import lombok.Getter;
import lombok.extern.java.Log;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.logging.Level;


/**
 * date : 15-04-04 <br>
 * Une classe pour enveloper et factoriser les traitements propres à une {@code Session}
 *
 * @author sveinburne
 * @implNote Utilise des lambdas envelopés dans un traitement sécurisé d'une {@code Session} et d'une {@code Transaction}
 * @see org.hibernate.Session
 * @see org.hibernate.Transaction
 * @see ca.usherbrooke.domus.conq.db.HibernateDAO
 */
@Log
public class SessionBuilder<T> {


    @Getter
    private final DatabaseConfig databaseName;

    SessionBuilder(DatabaseConfig databaseName){
        this.databaseName = databaseName;
    }

    /**
     * Encapsule un traitement de façon sécurisée sur une lambda renvoyant une liste d'{@code Entity}.
     *
     * @param callback le traitement encapsulé
     * @return la liste que doit renvoyer la callback
     * @see javax.persistence.Entity
     */
    public List<T> list(BiFunction<Session, Transaction, List<T>> callback) {
        return list(callback, SessionBuilder::defaultConstraintViolationExceptionHandler);
    }

    /**
     * Encapsule un traitement de façon sécurisée sur une lambda renvoyant une liste d'{@code Entity}.
     *
     * @param callback              le traitement encapsulé
     * @param onConstraintViolation la fonction a appeller en cas de {@code ConstraintViolationException} (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @return la liste que doit renvoyer la callback
     * @see javax.persistence.Entity
     * @see ConstraintViolationException
     */
    public List<T> list(BiFunction<Session, Transaction, List<T>> callback, Consumer<ConstraintViolationException> onConstraintViolation) {
        Session session = null;
        try {
            session = DatabaseConnection.getConnection(databaseName).openSession();
            Transaction trans = session.beginTransaction();
            List<T> collection = callback.apply(session, trans);
            trans.commit();
            return collection;
        } catch (ConstraintViolationException jdbce) {
            onConstraintViolation.accept(jdbce);
        } finally {
            closeSilently(session);
        }
        return null;
    }

    /**
     * Encapsule un traitement de façon sécurisée sur une lambda renvoyant une {@code Entity}.
     *
     * @param callback              le traitement encapsulé
     * @param onConstraintViolation la fonction a appeller en cas de {@code ConstraintViolationException} (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @return l'entité renvoyée par la callback
     * @see javax.persistence.Entity
     * @see ConstraintViolationException
     */
    public T entity(BiFunction<Session, Transaction, T> callback, Consumer<ConstraintViolationException> onConstraintViolation) {
        Session session = null;
        try {
            session = DatabaseConnection.getConnection(databaseName).openSession();
            Transaction trans = session.beginTransaction();
            T entity = callback.apply(session, trans);
            trans.commit();
            return entity;
        } catch (ConstraintViolationException jdbce) {
            onConstraintViolation.accept(jdbce);
        } finally {
            closeSilently(session);
        }
        return null;
    }

    /**
     * Encapsule un traitement de facon sécurisée sur une lamda renvoyant une {@code Entity}.
     *
     * @param callback le traitement encapsulé
     * @return l'entité renvoyée par la callback
     * @see javax.persistence.Entity
     */
    public T entity(BiFunction<Session, Transaction, T> callback) {
        return entity(callback, SessionBuilder::defaultConstraintViolationExceptionHandler);
    }

    /**
     * Encapsule un traitement de facon sécurisée sur une lamda ne renvoyant rien.
     *
     * @param callback le traitement encapsulé
     */
    public void vacuity(BiConsumer<Session, Transaction> callback) {
        vacuity(callback, SessionBuilder::defaultConstraintViolationExceptionHandler);
    }

    /**
     * Encapsule un traitement de façon sécurisée sur une lambda ne renvoyant rien.
     *
     * @param callback              le traitement encapsulé
     * @param onConstraintViolation la fonction a appeller en cas de {@code ConstraintViolationException} (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @see ConstraintViolationException
     */
    public void vacuity(BiConsumer<Session, Transaction> callback, Consumer<ConstraintViolationException> onConstraintViolation) {
        Session session = null;
        try {
            session = DatabaseConnection.getConnection(databaseName).openSession();
            Transaction trans = session.beginTransaction();
            callback.accept(session, trans);
            trans.commit();
        } catch (ConstraintViolationException jdbce) {
            onConstraintViolation.accept(jdbce);
        } finally {
            closeSilently(session);
        }
    }


    private void closeSilently(Session session) {
        if (session != null) {
            session.close();
        }
    }

    private static void defaultConstraintViolationExceptionHandler(ConstraintViolationException jdbce) {
        log.log(Level.SEVERE, "Exception sql inatendue : ", jdbce);
    }
}
