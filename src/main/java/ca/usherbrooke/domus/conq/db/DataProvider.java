package ca.usherbrooke.domus.conq.db;

import ca.usherbrooke.domus.conq.db.model.ApartmentEntity;
import ca.usherbrooke.domus.conq.db.model.RelationTypeEntity;
import lombok.Getter;


/**
 * date 16/04/15 <br/>
 * Un singleton pour gérer les instances d'entité en cache au travers des multiples {@code UApp}s
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 * @implNote implémentation du pattern façade, car l'accès aux collections disponible dans cet objet est restreint
 */
public class DataProvider {

    private static final DataProvider SINGLETON = new DataProvider();

    private static final HibernateDAO<RelationTypeEntity> RELATION_TYPE_DAO = new HibernateDAO<>(RelationTypeEntity.class, DatabaseConfig.MODEL);
    private static final HibernateDAO<ApartmentEntity> APARTEMENT_DAO = new HibernateDAO<>(ApartmentEntity.class, DatabaseConfig.MODEL);


    /**
     * L'ensemble des types de relation
     */
    @Getter(lazy = true)
    private final WrappedListDAO<RelationTypeEntity> relationTypes = new WrappedListDAO<>(RELATION_TYPE_DAO);

    /**
     * L'ensemble des appartements
     */
    @Getter(lazy = true)
    private final WrappedListDAO<ApartmentEntity> apartments = new WrappedListDAO<>(APARTEMENT_DAO);

    private DataProvider() {
    }

    /**
     * @return le singleton
     */
    public static DataProvider get() {
        return SINGLETON;
    }

}
