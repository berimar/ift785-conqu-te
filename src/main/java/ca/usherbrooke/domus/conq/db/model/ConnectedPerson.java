package ca.usherbrooke.domus.conq.db.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * date 10/04/15 <br/>
 * Classe abstraite pour représenter une personne potentiellement joignable par internet.
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 * @implNote Ceci n'est pas une entité
 * @see javax.persistence.MappedSuperclass
 */
@MappedSuperclass
public abstract class ConnectedPerson extends Person {

    @Getter
    @Setter
    @Column(name = "emailAddress")
    protected String emailAddress;

}
