package ca.usherbrooke.domus.conq.db;

import lombok.Getter;
import lombok.ToString;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import java.util.List;
import java.util.function.Consumer;

/**
 * --- Patron de conception Façade ---
 * date : 15-04-04 <br>
 * Un DAO pour réaliser les operations relative à la BDD propres à une entité <br>
 *
 * @author sveinburne
 * @apiNote Pour des sessions avec des manipulations plus complexes, il est recommander d'utiliser directement un {@link SessionBuilder}
 * @see javax.persistence.Entity
 * @see SessionBuilder
 */
@SuppressWarnings("unchecked")
@ToString
public class HibernateDAO<T> {


    @Getter
    private final Class<T> entityClass;
    @Getter
    private final DatabaseConfig databaseConfig;
    private final SessionBuilder<T> sessionBuilder ;

    /**
     * Créer une instance relative à une {@code Entity}
     *
     * @param entityClass la classe de l'entité annotée par {@code @Entity}
     * @see javax.persistence.Entity
     */
    public HibernateDAO(final Class<T> entityClass, final DatabaseConfig databaseConfig) {
        this.entityClass = entityClass;
        this.databaseConfig = databaseConfig;
        this.sessionBuilder = new SessionBuilder<>(databaseConfig);
    }

    /**
     * @return la liste complète, sans filtrage, des instances d'une entité de type T.
     */
    public List<T> getAll() {
        return sessionBuilder.list((session, trans) -> {
            Query query = session.createQuery("FROM " + entityClass.getSimpleName());
            return query.list();
        });
    }

    /**
     * @param onConstraintViolation la fonction a appeller en cas d'erreur (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @return la liste complète, sans filtrage, des instances d'une entité de type T.
     */
    public List<T> getAll(Consumer<ConstraintViolationException> onConstraintViolation) {
        return sessionBuilder.list((session, trans) -> {
            Query query = session.createQuery("from " + entityClass.getSimpleName());
            return query.list();
        }, onConstraintViolation);
    }

    /**
     * Rend une entité transitoire persistante.
     *
     * @param entity l'entité qu'on souhaite rendre persistante
     * @see org.hibernate.Session#persist
     */
    public void persist(T entity) {
        sessionBuilder.vacuity((session, trans) -> session.persist(entity));
    }

    /**
     * Rend une entité transitoire persistante.
     *
     * @param entity                l'entité qu'on souhaite rendre persistante
     * @param onConstraintViolation la fonction a appeller en cas d'erreur (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @see org.hibernate.Session#persist
     */
    public void persist(T entity, Consumer<ConstraintViolationException> onConstraintViolation) {
        sessionBuilder.vacuity((session, trans) -> session.persist(entity), onConstraintViolation);
    }

    /**
     * @param entity l'entité qu'on souhaite supprimer en base.
     * @see org.hibernate.Session#delete
     */
    public void delete(T entity) {
        sessionBuilder.vacuity((session, trans) -> session.delete(entity));
    }

    /**
     * @param entity                l'entité qu'on souhaite supprimer en base.
     * @param onConstraintViolation la fonction a appeller en cas d'erreur (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @see org.hibernate.Session#delete
     */
    public void delete(T entity, Consumer<ConstraintViolationException> onConstraintViolation) {
        sessionBuilder.vacuity((session, trans) -> session.delete(entity), onConstraintViolation);
    }

    /**
     * @param entity l'entité persistente détachée (qui est modifié depuis la dernière version) qu'on souhaite mettre à jour en base.
     * @see org.hibernate.Session#update
     */
    public void update(T entity) {
        sessionBuilder.vacuity((session, trans) -> session.update(entity));
    }

    /**
     * @param entity                l'entité persistente détachée (qui est modifié depuis la dernière version) qu'on souhaite mettre à jour en base.
     * @param onConstraintViolation la fonction a appeller en cas d'erreur (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @see org.hibernate.Session#update
     */
    public void update(T entity, Consumer<ConstraintViolationException> onConstraintViolation) {
        sessionBuilder.vacuity((session, trans) -> session.update(entity), onConstraintViolation);
    }

    /**
     * @param oldVersion la version détachée qu'on sohaite fusionner avec la version en base.
     * @return la version fusionnée.
     * @see org.hibernate.Session#merge
     */
    public T merge(T oldVersion) {
        return sessionBuilder.entity((session, transaction) -> (T) session.merge(oldVersion));
    }

    /**
     * @param oldVersion            la version détachée qu'on souhaite fusionner avec la version en base.
     * @param onConstraintViolation la fonction a appeller en cas d'erreur (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @return la version fusionnée.
     * @see org.hibernate.exception.ConstraintViolationException
     * @see org.hibernate.Session#merge
     */
    @SuppressWarnings("RedundantCast")
    public T merge(T oldVersion, Consumer<ConstraintViolationException> onConstraintViolation) {
        return sessionBuilder.entity((session, transaction) -> (T) session.merge(oldVersion), onConstraintViolation);
    }


    /**
     * @param versionToRefresh la version persistente qu'on souhaite réactualiser depuis la base de donnée.
     * @see org.hibernate.Session#refresh
     */
    public void refresh(T versionToRefresh) {
        sessionBuilder.vacuity((session, transaction) -> session.refresh(versionToRefresh));
    }

    /**
     * @param versionToRefresh      la version persistente qu'on souhaite réactualiser depuis la base de donnée.
     * @param onConstraintViolation la fonction a appeller en cas d'erreur (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @see org.hibernate.exception.ConstraintViolationException
     * @see org.hibernate.Session#refresh
     */
    public void refresh(T versionToRefresh, Consumer<ConstraintViolationException> onConstraintViolation) {
        sessionBuilder.vacuity((session, transaction) -> session.refresh(versionToRefresh), onConstraintViolation);
    }

    /**
     * @param hql                   l'expression HQL permettant la sélection d'une ligne.
     * @param onConstraintViolation la fonction a appeller en cas d'erreur (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @return le premier élément de la liste remvoyée par la requête HQL
     * @see org.hibernate.exception.ConstraintViolationException
     */
    public T get(String hql, Consumer<ConstraintViolationException> onConstraintViolation) {
        return sessionBuilder.entity((session, transaction) -> {
            Query query = session.createQuery(hql);
            List<T> entities = query.list();
            if (entities.size() > 0) {
                return entities.get(0);
            } else return null;
        }, onConstraintViolation);
    }

    /**
     * @param hql l'expression HQL permettant la sélection d'une ligne.
     * @return le premier élément de la liste remvoyée par la requête HQL
     */
    public T get(String hql) {
        return sessionBuilder.entity((session, transaction) -> {
            Query query = session.createQuery(hql);
            List<T> entities = query.list();
            if (entities.size() > 0) {
                return entities.get(0);
            } else return null;
        });
    }


    /**
     * Retroune un élément en base avec un critère de recherche donné qui n'est pas la clé primaire
     * @param detachedCriteria le critère par lequel chercher
     * @return l'élément recherché
     */
    public T getByCriteria(DetachedCriteria detachedCriteria) {
        return sessionBuilder.entity((session, transaction) -> {
            Criteria criteria = detachedCriteria.getExecutableCriteria(session);
            return getByCriteria(session, transaction, criteria);
        });
    }

    /**
     * Retroune une liste d'éléments en base avec un critère de recherche donné qui n'est pas la clé primaire
     * @param detachedCriteria le critère par lequel chercher
     * @return la liste d'éléments recherchés
     */
    public List<T> getListByCriteria(DetachedCriteria detachedCriteria) {
        return sessionBuilder.list((session, transaction) -> {
            Criteria criteria = detachedCriteria.getExecutableCriteria(session);
            return getListByCriteria(session, transaction, criteria);
        });
    }

    /**
     * Retourne une liste d'éléments de la base de données en filtrant la recherche avec une requête HQL donnée
     * @param hqlStatement la requête HQL
     * @return la liste d'éléments
     */
    public List<T> getListByHQLStatement(String hqlStatement){
        return sessionBuilder.list((session,transaction)-> session.createQuery(hqlStatement).list());
    }

    /**
     * Retourne un élément d'une recherche en base de donnée avec un critère donné. La connection à la base de données est celle
     * donnée en argument
     * @param session la connection à la BD
     * @param transaction la connection à la BD
     * @param criteria le critère par lequel chercher
     * @return l'élément recherché
     */
    private T getByCriteria(Session session, Transaction transaction, Criteria criteria) {
        List<T> entities = getListByCriteria(session, transaction, criteria);
        if (entities != null && entities.size() > 0) {
            return entities.get(0);
        } else return null;
    }

    /**
     * Retourne une liste d'éléments d'une recherche en base de donnée avec un critère donné. La connection à la base de données est celle
     * donnée en argument
     * @param session la connection à la BD
     * @param transaction la connection à la BD
     * @param criteria le critère par lequel chercher
     * @return les éléments recherchés
     */
    private List<T> getListByCriteria(Session session, Transaction transaction, Criteria criteria) {
        return criteria.list();
    }

    /**
     * Retourne un élément de la base de donnée avec un critère de recherche qui n'est pas la clé primaire
     * Équivalent à un 'SELECT * FROM T WHERE nameCriterion=valueCriterion'
     *
     * @param nameCriterion  le nom de la colonne par laquelle filtrer
     * @param valueCriterion la valeur par laquelle filtrer
     * @return l'élément (l'entité)
     */
    public T getByCriteria(String nameCriterion, Object valueCriterion) {
        return sessionBuilder.entity((session, transaction) -> {
            Criteria criteria = session.createCriteria(entityClass);
            criteria.add(Restrictions.eq(nameCriterion, valueCriterion));
            return getByCriteria(session, transaction, criteria);
        });
    }

    /**
     * Retourne un élément de la base de donnée avec un critère de recherche qui n'est pas la clé primaire
     * Équivalent à un 'SELECT * FROM T WHERE nameCriterion=valueCriterion AND nameCriterion2=valueCriterion2'
     *
     * @param nameCriterion   le nom de la colonne par laquelle filtrer
     * @param valueCriterion  la valeur par laquelle filtrer
     * @param nameCriterion2  le nom de la 2e colonne par laquelle filtrer
     * @param valueCriterion2 la valeur de la 2e colonne par laquelle filtrer
     * @return l'élément (l'entité)
     */
    public T getByCriteriae(String nameCriterion, Object valueCriterion, String nameCriterion2, Object valueCriterion2) {
        return sessionBuilder.entity((session, transaction) -> {
            Criteria criteria = session.createCriteria(entityClass);
            criteria.add(Restrictions.eq(nameCriterion, valueCriterion));
            criteria.add(Restrictions.eq(nameCriterion2, valueCriterion2));
            return getByCriteria(session, transaction, criteria);
        });
    }

    /**
     * Retourne un élément de la base de donnée avec un critère de recherche qui n'est pas la clé primaire
     * Équivalent à un 'SELECT * FROM T WHERE nameCriterion=valueCriterion'
     *
     * @param nameCriterion         le nom de la colonne par laquelle filtrer
     * @param valueCriterion        la valeur par laquelle filtrer
     * @param onConstraintViolation la fonction a appeller en cas d'erreur (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @return l'élément (l'entité)
     * @see org.hibernate.exception.ConstraintViolationException
     */
    public T getByCriterion(String nameCriterion, String valueCriterion, Consumer<ConstraintViolationException> onConstraintViolation) {
        return sessionBuilder.entity((session, transaction) -> {
            Criteria criteria = session.createCriteria(entityClass);
            Criterion crit1 = Restrictions.eq(nameCriterion, valueCriterion);
            criteria.add(crit1);
            return getByCriteria(session, transaction, criteria);
        }, onConstraintViolation);
    }

    /**
     * Retourne une liste de la base de donnée avec un critère de recherche qui n'est pas la clé primaire
     * Équivalent à un 'SELECT * FROM T WHERE nameCriterion=valueCriterion'
     *
     * @param nameCriterion  le nom de la colonne par laquelle filtrer
     * @param valueCriterion la valeur par laquelle filtrer
     * @return la liste d'éléments (les entités)
     */
    public List<T> getListByCriterion(String nameCriterion, String valueCriterion) {
        return sessionBuilder.list((session, transaction) -> {
            Criteria criteria = session.createCriteria(entityClass);
            criteria.add(Restrictions.eq(nameCriterion, valueCriterion));
            return criteria.list();
        });
    }

    /**
     * Retourne une liste de la base de donnée avec un critère de recherche qui n'est pas la clé primaire
     * Équivalent à un 'SELECT * FROM T WHERE nameCriterion=valueCriterion'
     *
     * @param nameCriterion  le nom de la colonne par laquelle filtrer
     * @param valueCriterion la valeur par laquelle filtrer
     * @return la liste d'éléments (les entités)
     */
    public List<T> getListByCriterion(String nameCriterion, int valueCriterion) {
        return sessionBuilder.list((session, transaction) -> {
            Criteria criteria = session.createCriteria(entityClass);
            criteria.add(Restrictions.eq(nameCriterion, valueCriterion));
            return criteria.list();
        });
    }

    /**
     * Retourne une liste de la base de donnée avec un critère de recherche qui n'est pas la clé primaire
     * Équivalent à un 'SELECT * FROM T WHERE nameCriterion=valueCriterion'
     *
     * @param nameCriterion         le nom de la colonne par laquelle filtrer
     * @param valueCriterion        la valeur par laquelle filtrer
     * @param onConstraintViolation la fonction a appeller en cas d'erreur (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @return l'élément (l'entité)
     * @see org.hibernate.exception.ConstraintViolationException
     */
    public List<T> getListByCriterion(String nameCriterion, String valueCriterion, Consumer<ConstraintViolationException> onConstraintViolation) {
        return sessionBuilder.list((session, transaction) -> {
            Criteria criteria = session.createCriteria(entityClass);
            Criterion crit1 = Restrictions.eq(nameCriterion, valueCriterion);
            criteria.add(crit1);
            return criteria.list();
        }, onConstraintViolation);
    }

    public DetachedCriteria buildCriteria() {
        return DetachedCriteria.forClass(entityClass);
    }


    /**
     * Sauve une entité qui n'est pas en base
     *
     * @param entity l'entité qu'on souhaite sauver
     * @see org.hibernate.Session#persist
     */
    public void save(T entity) {
        sessionBuilder.vacuity((session, trans) -> session.save(entity));
    }

    /**
     * Sauve une entité qui n'est pas en base
     *
     * @param entity                l'entité qu'on souhaite sauver
     * @param onConstraintViolation la fonction a appeller en cas d'erreur (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @see org.hibernate.Session#persist
     */
    public void save(T entity, Consumer<ConstraintViolationException> onConstraintViolation) {
        sessionBuilder.vacuity((session, trans) -> session.save(entity), onConstraintViolation);
    }

    /**
     * Sauve ou commit une entité qui n'est pas en base
     *
     * @param entity l'entité qu'on souhaite sauver ou commit
     * @see org.hibernate.Session#persist
     */
    public void saveOrUpdate(T entity) {
        sessionBuilder.vacuity((session, trans) -> session.saveOrUpdate(entity));
    }

    /**
     * Sauve ou commit  une entité qui n'est pas en base
     *
     * @param entity                l'entité qu'on souhaite sauver ou commit
     * @param onConstraintViolation la fonction a appeller en cas d'erreur (typiquement des contraintes de type UNIQUE ou FOREIGN KEY)
     * @see org.hibernate.Session#persist
     */
    public void saveOrUpdate(T entity, Consumer<ConstraintViolationException> onConstraintViolation) {
        sessionBuilder.vacuity((session, trans) -> session.saveOrUpdate(entity), onConstraintViolation);
    }
}
