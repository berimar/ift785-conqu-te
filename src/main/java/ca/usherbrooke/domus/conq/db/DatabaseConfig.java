package ca.usherbrooke.domus.conq.db;

/**
 * Cette enumération contient les noms de toutes les bases de données ainsi que la localisation de leurs fichiers de configuration.
 * <p>
 * Created by louw2901 on 15-03-27.
 */
public enum DatabaseConfig {
    MODEL("dblocal/model.cfg.xml");

    /***
     *
     */
    private String value;

    DatabaseConfig(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

}
