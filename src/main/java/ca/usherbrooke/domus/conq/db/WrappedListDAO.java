package ca.usherbrooke.domus.conq.db;

import ca.usherbrooke.domus.conq.uapps.UApp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.java.Log;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;

/**
 * date 16/04/15 <br/>
 * Utilitaire pour gérer une collection d'entités uniques, possiblement partagées entre différentes {@link UApp}s
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 * @implNote Patron façade pour les entités qui ne doivent pas être instanciées directement depuis le DAO
 * @see DataProvider
 */
@Log
public class WrappedListDAO<T> {

    private final HibernateDAO<T> hibernateDAO;
    private final Class<T> backingClass;

    @Getter(value = AccessLevel.PRIVATE, lazy = true)
    private final ObservableList<T> backingList = FXCollections.observableList(hibernateDAO.getAll());
    private final Constructor<T> constructor;

    /**
     * Créée une nouvelle instance associée au DAO passé en paramètre
     *
     * @param hibernateDAO le DAO associé
     */
    WrappedListDAO(HibernateDAO<T> hibernateDAO) {
        this.hibernateDAO = hibernateDAO;
        this.backingClass = hibernateDAO.getEntityClass();
        try {
            this.constructor = backingClass.getDeclaredConstructor();
            this.constructor.setAccessible(true);
        } catch (NoSuchMethodException e) {
            log.log(Level.SEVERE, "Erreur d'instanciation. Il doit exister un constructeur sans argument dans une Entity hibernate.", e);
            throw new IllegalStateException(e);
        }
    }


    public ObservableList<T> getUnmodifiableList(){
        return FXCollections.unmodifiableObservableList(getBackingList());
    }

    /**
     * @param entity supprime l'entité de la liste locale et de la base
     */
    public void delete(T entity) {
        if (getBackingList().remove(entity)) {
            hibernateDAO.delete(entity);
        } else throw new IllegalStateException("Ne peut pas supprimer un élément inexistant!");

    }

    /**
     * Sauvegarde l'entité en base <strong>et lui attribue un nouvel id si et seulement si l'entité à une primary key générée automatiquement</strong>
     *
     * @param entity l'entité à sauver
     */
    public void save(T entity) {
        hibernateDAO.save(entity);
        getBackingList().add(entity);
    }

    /**
     * Rend persistante une entité et l'ajoute au set
     *
     * @param entity l'entité à rendre persistante
     */
    public void persist(T entity) {
        hibernateDAO.persist(entity);
        getBackingList().add(entity);
    }


    /**
     * Met à jour l'entité (les modifications locales sont transférées en base)
     *
     * @param entity
     */
    public void update(T entity) {
        hibernateDAO.update(entity);
    }

    /**
     * Rafraichit l'entité (les modifications locales sont écrasées par la version en base)
     *
     * @param entity
     */
    public void refresh(T entity) {
        hibernateDAO.refresh(entity);
    }

    /**
     * Renvoie le premier élément sous la forme d'un {@code Optional}
     *
     * @return le premier élément de la liste enrobé dans un {@code Optional}
     * @see Optional
     */
    public Optional<T> getFirst() {
        return getBackingList().stream().findFirst();
    }

    /**
     * Créée une nouvelle instance
     *
     * @return la nouvelle instance
     * @implNote Pattern façade
     */
    public T newInstance() {
        try {
            return constructor.newInstance();
        } catch (InstantiationException | InvocationTargetException e) {
            log.log(Level.SEVERE, "Erreur d'instanciation", e);
        } catch (IllegalAccessException e) {
            log.log(Level.SEVERE, "Erreur d'instanciation. Accès au constructeur refusé.", e);
        }
        return null;
    }


}
