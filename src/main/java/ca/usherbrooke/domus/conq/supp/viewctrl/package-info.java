/**
 * date 11/03/15
 * @author sv3inburn3
 * Package destinée à l'ensemble vue/controlleur de l'application support.
 * @see ca.usherbrooke.domus.conq.supp
 */
package ca.usherbrooke.domus.conq.supp.viewctrl;