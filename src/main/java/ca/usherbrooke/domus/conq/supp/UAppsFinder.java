package ca.usherbrooke.domus.conq.supp;

import ca.usherbrooke.domus.conq.uapps.Ignore;
import ca.usherbrooke.domus.conq.uapps.UApp;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
//TODO refactoring : remplacer ces affreuses boucles par des lambdas, mettre en place une option de "deepness" (profondeur) de recherche. Par exemple, 0 = aucun sous-package, 1= premier niveaux de sous-packages etc...

/**
 * date 09/03/15 <br/>
 * Objet-outil permettant de chercher des instances de UApp contenues dans un package
 * confère : <a href="http://stackoverflow.com/questions/10910510/get-a-array-of-class-files-inside-a-package-in-java">ce post stackoverflow</a><br/>
 * Il aurait été mieux d'utiliser <a href="https://github.com/ronmamo/reflections">cette librairie</a><br/
 * mais je n'ai pas réussi à la faire fonctionner
 *
 * @author sv3inburn3(jules.randolph[at]reseau.eseo.fr)
 */
@SuppressWarnings("rawtypes")
@RequiredArgsConstructor
@ToString
public class UAppsFinder {
    @NonNull
    private final SupportAppGC supportAppGC;

    private static boolean isConcreteUApp(Class clazz) {
        return UApp.class.isAssignableFrom(clazz) && !Modifier.isAbstract(clazz.getModifiers()) && !clazz.isAnnotationPresent(Ignore.class);
    }

    @SuppressWarnings("unchecked")
    private UApp instanciateUApp(Class clazz) {
        UApp app;
        try {
            app = (UApp) clazz.getConstructor(SupportAppGC.class).newInstance(supportAppGC);
        } catch (InstantiationException e) {
            throw new RuntimeException("La classe à instancier ne peut pas être abstraite.");
        } catch (InvocationTargetException e) {
            //Le constructeur a renvoyé une exception
            InstantiationError error = new InstantiationError("Le constructeur de " + clazz.getName() + " a rencontré une erreur à l'execution : ");
            error.addSuppressed(e);
            throw error;
        } catch (NoSuchMethodException | IllegalAccessException e) {
            throw new IllegalStateException("Sous-classe de UApp : Erreur d'implémentation." +
                    "La classe doit impérativement définir un constructeur 'public' avec un argument de type SupportAppGC",e);
        }
        return app;
    }

    private List<Class> build(Package pkg) {
        String pkgname = pkg.getName();

        String relPath = pkgname.replace('.', '/');

        //System.out.println("ClassDiscovery: Package: " + pkgname + " becomes Path:" + relPath);

        URL resource = ClassLoader.getSystemClassLoader().getResource(relPath);

        //System.out.println("ClassDiscovery: Resource = " + resource);
        if (resource == null) {
            throw new RuntimeException("No resource for " + relPath);
        }

        try {
            return build(new File(resource.toURI()), pkgname);

        } catch (URISyntaxException | IllegalArgumentException e) {
            throw new RuntimeException(pkgname + " (" + resource + ") does not appear to be a valid URL / URI.  Strange, since we got it from the system...", e);
        }

    }

    private List<Class> build(File directory, String pkgname) {
        List<Class> classes = new ArrayList<>();
        String relPath = pkgname.replace('.', '/');

        //System.out.println("ClassDiscovery: Directory = " + directory);
        if (directory != null) {
            String fullPath = directory.getAbsolutePath();
            if (directory.exists()) {

                // Get the list of the files contained in the package
                String[] files = directory.list();
                for (String fileName : files) {

                    // we are only interested in .class files
                    if (fileName.endsWith(".class")) {

                        // removes the .class extension
                        String className = pkgname + '.' + fileName.substring(0, fileName.length() - 6);

                        //System.out.println("ClassDiscovery: className = " + className);

                        try {
                            classes.add(Class.forName(className));
                        } catch (ClassNotFoundException e) {
                            throw new RuntimeException("ClassNotFoundException loading " + className);
                        }
                    } else {
                        //Test if this is a directory
                        File file = Paths.get(directory.getAbsolutePath(), fileName).toFile();
                        if (file.isDirectory()) {
                            classes.addAll(build(file, pkgname + "." + file.getName().replace(".class", "")));
                        }
                    }
                }
            } else {
                try {
                    String jarPath = fullPath.replaceFirst("[.]jar[!].*", ".jar").replaceFirst("file:", "");
                    JarFile jarFile = new JarFile(jarPath);
                    Enumeration<JarEntry> entries = jarFile.entries();
                    while (entries.hasMoreElements()) {
                        JarEntry entry = entries.nextElement();
                        String entryName = entry.getName();
                        if (entryName.startsWith(relPath) && entryName.length() > (relPath.length() + "/".length())) {

                            //System.out.println("ClassDiscovery: JarEntry: " + entryName);
                            String className = entryName.replace('/', '.').replace('\\', '.').replace(".class", "");

                            //System.out.println("ClassDiscovery: className = " + className);
                            try {
                                classes.add(Class.forName(className));
                            } catch (ClassNotFoundException e) {
                                throw new RuntimeException("ClassNotFoundException loading " + className);
                            }
                        }
                    }
                } catch (IOException e) {
                    throw new RuntimeException(pkgname + " (" + directory + ") does not appear to be a valid package", e);
                }
            }
        }
        return classes;
    }

    /**
     * Cherche l'ensemble des classes héritant de {@link UApp} à partir du {@code Package} fourni au constructeur
     *
     * @param pkg le package à partir duquel effectuer la recherche <br/>
     *            La manière la plus simple de récupérer un Pakage : {@link Class#getPackage()}
     * @return une Map contenant une liste d'{@link UApp}s identifiées par leur champ {@code name},
     * qui est leur nom de classe.
     * @throws java.lang.IllegalStateException si plusieurs {@code UApp}s portent le même nom
     */
    public Map<String, UApp> lookup(@NonNull Package pkg) {
        List<Class> classes = build(pkg);
        try {
            return classes.stream()
                    //on élimine les classes qui ne réspectent pas notre prédicat
                    //(qui ne sont pas des implémentation concrêtes de UApp)
                    .filter(UAppsFinder::isConcreteUApp)
                            //on transforme la List<Class> filtrée en List<UApp>
                    .map(this::instanciateUApp)
                            //on génère une Map<String,UApp> à partir d'un Collector
                    .collect(Collectors.toMap(UApp::getName, Function.identity()));
        } catch (IllegalStateException ise) {
            //renvoyé par {@link Collectors#toMap} lorsque une clé est dupliquée
            throw new IllegalStateException("Violation des spécifications propres à l'implémentation d'une UApp. Il est interdit d'avoir deux implémentation portant un même nom. Lisez la documentation du package ca.usherbrooke.domus.conq.uapps pour plus d'infos.", ise);
        }
    }


}
