package ca.usherbrooke.domus.conq.supp.msg;

import ca.usherbrooke.domus.conq.commons.fx.util.FAGlyphs;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import javafx.geometry.Pos;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import lombok.*;
import lombok.extern.java.Log;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.action.Action;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import rx.Observable;

import java.util.concurrent.TimeUnit;

/**
 * date 20/03/15 <br/>
 * Un message à envoyer à l'utilisateur.
 * Utiliser la fabrique {@link MsgHelperFactory} pour construire des instances.
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 * @implNote Façade (la complexité intrinsèque est cachée à l'utilisateur de l'API)
 * @see MsgHelperFactory
 */
@SuppressWarnings("NumericCastThatLosesPrecision")
@Log
@RequiredArgsConstructor(access = AccessLevel.MODULE)
@AllArgsConstructor
public class FeedbackMsgHelper {

    /**
     * Ordre d'importance du message
     */
    @NonNull
    @Getter
    private final Severity severity;


    /**
     * Corps du message
     */
    @NonNull
    @Getter
    private final String message;

    /**
     * Titre fourni par le développeur
     */
    @Getter
    private String providedTitle;

    /**
     * Réacteur qui va charger les messages dans un contexte graphique en particulier.
     */
    @NonNull
    private final FeedbackMsgReactor reactor;

    @Getter(value = AccessLevel.PRIVATE, lazy = true)
    private final Observable<FeedbackMsgHelper> delayedObsrvbl = Observable.just(this).delaySubscription((long) getSeverity().getDuration().toMillis(), TimeUnit.MILLISECONDS);

    @Getter(value = AccessLevel.MODULE, lazy = true)
    private final Notifications fxmsgui = createNotification();

    private Notifications createNotification() {
        Notifications notification = Notifications.create()
                .title(providedTitle == null ? getSeverity().getDefaultTitle() : providedTitle)
                .text(getMessage())
                .hideAfter(getSeverity().getDuration())
                .position(Pos.BOTTOM_RIGHT)
                .graphic(getSeverity().buildNewGlyph())
                .owner(SupportApp.get().getAppGC().getStage())
                .darkStyle();
        return notification;
    }

    /**
     * @return l'{@linkplain Observable} associé au message.
     * @implNote Cet {@code Observable} sera activé à la première souscription ({@linkplain Observable#subscribe()}), et sera retiré après un délais spécifié dans {@link Severity#getDuration()}
     */
    Observable<FeedbackMsgHelper> getDelayableObservable() {
        //Le delayedObsrvbl disparaitra après son lifespan
        return Observable.defer(this::displayThenDie);
    }

    private Observable<FeedbackMsgHelper> displayThenDie() {
        return Observable.just(this)
                .doOnNext(reactor::show)
                .doOnCompleted(() -> getDelayedObsrvbl().subscribe(reactor::remove))
                .doOnError((t) -> {
                    IllegalStateException ise = new IllegalStateException("Erreur : impossible de traiter l'affichage du message.");
                    ise.addSuppressed(t);
                    throw ise;
                });
    }


    /**
     * Ajoute un bouton dont l'action est donnée en argument à la notification
     *
     * @param action du bouton
     */
    public void addAction(Action action) {
        getFxmsgui().action(action);
    }

    @Override
    public String toString() {
        return "(" + getSeverity() + "){" + getMessage() + "}";
    }

    /**
     * Publie le message auprès du {@link FeedbackMsgReactor} pour être affiché à l'écran.
     * Cette méthode peut être appellée un nombre indéterminé de fois.
     */
    public void show() {
        reactor.push(this);
    }

    /**
     * Ordonne l'importance des messages, leur durée de vie et explicite leurs aspects graphiques.
     */
    @AllArgsConstructor
    @SuppressWarnings("MagicNumber")
    public enum Severity {
        /**
         * Confirmer une action à l'utilisateur (vous avez bien modifié ...)
         */
        CONFIRM_USR(Duration.seconds(2),
                "Succès!",
                FontAwesome.Glyph.CHEVRON_CIRCLE_DOWN,
                Color.GREEN),
        /**
         * Donner une information à l'utilisateur.
         */
        INFORM_USR(Duration.millis(2500),
                "Information",
                FontAwesome.Glyph.INFO_CIRCLE,
                Color.DODGERBLUE),
        /**
         * Prévenir l'utilisateur qu'il ne peut pas effectuer une action
         */
        WARN_USR(Duration.seconds(5),
                "Attention!",
                FontAwesome.Glyph.EXCLAMATION_TRIANGLE,
                Color.YELLOW),
        /**
         * Avertir l'utilisateur que son action a échoué
         */
        ALERT_USR(Duration.INDEFINITE,
                "Une erreur a été rencontrée",
                FontAwesome.Glyph.WARNING,
                Color.RED),

        /**
         * Demander à l'utilisateur s'il veut annuler son action
         */
        CANCEL_ACTION(Duration.seconds(7),
                "Attention",
                FontAwesome.Glyph.WARNING,
                Color.YELLOW);

        /**
         * La durée de vie du message en millisecondes.
         */
        @Getter
        private final Duration duration;

        /**
         * La classe css associé au feedback.
         */
        @Getter
        private final String defaultTitle;

        private final FontAwesome.Glyph glyphEnum;

        private final Color color;

        /**
         * @return l'icône ({@link Glyph}) associée à la catégorie de message.
         */
        Glyph buildNewGlyph() {
            Glyph glyph = FAGlyphs.getIcon(glyphEnum, color, 2.35f);
            glyph.setStyle(glyph.getStyle() + "-fx-padding:0.2em 1em 0.2em 0.1em;");
            return glyph;
        }
    }
}
