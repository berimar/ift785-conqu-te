/**
 * date 25/03/15 <br/>
 * Package destiné à la génération de messages dans le cadre de l'intéraction avec l'utilisateur.
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr 
 */
package ca.usherbrooke.domus.conq.supp.msg;