package ca.usherbrooke.domus.conq.supp.msg;

import ca.usherbrooke.domus.conq.supp.msg.FeedbackMsgHelper.Severity;
import javafx.scene.control.Button;
import lombok.NonNull;
import lombok.ToString;
import org.controlsfx.control.action.Action;

/**
 * date 22/03/15 <br/>
 * Fabrique de {@link FeedbackMsgHelper}
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@SuppressWarnings("MethodReturnOfConcreteClass")
@ToString
public class MsgHelperFactory {


    private final FeedbackMsgReactor reactor;

    /**
     * Constructeur d'une fabrique à message.
     */
    public MsgHelperFactory() {
        this.reactor = new FeedbackMsgReactor();
    }

    /**
     * @param msg           le message à transmettre
     * @param providedTitle le titre fourni
     * @return une instance de message. Pour transmettre le message, cf {@link FeedbackMsgHelper#show()}
     * @see FeedbackMsgHelper.Severity#WARN_USR
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper warner(String msg, String providedTitle) {
        return new FeedbackMsgHelper(Severity.WARN_USR, msg, providedTitle, reactor);
    }

    /**
     * @param msg le message à transmettre
     * @return une instance de message. Pour transmettre le message, cf {@link FeedbackMsgHelper#show()}
     * @see FeedbackMsgHelper.Severity#WARN_USR
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper warner(String msg) {
        return new FeedbackMsgHelper(Severity.WARN_USR, msg, reactor);
    }

    /**
     * @param msg           le message à transmettre
     * @param providedTitle le titre fourni
     * @return une instance de message. Pour transmettre le message, cf {@link FeedbackMsgHelper#show()}
     * @see FeedbackMsgHelper.Severity#INFORM_USR
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper informer(String msg, String providedTitle) {
        return new FeedbackMsgHelper(Severity.INFORM_USR, msg, providedTitle, reactor);
    }

    /**
     * @param msg le message à transmettre
     * @return une instance de message. Pour transmettre le message, cf {@link FeedbackMsgHelper#show()}
     * @see FeedbackMsgHelper.Severity#INFORM_USR
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper informer(String msg) {
        return new FeedbackMsgHelper(Severity.INFORM_USR, msg, reactor);
    }

    /**
     * @param msg           le message à transmettre
     * @param providedTitle le titre fourni
     * @return une instance de message. Pour transmettre le message, cf {@link FeedbackMsgHelper#show()}
     * @see FeedbackMsgHelper.Severity#CONFIRM_USR
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper confirmer(@NonNull String msg, String providedTitle) {
        return new FeedbackMsgHelper(Severity.CONFIRM_USR, msg, providedTitle, reactor);
    }

    /**
     * @param msg le message à transmettre
     * @return une instance de message. Pour transmettre le message, cf {@link FeedbackMsgHelper#show()}
     * @see FeedbackMsgHelper.Severity#CONFIRM_USR
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper confirmer(@NonNull String msg) {
        return new FeedbackMsgHelper(Severity.CONFIRM_USR, msg, reactor);
    }

    /**
     * @param msg           le message à transmettre
     * @param providedTitle le titre fourni
     * @return une instance de message. Pour transmettre le message, cf {@link FeedbackMsgHelper#show()}
     * @see FeedbackMsgHelper.Severity#ALERT_USR
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper alerter(@NonNull String msg, String providedTitle) {
        return new FeedbackMsgHelper(Severity.ALERT_USR, msg, providedTitle, reactor);
    }

    /**
     * @param msg le message à transmettre
     * @return une instance de message. Pour transmettre le message, cf {@link FeedbackMsgHelper#show()}
     * @see FeedbackMsgHelper.Severity#ALERT_USR
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper alerter(@NonNull String msg) {
        return new FeedbackMsgHelper(Severity.ALERT_USR, msg, reactor);
    }

    /**
     * @param msg              le message à transmettre
     * @param onCancelCallback callback appellé lorsque l'utilisateur interagit avec le bouton "annuler ?"
     * @return une instance de message. Pour transmettre le message, cf {@link FeedbackMsgHelper#show()}
     * @see FeedbackMsgHelper.Severity#CANCEL_ACTION
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper canceller(@NonNull String msg, Runnable onCancelCallback) {
        FeedbackMsgHelper feedbackMsg = new FeedbackMsgHelper(Severity.CANCEL_ACTION, msg, reactor);
        feedbackMsg.addAction(new Action("annuler ?", (event) -> {
            onCancelCallback.run();
            //hack pour cacher le bouton
            ((Button) event.getSource()).getParent().getParent().getParent().setVisible(false);
        }));
        return feedbackMsg;
    }

    /**
     * @param msg           le message à transmettre
     * @param providedTitle le titre fourni
     * @return une instance de message. Pour transmettre le message, cf {@link FeedbackMsgHelper#show()}
     * @see FeedbackMsgHelper.Severity#CANCEL_ACTION
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper canceller(@NonNull String msg, String providedTitle, Runnable onCancelCallback) {
        FeedbackMsgHelper feedbackMsg = new FeedbackMsgHelper(Severity.CANCEL_ACTION, msg, providedTitle, reactor);
        feedbackMsg.addAction(new Action("annuler ?", (event) -> {
            onCancelCallback.run();
            //hack pour cacher le bouton
            ((Button) event.getSource()).getParent().getParent().getParent().setVisible(false);
        }));
        return feedbackMsg;
    }

    /**
     * Créé un message personnalisé
     *
     * @param msg      le message à afficher
     * @param severity le niveau de sévérité du message
     * @return le message d'intéraction utilisateur
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper personalized(@NonNull String msg, Severity severity) {
        return new FeedbackMsgHelper(severity, msg, reactor);
    }

    /**
     * Créé un message personnalisé
     *
     * @param msg           le message à afficher
     * @param providedTitle le titre fourni
     * @param severity      le niveau de sévérité du message
     * @return le message d'intéraction utilisateur
     * @see FeedbackMsgHelper.Severity
     */
    public FeedbackMsgHelper personalized(@NonNull String msg, String providedTitle, Severity severity) {
        return new FeedbackMsgHelper(severity, msg, providedTitle, reactor);
    }

}
