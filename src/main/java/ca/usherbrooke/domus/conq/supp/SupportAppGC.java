package ca.usherbrooke.domus.conq.supp;

import ca.usherbrooke.domus.conq.Launcher;
import ca.usherbrooke.domus.conq.commons.TimeUtils;
import ca.usherbrooke.domus.conq.commons.app.GraphicalContext;
import ca.usherbrooke.domus.conq.commons.fx.util.*;
import ca.usherbrooke.domus.conq.supp.msg.MsgHelperFactory;
import ca.usherbrooke.domus.conq.uapps.UApp;
import ca.usherbrooke.domus.conq.uapps.UAppGC;
import javafx.application.Platform;
import javafx.beans.value.ObservableDoubleValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Labeled;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import lombok.*;
import lombok.extern.java.Log;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

import java.util.Calendar;
import java.util.Optional;

/**
 * date 12/03/15 <br/>
 * La contexte graphique de {@link ca.usherbrooke.domus.conq.supp.SupportApp}
 *
 * @author sv3inburn3(jules.randolph[at]reseau.eseo.fr)
 * @implNote C'est une implémentation du pattern Mediator entre une {@link ca.usherbrooke.domus.conq.uapps.UApp} et la {@link SupportApp}
 */
@RequiredArgsConstructor
@ToString
@Log
public class SupportAppGC implements GraphicalContext<StackPane> {

    /**
     * @see javafx.scene.Scene
     */
    @Getter(lazy = true)
    private final Scene scene = initScene();

    @Getter(lazy = true)
    private final MsgHelperFactory msgFactory = new MsgHelperFactory();

    @Getter
    private final Rectangle2D targetScreenBounds = Screen.getPrimary().getBounds();

    @Getter
    private final StackPane rootPane;


    @Getter(lazy = true, value = AccessLevel.PRIVATE)
    private final ObservableDoubleValue uAppGCMaxHeight = rootPane.heightProperty().subtract(getNavBar().heightProperty()).subtract(5);

    @Getter(lazy = true)
    private final StackPane uAppsContext = (StackPane) FXUtils.strictDeepIdLookup(rootPane, "#applicationBody").get();

    @Getter(lazy = true, value = AccessLevel.PRIVATE)
    private final HBox navBar = (HBox) FXUtils.strictDeepIdLookup(rootPane, "#navBar").get();

    /**
     * Element de support graphique du lançeur d'{@link UApp}s
     */
    @Getter(lazy = true)
    private final HBox launcherPane = (HBox) rootPane.lookup(".launcher");

    @Getter
    private Stage stage;

    @Getter
    private CoherantUXHelper coherantUXHelper;

    @Getter
    private Optional<UAppGC> focusedUAppGC = Optional.empty();


    /**
     * Effet de bord : relie le 'stage' à la {@link javafx.scene.Scene} du contexte graphique.
     *
     * @param stage l'instance de {@link javafx.stage.Stage}
     */
    public void setStage(Stage stage) {
        this.stage = stage;
        this.coherantUXHelper = new CoherantUXHelper(stage, getRootPane());
        getRootPane().setVisible(false);
        this.stage.setX(targetScreenBounds.getMinX());
        this.stage.setY(targetScreenBounds.getMinY());
        this.stage.setWidth(targetScreenBounds.getWidth());
        this.stage.setHeight(targetScreenBounds.getHeight());
        this.stage.setScene(getScene());
        getUAppsContext().maxHeightProperty().bind(getUAppGCMaxHeight());
        Platform.runLater(()-> getRootPane().setVisible(true));
        Glyph node = FAGlyphs.getIcon(FontAwesome.Glyph.QUESTION_CIRCLE, Color.LAWNGREEN);
        node.prefWidthProperty().bind(stage.heightProperty().divide(20));
    }

    @Override
    public void show() {
        stage.setIconified(false);
    }

    @Override
    public void hide() {
        stage.setIconified(true);
    }

    public void hideFocusedUApp() {
        focusedUAppGC.ifPresent(this::hideUApp);
    }

    /**
     * Initialise la scène principale du contexte graphique. Lance le thread qui met à jour l'heure et la date.
     * @return la scène principale
     */
    private Scene initScene() {
        coherantUXHelper.addGlyphToButtonClasses(GlyphHelper.PARAMS_HELPER);

        // Thread qui met à jour l'heure et la date
        Task<Void> task = new Task<Void>() {
            final Labeled heureApp = FXUtils.castAndSetLabeled(FXUtils.safeDeepIdLookup(rootPane, "#heureHautApplication").get(), TimeUtils.getFormattedTime());
            final Labeled dateApp = FXUtils.castAndSetLabeled(FXUtils.safeDeepIdLookup(rootPane, "#dateHautApplication").get(), TimeUtils.getFormattedDate(Calendar.getInstance()));
            final Calendar instance = Calendar.getInstance();

            @Override
            public Void call() throws Exception {

                while (true) {
                    Platform.runLater(() -> {
                        heureApp.setText(TimeUtils.getFormattedTime());
                        dateApp.setText(TimeUtils.getFormattedDate(instance));
                    });
                    Thread.sleep(5000);
                }
            }
        };
        Thread th = new Thread(task);
        th.setDaemon(true);
        th.start();

        Scene scene1 = new Scene(getRootPane());
        scene1.getStylesheets().add(Launcher.class.getResource("DefaultTheme.css").toExternalForm());
        return scene1;
    }

    /**
     * Instancie un bouton par {@link UApp} dans le launcher.
     */
    public void loadButtons() {
        //TODO refactoriser en créant une classe UAppsLauncher et un fxml associé. Cette classe aura une méthode addUApp qui fera tout ce travail.

        final ObservableList<Node> children = getLauncherPane().getChildren();
        SupportApp.get().getUApps().values().forEach((uapp) -> {
            Button button = new Button();
            children.add(button);

            button.setGraphic(uapp.getAppGC().getGlyphApp());
            button.setContentDisplay(ContentDisplay.TOP);
            button.getStyleClass().add("notYetConnected");
            button.getStyleClass().add("uAppButton");
            button.setDisable(true);

            uapp.getAppGC().bindButton(button);
        });


    }

    /**
     * Charge au premier plan l'application ({@link ca.usherbrooke.domus.conq.uapps.UApp}) dont le nom de classe est passé en paramètre.
     *
     * @param uAppGC le contexte graphique de l'application qu'on souhaite rendre visible
     * @throws java.lang.IllegalStateException si aucune application enregistrée ne porte ce nom.
     */
    public void showUApp(@NonNull UAppGC uAppGC) {
        if (focusedUAppGC.isPresent() && focusedUAppGC.get() != uAppGC) {
            focusedUAppGC.get().hide();
        }
        focusedUAppGC = Optional.of(uAppGC);
        uAppGC.getRootPane().maxHeightProperty().bind(FXSizer.UAPP_HEIGHT);
        getUAppsContext().getChildren().set(0, uAppGC.getRootPane());
        uAppGC.onFocusGained();
    }

    /**
     * @param uAppGC le contexte graphique de l'application qu'on souhaite cacher
     * @throws java.lang.IllegalStateException si aucune application enregistrée ne porte ce nom.
     * @implNote Pour le moment l'argument est inutile car le contexte n'est pas multi-fenestré. Mais à terme ça le sera.
     */
    public void hideUApp(@NonNull UAppGC uAppGC) {
        HBox defaultPane = new HBox();
        defaultPane.getStyleClass().addAll("applicationPane", "applicationPaneNull");
        defaultPane.setPrefHeight(100.0);
        defaultPane.setPrefWidth(200.0);
        defaultPane.setAlignment(Pos.CENTER);

        getUAppsContext().getChildren().set(0, defaultPane);
        uAppGC.onFocusLost();
    }
}
