package ca.usherbrooke.domus.conq.supp.viewctrl;

import ca.usherbrooke.domus.conq.commons.fx.util.FAGlyphs;
import ca.usherbrooke.domus.conq.commons.fx.util.GraphicsUtils;
import ca.usherbrooke.domus.conq.db.model.NewsEntity;
import ca.usherbrooke.domus.conq.db.model.UserEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.java.Log;
import org.controlsfx.control.PopOver;
import org.controlsfx.glyphfont.FontAwesome;

import java.util.List;

/**
 * date 08/03/15
 * <p>
 * Controlleur de l'application support (tout ce qui est commun aux uApps, c'est-à-dire la partie autour de la zone accordée
 * à une uApp)
 * <br />
 * S'occupe de l'ouverture/fermeture des panneaux glissant, de la connection/déconnection et de l'affichage des news pour
 * un utilisateur donné.
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@SuppressWarnings("NumericCastThatLosesPrecision")
@ToString
@Log
public class SupportAppController extends GenericSupportAppController {

    @Getter
    @FXML private StackPane rootPane;
    @FXML private VBox paramsPanel;
    @FXML private ToggleButton openParamsButton;
    @FXML private ToggleButton fullscreenButton;

    @FXML private ToggleButton buttonOpenMapPane;
    @FXML private BorderPane mapPane;
    @FXML private ImageView imageMap;

    @FXML private TextField login;
    @FXML private PasswordField password;
    @FXML private Label connectedAs;

    @FXML private Label testNews;
    @FXML private VBox newsVbox;
    @FXML private HBox newsHbox;
    @FXML private ScrollPane scrollNews;

    private List<NewsEntity> listNews;


    @FXML public void initialize() {
        Platform.runLater(() -> {
            if (getBoundApp().isLogged()) {
                logIn();
            }
        });

        // Listener pour la fermeture automatique du panneau de paramètres (quand on connecte/déconnecte par exemple)
        paramsPanel.prefWidthProperty().addListener((observable, oldValue, newValue) -> {
            if((newValue.intValue() <= 0) && (paramsPanel.getPrefWidth() >= 0)) {
                GraphicsUtils.showHidePane(openParamsButton.translateXProperty(), 0, 10.0);
            }
        });
    }


    /**
     * Ouvre ou ferme le panneau de la carte
     */
    @SuppressWarnings("MagicNumber")
    @FXML public void getMapPanel() {
        if (mapPane.getMinWidth() <= 80) {
            // Show
            double widthMax = SupportApp.get().getAppGC().getRootPane().getWidth();
            imageMap.setFitWidth(widthMax * 0.75);
            GraphicsUtils.showHidePane(mapPane.minWidthProperty(), (int) (widthMax * 0.80), 1000.0);
        } else {
            // Hide
            GraphicsUtils.showHidePane(mapPane.minWidthProperty(), 80, 1000.0);
        }
    }


    /**
     * Ouvre ou ferme le panneau de paramètres
     */
    @SuppressWarnings("MagicNumber")
    @FXML public void getParametersPanel() {
        if (paramsPanel.getPrefWidth() <= 0) {
            GraphicsUtils.showHidePane(openParamsButton.translateXProperty(), 225, 500.0);
            GraphicsUtils.showHidePane(paramsPanel.prefWidthProperty(), 225, 500.0);
        } else {
            GraphicsUtils.showHidePane(openParamsButton.translateXProperty(), 0, 500.0);
            GraphicsUtils.showHidePane(paramsPanel.prefWidthProperty(), 0, 500.0);
        }
    }


    /**
     * Bouton : fullscreenButton
     * Met l'application en plein écran si elle ne l'est pas déjà et change le bouton (fullscreen>minimize)
     * Si l'application est déjà en plein écran, alors l'application est minimisée et le bouton est changé pour fullscreen (minimize>fullscreen)
     */
    @FXML public void fullscreen() {
        if (getBoundApp().getPrimaryStage().isFullScreen()) {
            getBoundApp().getPrimaryStage().setFullScreen(false);
            fullscreenButton.getStyleClass().remove("minimizeButton");
            fullscreenButton.getStyleClass().add("fullscreenButton");
            fullscreenButton.setText("Plein écran");
        } else {
            getBoundApp().getPrimaryStage().setFullScreen(true);
            fullscreenButton.getStyleClass().remove("fullscreenButton");
            fullscreenButton.getStyleClass().add("minimizeButton");
            fullscreenButton.setText("Minimiser");
        }
    }

    /**
     * Méthode appelée lorsqu'on appuie sur ESCAPE, pour changer le bouton lorsque le plein écran est enlevé
     * @param event
     */
    @FXML public void exitFullscreen(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ESCAPE)) {
            fullscreenButton.getStyleClass().remove("minimizeButton");
            fullscreenButton.getStyleClass().add("fullscreenButton");
            fullscreenButton.setText("Plein écran");
        }
    }


    /**
     * Bouton : exitButton
     * Ferme l'application
     */
    @FXML public void exitApp() {
            PopOver pop = GraphicsUtils.createOkCancelDialog("Êtes vous sur?", "Vous êtes sur le point de quiter l'application",
                () -> {
                    getBoundApp().stop();
                    System.exit(0);
                }
        );
        pop.show(getBoundApp().getPrimaryStage());
    }


    /**
     * Bouton : parametersButton
     * Ouvre les paramètres
     */
    @FXML public void openParams() {
        log.info("Ouverture de paramètres");

        getBoundApp().getMsgFactory().informer("À venir bientôt.").show();
        // TODO : décider quels sont les paramètres, et les afficher
    }


    /**
     * Méthode appelée lorsqu'on clique sur le bouton pour se connecter
     */
    @FXML public void connectUser() {
        if (checkUser(login.getText(), password.getText())) {
            logIn();
        }
    }

    /**
     * Méthode appelée lorsque l'utilisateur appuie sur ENTER lorsqu'il est dans le champs de connection (raccourcis clavier)
     * @param event l'évènement de la touche
     */
    @FXML public void connectUserEnter(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            connectUser();
        }
    }

    /**
     * Méthode qui connecte un utilisateur dont le login/mot de passe a déjà été vérifié et affiche les informations (news)
     * reliées à l'utilisateur
     */
    public void logIn() {
        connectedAs.setText(getBoundApp().getUserEntity().getPseudo());
        login.setText("");
        password.clear();
        listNews = NewsEntity.DAO.getAll();
        showNews(listNews);
    }


    /**
     * Bouton : "Se déconnecter".
     * Fait apparaître un dialog (menu contextuel) pour confirmer l'intention de l'utilisateur
     */
    @FXML
    public void disconnectUser() {
        PopOver pop = GraphicsUtils.createOkCancelDialog("Êtes vous sur?", "En vous déconnectant, les applications vont être fermées",
                () -> {
                    getBoundApp().logOut();
                    connectedAs.setText("Déconnecté");
                }
        );
        pop.show(getBoundApp().getPrimaryStage());

    }


    /**
     * Vérifie que l'input entré par l'utilisateur pour se connecter est correct et qu'il existe en base de données
     * @param loginToCheck le login entré
     * @param passwordToCheck le password entré
     * @return true si l'utilisateur/pass est correct, false sinon (et affiche un message)
     */
    private boolean checkUser(String loginToCheck, String passwordToCheck) {
        boolean isAMatch = false;

        // Vérification pour les caractères autorisés
        if (!loginToCheck.matches("^\\w+$") || !passwordToCheck.matches("^[\\w\\-]+$")) {
            getBoundApp().getMsgFactory().warner("Caractères non autorisés.").show();
        } else {
            // Vérification de la combinaison login/password
            UserEntity user = UserEntity.DAO.getByCriteria("pseudo", loginToCheck);

            if ((user != null) && (user.getPassword().equals(passwordToCheck))) {
                isAMatch = true;
                getBoundApp().logIn(user);
            } else {
                login.setText("");
                password.clear();

                getBoundApp().getMsgFactory().warner("Mauvais nom d'utilisateur ou mot de passe").show();
            }
        }

        // TODO : compter le nombre d'essai et bloquer après un certain seuil ?

        return isAMatch;
    }


    /**
     * Affiche les news données en argument dans le panneau de droite, en préparant la disparition de la news lorsqu'on clique dessus
     * @param list la liste des news à afficher pour un user donné
     */
    private void showNews(List<NewsEntity> list) {

        newsVbox.setPrefWidth(scrollNews.getWidth() - 2);

        for (NewsEntity e : list) {

            HBox newsHbox = new HBox();
            newsHbox.getStyleClass().add("oneNews");

            // Image rouge avec une cloche
            Button notifImage = new Button();
            notifImage.setGraphic(FAGlyphs.getIcon(FontAwesome.Glyph.BELL, Color.WHITE));
            notifImage.getStyleClass().add("newsPic");
            notifImage.setDisable(true);


            VBox newsVboxInside = new VBox();

            Label newsLabelObject = new Label();
            newsLabelObject.getStyleClass().add("newsTitle");

            Label newsLabelDescription = new Label();
            Label newsLabelCreateDate = new Label();


            newsLabelObject.setText(e.getObject());
            newsLabelDescription.setText(e.getDescription());
            newsLabelCreateDate.setText(e.getCreateDate());


            //Gestion de l'évènement lorsque l'on clique sur une notification.
            newsHbox.setOnMouseClicked(event -> newsVbox.getChildren().remove(newsHbox));
            newsHbox.getChildren().add(notifImage);
            newsVboxInside.getChildren().add(newsLabelObject);
            newsVboxInside.getChildren().add(newsLabelDescription);
            newsVboxInside.getChildren().add(newsLabelCreateDate);
            newsHbox.getChildren().add(newsVboxInside);
            newsVbox.getChildren().add(newsHbox);

        }
    }
}
