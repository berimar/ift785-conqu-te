package ca.usherbrooke.domus.conq.supp.news;

import ca.usherbrooke.domus.conq.db.model.NewsEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.java.Log;


/**
 * Une classe pour les utilitaires liés à l'actualités
 *
 * @author Imad
 */

@Setter
@Getter
@ToString
@Log
public class NewsUtil {

    private NewsUtil(){}
    /**
     * Méthode permettant d'ajouter une notification dans la table news.*
     */
    public static synchronized void addNews(String createdDate, String title, String description) {
        NewsEntity news = new NewsEntity();
        news.setCreateDate(createdDate);
        news.setObject(title);
        news.setDescription(description);
        NewsEntity.DAO.persist(news);
    }
}
