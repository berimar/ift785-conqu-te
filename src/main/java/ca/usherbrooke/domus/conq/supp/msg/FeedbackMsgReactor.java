package ca.usherbrooke.domus.conq.supp.msg;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.java.Log;


/**
 * date 20/03/15 <br/>
 * "Réacteur" gérant l'affichage de messages de feedback destinés à l'utilisateur
 *
 * @author sv3inburn3
 */
@ToString
@Log
class FeedbackMsgReactor {

    public static final int MAX_STACKED_FEEDBCK_MSGS = 30;


    private final ObservableList<FeedbackMsgHelper> displayedMsgs = FXCollections.observableArrayList();


    /**
     * Ajoute le message à la liste de notifications
     * @param feedbackMsg le message à transmettre
     */
    void push(@NonNull FeedbackMsgHelper feedbackMsg) {
        displayedMsgs.add(feedbackMsg);

    }

    /**
     * Affiche la notification donnée en argument
     * @param msg la notification à afficher
     */
    public void show(FeedbackMsgHelper msg) {
        Platform.runLater(() -> msg.getFxmsgui().show());
    }

    FeedbackMsgReactor() {
        displayedMsgs.addListener((ListChangeListener<FeedbackMsgHelper>) (listnr) -> {
            while (listnr.next()) {
                listnr.getAddedSubList().forEach((msg) -> msg.getDelayableObservable().subscribe());
            }
        });
    }

    /**
     * Enlève la notification de la liste de notifications
     * @param fbm le message à enlever de la liste
     */
    public void remove(FeedbackMsgHelper fbm) {
        displayedMsgs.remove(fbm);
        log.fine("Message retiré :  " + fbm.toString());
    }
}
