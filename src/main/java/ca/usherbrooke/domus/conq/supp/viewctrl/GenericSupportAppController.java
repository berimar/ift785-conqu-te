package ca.usherbrooke.domus.conq.supp.viewctrl;

import ca.usherbrooke.domus.conq.commons.app.AppController;
import ca.usherbrooke.domus.conq.supp.SupportApp;

/**
 * Classe abstraite générique pour le controller de SupportApp
 *
 * date 10/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class GenericSupportAppController extends AppController<SupportApp> {

    @Override
    protected SupportApp getBoundApp() {
        return SupportApp.get();
    }

}
