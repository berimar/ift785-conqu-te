package ca.usherbrooke.domus.conq.supp;

import ca.usherbrooke.domus.conq.commons.app.GraphicalLCApp;
import ca.usherbrooke.domus.conq.commons.app.LCApp;
import ca.usherbrooke.domus.conq.commons.fx.util.FXUtils;
import ca.usherbrooke.domus.conq.db.model.UserEntity;
import ca.usherbrooke.domus.conq.supp.msg.MsgHelperFactory;
import ca.usherbrooke.domus.conq.uapps.UApp;
import javafx.application.Platform;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.java.Log;

import java.util.Collections;
import java.util.Map;
import java.util.TimerTask;

/**
 * --- Singleton ---
 * <br />
 * L'application support.
 * Elle est responsable de lancer le contexte graphique, charger le lanceurs des {@link UApp}s,
 * charger le portail, le fil d'actualités et la barre de menu.
 *
 * @author sv3inburn3(jules.randolph[at]reseau.eseo.fr)
 */
@SuppressWarnings("MethodReturnOfConcreteClass")
@Log
@ToString
public class SupportApp extends GraphicalLCApp<SupportAppGC> {

    @Getter
    private UserEntity userEntity;

    public enum State {
        USR_LOGGED_IN,
        USR_LOGGED_OUT
    }

    private static final SupportApp SINGLETON = new SupportApp();

    private final Map<String, UApp> registeredUApps;

    @Getter
    private final String name = SupportApp.class.getSimpleName();


    /**
     * Contexte graphique de l'application
     */
    @Getter(lazy = true)
    private final SupportAppGC  appGC = new SupportAppGC(FXUtils.loadFXML(SupportApp.class, "viewctrl/SupportAppLayout.fxml"));


    @Getter(lazy = true)
    private final MsgHelperFactory msgFactory = getAppGC().getMsgFactory();

    @Getter
    private Stage primaryStage;

    /**
     * @param userEntity l'utilisateur s'étant authentifié sur l'application
     * @return {@code true} si l'authentification a été couronnée de succès
     */
    public void logIn(@NonNull UserEntity userEntity) {
        this.userEntity = userEntity;
        Platform.runLater(() -> {
            getAppGC().enableAllButtons(".notYetConnected");
            getAppGC().swap(".disconnected", ".connected");
            getAppGC().getRootPane().lookup(".connectionDisplay").setVisible(false);
            //contourne un bug qui fait afficher le message en haut à gauche lorsqu'on
            //autolog au démarrage
            new java.util.Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    getMsgFactory().confirmer("Connexion avec succès").show();
                }
            },500);
        });
    }

    /**
     * Déconecte l'utilisateur courant
     */
    public void logOut() {
        userEntity = null;
        Platform.runLater(() -> {
            getMsgFactory().confirmer("Déconnection").show();
            getAppGC().hideFocusedUApp();
            getUApps().values().forEach(LCApp::stop);
            getAppGC().disableAllButtons(".notYetConnected");
            getAppGC().swap(".connected", ".disconnected");
            getAppGC().getRootPane().lookup(".connectionDisplay").setVisible(true);
        });
    }

    /**
     * @return si un utilisateur est connecté
     */
    public boolean isLogged() {
        return userEntity != null;
    }


    /**
     * Constructeur privé
     * <br />
     * Recherche les applications enregistrées (pas Ignorées) et les enregistre dans une liste
     */
    private SupportApp() {
        registeredUApps = new UAppsFinder(this.getAppGC()).lookup(UApp.class.getPackage());
        log.info(registeredUApps.values().stream()
                .map(UApp::getName)
                .reduce("Micro Applications trouvées : ", (name1, name2) -> name1 + " " + name2));
    }


    /**
     * @return l'instance du singleton
     * @throws IllegalStateException si l'application n'a pas été initialisée.
     * @see #init(javafx.stage.Stage)
     */
    public static SupportApp get() {
        if (SINGLETON.primaryStage != null)
            return SINGLETON;
        else throw new IllegalStateException("L'application n'est pas encore initialisée");
    }

    /**
     * Initialise l'application.
     *
     * @param stage le contexte graphique de l'application <br/>
     *              <img src="http://code.makery.ch/assets/library/javafx-8-tutorial/part1/javafx-hierarchy.png"/>
     * @return l'instance unique
     * @see javafx.stage.Stage
     */
    public static SupportApp init(@NonNull final Stage stage) {
        // Nope, still not working for me.
        /*stage.getIcons().add(ImageLoader.load("home-icon.png"));
        stage.getIcons().add(ImageLoader.load("icon-try.png"));*/

        SINGLETON.primaryStage = stage;
        // Le runLater() est ce qui me donnait les exceptions, ça, prefHeight=Infinity et prefWidth=Infinity dans SupportAppLayout.
//        Platform.runLater(() -> {
            SINGLETON.getAppGC().loadButtons();
            stage.setTitle("superviseur conquête");
            SINGLETON.getAppGC().setStage(stage);
//        });
        return SINGLETON;
    }

    /**
     * @return une {@link Map} en lecture seule de l'ensemble des {@link UApp}s enregistreés dont les clés correspondent
     * à {@link UApp#getName()}
     */
    public Map<String, UApp> getUApps() {
        //décorateur en lecture seule
        return Collections.unmodifiableMap(registeredUApps);
    }

    /**
     * Fermeture de toutes les applications lorsque la commande stop() est reçue
     */
    @Override
    protected void onStop() {
        registeredUApps.values().forEach(UApp::stop);
    }

    /**
     * Affichage de la fenêtre lorsque la commande start() est reçue
     */
    @Override
    protected void onStart() {
        primaryStage.show();
    }

}
