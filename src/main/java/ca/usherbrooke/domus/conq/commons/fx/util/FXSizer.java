package ca.usherbrooke.domus.conq.commons.fx.util;

import ca.usherbrooke.domus.conq.supp.SupportApp;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.ReadOnlyDoubleProperty;

/**
 * date 25/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@SuppressWarnings("MagicNumber")
public class FXSizer {

    private FXSizer(){}

    public static final ReadOnlyDoubleProperty UAPP_WIDTH = SupportApp.get().getAppGC().getUAppsContext().widthProperty();
    public static final ReadOnlyDoubleProperty UAPP_HEIGHT = SupportApp.get().getAppGC().getUAppsContext().heightProperty();
    public static final ReadOnlyDoubleProperty ROOT_WIDTH = SupportApp.get().getAppGC().getRootPane().widthProperty();
    public static final ReadOnlyDoubleProperty ROOT_HEIGHT = SupportApp.get().getAppGC().getRootPane().heightProperty();
    public static final DoubleBinding PREZ_WIDTH = UAPP_WIDTH.multiply(0.32d);
    public static final DoubleBinding PREZ_FIELD_WIDTH = UAPP_WIDTH.multiply(0.3d).multiply(0.8d);
    public static final DoubleBinding PREF_BUTTON_BOX_HEIGHT = FXSizer.UAPP_HEIGHT.multiply(0.07d);
    public static final DoubleBinding PREF_PREZ_BODY_HEIGHT = FXSizer.UAPP_HEIGHT.multiply(0.93d);

    public static Double getPrefButtonboxHeight() {return PREF_BUTTON_BOX_HEIGHT.get();}

    public static Double getPrezWidth(){
        return PREZ_WIDTH.get();
    }

    public static Double getCheckListWidth(){
        return getPrezWidth()*0.65;
    }

    public static Double getPrezFieldWidth(){
        return PREZ_FIELD_WIDTH.get();
    }

    public static Double get30PrcPrezWidth(){
        return getPrezWidth()*0.3;
    }

    public static Double get40PrcPrezWidth(){
        return getPrezWidth()*0.4;
    }

    public static Double get10PrcUappHeight(){
        return UAPP_HEIGHT.get()*0.1;
    }

    public static Double get15PrcUappHeight(){
        return UAPP_HEIGHT.get()*0.14;
    }

    public static Double get20PrcUappHeight(){
        return UAPP_HEIGHT.get()*0.2;
    }

    public static Double get30PrcUappHeight(){
        return UAPP_HEIGHT.get()*0.3;
    }

    public static Double get40PrcUappHeight(){
        return UAPP_HEIGHT.get()*0.4;
    }

    public static Double get50PrcUappHeight(){
        return UAPP_HEIGHT.get()*0.5;
    }

    public static Double get60PrcUappHeight(){
        return UAPP_HEIGHT.get()*0.6;
    }

    public static Double get70PrcUappHeight(){
        return UAPP_HEIGHT.get()*0.7;
    }

    public static Double get80PrcUappHeight(){
        return UAPP_HEIGHT.get()*0.8;
    }

    public static Double get87PrcUappHeight(){
        return UAPP_HEIGHT.get()*0.87;
    }

    public static Double get90PrcUappHeight(){
        return UAPP_HEIGHT.get()*0.9;
    }

    public static Double getUappHeight(){
        return UAPP_HEIGHT.get();
    }
}
