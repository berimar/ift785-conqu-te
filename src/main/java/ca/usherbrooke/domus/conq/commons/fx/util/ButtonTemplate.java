package ca.usherbrooke.domus.conq.commons.fx.util;

import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import org.controlsfx.glyphfont.FontAwesome;

import java.util.ArrayList;

/**
 * date 13/04/15 <br/>
 * Template utilisé pour instancier des bouttons.
 * Utiliser le builder associer pour construire des instances.
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Data
@Builder(builderClassName = "Builder")
public class ButtonTemplate {
    @NonNull
    private final FontAwesome.Glyph glyph;

    /**
     * Handler d'une action
     */
    private final Runnable onAction;
    @NonNull
    private final Color color;
    private ArrayList<String> styleClassesArray;
    private String styleClassesComaSeparated;
    private String styleClass;
    private String label;
    /**
     * L'identifiant css unique sans le préfixe '#'
     */
    private String cssId;
    private String tooltip;


    /**
     * Construit un nouveau bouton à partir du template
     *
     * @return
     */
    public Button toButton() {
        Button button = new Button();
        if (onAction != null) {
            button.setOnAction(e -> onAction.run());
        }
        button.setGraphic(FAGlyphs.getIcon(glyph, color));
        button.setText(label);
        if (styleClassesArray != null) {
            button.getStyleClass().addAll(styleClassesArray);
        }
        if (styleClass != null) {
            button.getStyleClass().add(styleClass);
        }
        if (cssId != null) {
            button.setId(cssId);
        }
        if (styleClassesComaSeparated != null) {
            button.getStyleClass().addAll(styleClassesComaSeparated.split(","));
        }
        if (tooltip != null) {
            button.setTooltip(new Tooltip(tooltip));
        }
        return button;
    }

    /**
     * Créé un builder à partir d'un {@code GlyphHelper}
     *
     * @param glyphHelper le glyphHelper
     * @return un builder
     * @implNote pour ajouter des classes css, utiliser {@link ButtonTemplate.Builder#styleClassesArray(java.util.ArrayList)} ou {@link ButtonTemplateBuilder#styleClassesComaSeparated(String)}  et non {@link ButtonTemplateBuilder#styleClass(String)}
     */
    public static ButtonTemplate.Builder of(GlyphHelper glyphHelper) {
        return builder()
                .glyph(glyphHelper.getGlyph())
                .color(glyphHelper.getButtonColor())
                .styleClass(glyphHelper.getCssClassSelector().substring(1));
    }
}
