package ca.usherbrooke.domus.conq.commons.fx.internal;

import ca.usherbrooke.domus.conq.commons.fx.util.FXSizer;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.ValidatorHelper;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Optional;

/**
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public abstract class Presentation extends AnchorPane implements FxmlBindable {

    @FXML
    protected BorderPane wrapper;
    @FXML
    protected HBox buttonsBox;

    protected final Button help = GlyphHelper.HELP_HELPER.newButton();
    protected ArrayList<Button> buttons = new ArrayList<>(5);
    protected Optional<PresentationBody> presentationBody = Optional.empty();
    protected final BooleanProperty isShowing = new SimpleBooleanProperty();

    protected void setButtons(PresentationDelegate delegate) {
        buttonsBox.getChildren().clear();
        buttons.clear();
        buttons.addAll(delegate.getButtons());
        buttonsBox.getChildren().addAll(buttons);
    }

    public Presentation() {
        bindToFXML();
        //ajout d'un listener permettant d'aller automatiquement chercher un nouveau PresentationBody s'il est présent,
        //ou appeller onHiding sinon
        isShowing.addListener((observable, oldValue, newValue) -> {
            PresentationDelegate prezDelegate = presentationBody.get().getPrezDelegate();
            if (oldValue && !newValue) {
                boolean wasPresent = prezDelegate.getNextPresentationBody().isPresent();
                prezDelegate.getNextPresentationBody().ifPresent(this::show);
                if (!wasPresent) {
                    isShowing.unbind();
                    onHiding();
                }
            }
        });
    }

    @FXML
    public final void initialize() {
        buttons = new ArrayList<>(5);
        help.setOnAction(this::showHelp);
        getChildren().add(help);
        AnchorPane.setRightAnchor(help, getWidtheOffset() / 2);
        AnchorPane.setTopAnchor(help, 1d);
        onInitialize();
        wrapper.maxHeightProperty().bind(FXSizer.UAPP_HEIGHT);
        wrapper.prefHeightProperty().bind(FXSizer.UAPP_HEIGHT);
        buttonsBox.prefHeightProperty().bind(FXSizer.PREF_BUTTON_BOX_HEIGHT);
    }

    public final void show(@NonNull PresentationBody menu) {
        if (!presentationBody.isPresent() || presentationBody.get() != menu) {
            presentationBody = Optional.of(menu);
            buttonsBox.maxWidthProperty().bind(menu.prefWidthProperty());
        }
        menu.getPrezDelegate().getIsShowingProperty().setValue(true);
        showing();

    }


    private void showing() {
        isShowing.bind(presentationBody.get().getPrezDelegate().getIsShowingProperty());
        setButtons(presentationBody.get().getPrezDelegate());
        insertPresentationBody(presentationBody.get());
        insertGlyphs(presentationBody.get().getGlyphHelpers());
        insertValidationSupport(presentationBody.get().getValidatorHelpers());
    }

    public boolean isShowing() {
        return wrapper.getCenter() != null;
    }


    protected void onHiding() {}

    protected void onInitialize() {}

    private void showHelp(ActionEvent event) {
        presentationBody.get().getCoherantUXHelper().toggleAllButtonTooltips();
        presentationBody.get().getCoherantUXHelper().toggleTooltips(buttons);
    }

    private void insertGlyphs(GlyphHelper... glyphHelpers) {
        presentationBody.get().getCoherantUXHelper().addGlyphToButtonClasses(glyphHelpers);
    }

    private void insertValidationSupport(ValidatorHelper... helpers) {
        presentationBody.get().getCoherantUXHelper().addValidationSupport(helpers);
    }

    protected abstract void insertPresentationBody(PresentationBody body);

    protected abstract Double getWidtheOffset();

    @Override
    public final String getFXMLPath() {
        return "Presentation.fxml";
    }


}
