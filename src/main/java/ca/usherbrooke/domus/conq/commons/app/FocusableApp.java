package ca.usherbrooke.domus.conq.commons.app;

/**
 * date :  15-04-05 <br>
 * Interface définissant deux méthodes appellée par le contexte graphique "maitre" sur le contexte graphique
 * "esclave" lorsque l'état de visiblilité est modifié.
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public interface FocusableApp {

    /**
     * Appellée lorsque le contexte graphique "esclave" est affiché
     */
    default void onFocusGained() {
    }

    /**
     * Appellée lorsque le contexte graphique "esclave" est caché
     */
    default void onFocusLost() {
    }
}
