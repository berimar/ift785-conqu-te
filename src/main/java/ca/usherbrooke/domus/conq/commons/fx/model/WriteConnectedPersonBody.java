package ca.usherbrooke.domus.conq.commons.fx.model;

import ca.usherbrooke.domus.conq.commons.CommonRegex;
import ca.usherbrooke.domus.conq.commons.fx.util.FXSizer;
import ca.usherbrooke.domus.conq.db.model.ConnectedPerson;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public abstract class WriteConnectedPersonBody extends WritePersonBody {

    @FXML
    protected TextField emailField;

    /**
     * Charge le contenu des champs dans l'entité passée en paramètre
     * @param connectedPerson l'entité qui va être modifiée
     */
    protected void commitConnectedPersonFields(ConnectedPerson connectedPerson) {
        commitPersonFields(connectedPerson);
        connectedPerson.setEmailAddress(emailField.getText());
    }

    /**
     * Efface les champs du formulaire
     */
    protected void clearConnectedPersonFields() {
        clearPersonFields();
        emailField.clear();
        setPrefSizeFields();
    }

    /**
     * Remplit les champs du formulaire à partir de l'entité passée en paramètre
     * @param connectedPerson l'entité à partir de laquelle les champs seront écrit
     */
    protected void uploadConnectedPersonFields(ConnectedPerson connectedPerson) {
        uploadPersonFields(connectedPerson);
        emailField.setText(connectedPerson.getEmailAddress());
        setPrefSizeFields();
    }


    /**
     * Vérifie que chaque champs de la {@code ConnectedPerson} est valide, et renvoie {code true} le cas échéant
     * @return {@code true} si tous les champs sont valides
     */
    protected boolean checkConnectedPersonInput() {
        boolean isWellFormed = true;
        if (!emailField.getText().matches(CommonRegex.EMAIL_REGEX)) {
            SupportApp.get().getMsgFactory().warner("L'adresse courriel n'est pas valide").show();
            isWellFormed = false;
        }
        return isWellFormed && checkPersonInput();
    }

    private void setPrefSizeFields(){
        emailField.setMaxWidth(FXSizer.getPrezFieldWidth());
    }

}
