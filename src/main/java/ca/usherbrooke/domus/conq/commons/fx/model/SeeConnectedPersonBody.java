package ca.usherbrooke.domus.conq.commons.fx.model;

import ca.usherbrooke.domus.conq.commons.fx.EmailLabel;
import ca.usherbrooke.domus.conq.commons.fx.util.FXSizer;
import ca.usherbrooke.domus.conq.db.model.ConnectedPerson;
import javafx.fxml.FXML;

/**
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public abstract class SeeConnectedPersonBody extends SeePersonBody {
    @FXML
    protected EmailLabel emailLabel;

    /**
     * Met à jour les champs à partir de l'entité passée en paramètre
     * @param connectedPerson l'entité à partir de laquelle les champs seront mis à jour.
     */
    protected void updateConnectedPersonFields(ConnectedPerson connectedPerson) {
        super.updatePersonFields(connectedPerson);
        emailLabel.setText(connectedPerson == null ? "" : connectedPerson.getEmailAddress());
        setPrefSizeFields();
    }

    private void setPrefSizeFields(){
        emailLabel.setPrefWidth(FXSizer.getPrezFieldWidth());
    }
}
