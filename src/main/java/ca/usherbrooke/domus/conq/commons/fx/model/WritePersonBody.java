package ca.usherbrooke.domus.conq.commons.fx.model;

import ca.usherbrooke.domus.conq.commons.CommonRegex;
import ca.usherbrooke.domus.conq.commons.fx.ExtendedImageView;
import ca.usherbrooke.domus.conq.commons.fx.internal.PictureResourceHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.FXSizer;
import ca.usherbrooke.domus.conq.commons.fx.internal.PresentationBody;
import ca.usherbrooke.domus.conq.commons.fx.internal.PictureResourceHelper.*;
import ca.usherbrooke.domus.conq.db.model.Person;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.java.Log;

import java.io.File;

/**
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public abstract class WritePersonBody extends PresentationBody {

    @FXML
    protected TextField photoPathField;
    @FXML
    protected TextField lastNameField;
    @FXML
    protected TextField firstNameField;
    @FXML
    protected TextField phoneField;
    @FXML
    protected ExtendedImageView imageView;

    @Getter(value = AccessLevel.PRIVATE, lazy = true)
    private final PictureResourceHelper pictureResourceHelper = new PictureResourceHelper(imageView);

    /**
     * Remplit les champs du formulaire à partir de l'entité passée en paramètre
     * @param person l'entité à partir de laquelle les champs seront écrit
     */
    protected void uploadPersonFields(Person person) {
        lastNameField.setText(person.getLastName());
        firstNameField.setText(person.getFirstName());
        phoneField.setText(person.getPhoneNumber());
        photoPathField.setText(person.getPhotoUri());
        this.getPictureResourceHelper().loadImageAsync(person.getPhotoUri(), OnErrorStrategy.WARN_USER);
        setPrefSizeFields();
    }

    /**
     * Efface les champs du formulaire
     */
    protected void clearPersonFields() {
        photoPathField.clear();
        lastNameField.clear();
        firstNameField.clear();
        phoneField.clear();
        this.getPictureResourceHelper().setDefaultImage();
        setPrefSizeFields();
    }

    /**
     * Charge le contenu des champs dans l'entité passée en paramètre
     * @param person l'entité qui va être modifiée
     */
    protected void commitPersonFields(Person person) {
        person.setLastName(lastNameField.getText());
        person.setFirstName(firstNameField.getText());
        person.setPhoneNumber(phoneField.getText());
        person.setPhotoUri(photoPathField.getText());
    }

    @FXML
    public void choosePhoto() {
        log.info("Choose Photo");
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image files (*.jpg, *.png)", "*.jpg", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(SupportApp.get().getAppGC().getStage());
        if(checkPictureExists(file)){
            photoPathField.setText(file.getAbsolutePath());
            getPictureResourceHelper().loadImageAsync(file.getAbsolutePath(), OnErrorStrategy.DO_NOTHING);
        }
    }

    /**
     * Vérifie que chaque champs de la {@code Person} est valide, et renvoie {@code true} le cas échéant
     * @return {@code true} si tous les champs sont valides
     */
    public boolean checkPersonInput() {

        boolean isWellFormed = true;
        if (!firstNameField.getText().matches(CommonRegex.ACCENTS_AND_LETTERS_REGEX)
                || !lastNameField.getText().matches(CommonRegex.ACCENTS_AND_LETTERS_REGEX)) {
            SupportApp.get().getMsgFactory().warner("Il faut remplir les champs (nom, prénom) avec \nles lettres autorisées : accents, lettres et '-'").show();
            isWellFormed = false;
        } else if (!phoneField.getText().matches(CommonRegex.PHONE_REGEX)) {
            SupportApp.get().getMsgFactory().warner("Le numéro de téléphone n'est pas valide").show();
            isWellFormed = false;
        } else if (photoPathField.getText()!=null&&
                !photoPathField.getText().isEmpty()&&
                !PictureResourceHelper.isRemoteUri(photoPathField.getText())&&
                !checkPictureExists(new File(photoPathField.getText()))){
            isWellFormed=false;
            this.getPictureResourceHelper().couldNotFindImageMsg().show();
        }
        return isWellFormed;
    }

    private void setPrefSizeFields(){
        imageView.setFitWidth(FXSizer.get30PrcPrezWidth() * 2);
        imageView.setFitHeight(FXSizer.get20PrcUappHeight());
        lastNameField.setMaxWidth(FXSizer.get40PrcPrezWidth());
        firstNameField.setMaxWidth(FXSizer.get40PrcPrezWidth());
        phoneField.setMaxWidth(FXSizer.getPrezFieldWidth());
    }

    private boolean checkPictureExists(File file){
        return (file != null) && (file.exists());
    }

}
