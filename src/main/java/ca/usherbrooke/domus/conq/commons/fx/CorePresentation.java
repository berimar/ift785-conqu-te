package ca.usherbrooke.domus.conq.commons.fx;

import ca.usherbrooke.domus.conq.commons.fx.internal.Presentation;
import ca.usherbrooke.domus.conq.commons.fx.internal.PresentationBody;
import javafx.scene.Node;

/**
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class CorePresentation extends Presentation {

    @Override
    protected void insertPresentationBody(PresentationBody body) {
        body.minWidthProperty().bind(body.prefWidthProperty());
        wrapper.setTop(body);
    }

    @Override
    protected Double getWidtheOffset() {
        return 0.0d;
    }

    public void setBase(Node base) {
        wrapper.setCenter(base);
    }
}
