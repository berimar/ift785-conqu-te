package ca.usherbrooke.domus.conq.commons.fx.internal;

import ca.usherbrooke.domus.conq.commons.fx.ExtendedImageView;
import ca.usherbrooke.domus.conq.commons.fx.util.ImageLoader;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import ca.usherbrooke.domus.conq.supp.msg.FeedbackMsgHelper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.image.Image;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


/**
 * date 24/04/15 <br/>
 * Utilitaire pour gérer différentes opérations sur les images (sélection, chargement local, chargment distant asynchrone)
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@RequiredArgsConstructor
public class PictureResourceHelper {
    private final AsynchronousLoadingService asyncLoadingService = new AsynchronousLoadingService();


    public enum OnErrorStrategy {
        DO_NOTHING,
        WARN_USER
    }

    public static final String FILE_IMG_DEFAULT = "default.png";
    public static final String IMAGE_NOT_FOUND = "image-not-found.jpg";

    @Getter(value = AccessLevel.PRIVATE,lazy = true)
    private final Image defaultImage = ImageLoader.staticLoad(FILE_IMG_DEFAULT);

    @Getter(value = AccessLevel.PRIVATE,lazy = true)
    private final Image imageNotFound = ImageLoader.staticLoad(IMAGE_NOT_FOUND);


    @Getter(AccessLevel.MODULE)
    private final ExtendedImageView imageView;
    private boolean hasLoadError = false;


    /**
     * Charge l'image depuis l'URI de façon asynchrone
     * @param pictureURI la chaine de caractère associée au chemin de l'image suivant les spécifications URI.
     * Remarque : le chemin peut être distant (http,ftp...etc).
     * Si le chemin est local, nul besoin de spécifier le préfixe URI "file:"
     * @param errorStrat la stratégie à adopter lorsque la ressource n'est pas disponible
     */
    public synchronized void loadImageAsync(String pictureURI, OnErrorStrategy errorStrat) {
        loadImage(pictureURI,errorStrat);
    }

    private void loadImage(String pictureURI, OnErrorStrategy onErrorStrategy){
        if ((pictureURI != null) && (!pictureURI.isEmpty())) {
            if (isLocalUri(pictureURI)) {
                loadImageAccordingStrategy(pictureURI,onErrorStrategy);
            } else if (isRemoteUri(pictureURI)) { //image distante
                if(!PictureCacheManager.get().setImageIfCached(this,pictureURI)){
                    loadImageAccordingStrategy(pictureURI,onErrorStrategy);
                }
            } else {
                loadImageAccordingStrategy("file:" + pictureURI, onErrorStrategy);
            }
        } else setDefaultImage();
    }

    /**
     * Insère l'image par défaut dans l'{@code ImageView} associée
     */
    public void setDefaultImage() {
        imageView.setImage(getDefaultImage());
    }

    /**
     * @return une instance de message associée à une erreur d'importation de l'image
     */
    public FeedbackMsgHelper couldNotFindImageMsg(){
        return SupportApp.get().getMsgFactory().warner("L'image est introuvable.");
    }

    private void loadImageAccordingStrategy(final String uriPath,
                                            final OnErrorStrategy onErrorStrategy){
        synchronized (asyncLoadingService) {
            asyncLoadingService.cancel();
            asyncLoadingService.setUriPath(uriPath);
            asyncLoadingService.setOnFailed((event) -> onFailure(onErrorStrategy));
            asyncLoadingService.setOnSucceeded(event -> onSucceed(onErrorStrategy));
            asyncLoadingService.restart();
        }

    }

    /**
     *
     * @param uriPath l'uri à tester
     * @return {@code true} si l'uri correspond à une ressource distante
     */
    public static boolean isRemoteUri(String uriPath){
        return uriPath.matches("^\\w+://.*");
    }
    /**
     *
     * @param uriPath l'uri à tester
     * @return {@code true} si l'uri correspond à une ressource locale
     */
    public static boolean isLocalUri(String uriPath){
        return uriPath.matches("^file:.*");
    }

    private void onSucceed(OnErrorStrategy onErrorStrategy){
        if(!asyncLoadingService.getValue()){
            onFailure(onErrorStrategy);
        } else {
            PictureCacheManager.get().putIfAbsent(asyncLoadingService.getUriPath(), imageView.getImage());
        }
    }

    private void onFailure(OnErrorStrategy onErrorStrategy){
        imageView.setImage(getImageNotFound());
        if (onErrorStrategy == OnErrorStrategy.WARN_USER) {
            couldNotFindImageMsg().show();
        }
    }

    private class AsynchronousLoadingService extends Service<Boolean> {
        private StringProperty uriPath = new SimpleStringProperty();
        private void setUriPath(String uri){uriPath.setValue(uri);}
        private String getUriPath(){return uriPath.get();}
        @Override
        protected Task<Boolean> createTask() {
            return new Task<Boolean>() {
                @Override
                protected Boolean call() throws Exception {
                    imageView.setIsLoadingProperty(true);
                    if(uriPath.isNotEmpty().get()) {
                        Image recoveredImage = new Image(uriPath.get());
                        imageView.setImage(recoveredImage);
                        return !recoveredImage.isError();
                    } else {
                        return Boolean.FALSE;
                    }
                }
            };
        }
    }
}

