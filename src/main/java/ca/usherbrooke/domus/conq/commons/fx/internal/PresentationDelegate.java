package ca.usherbrooke.domus.conq.commons.fx.internal;

import ca.usherbrooke.domus.conq.commons.fx.util.ButtonTemplate;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Button;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * date 28/04/15 <br/>
 * Classe chargée d'externaliser la logique interne à l'API Presentation, sert d'interface entre
 * {@link PresentationBody} et {@link Presentation}
 * @implNote Patrons Delegate et Façade
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
 class PresentationDelegate {

    private final PresentationBody presentationBody;


    @Getter(AccessLevel.MODULE)
    private final BooleanProperty isFirstLaunchProperty = new SimpleBooleanProperty(true);

    @Getter(AccessLevel.MODULE)
    private final HashMap<String,Button> mappedButtonById = new HashMap<>();

    @Getter(AccessLevel.MODULE)
    private final List<Button> buttons = new ArrayList<>();

    /**
     * Un éventuel PresentationBody à afficher par {@code Presentation} lorsque {@code isShowingProperty} passe de 'true' à 'false'
     */
    @Getter(AccessLevel.MODULE)
    private Optional<PresentationBody> nextPresentationBody = Optional.empty();
    @Getter(AccessLevel.MODULE)
    protected final BooleanProperty isShowingProperty = new SimpleBooleanProperty(false);

    @Getter
    private final BooleanProperty isShowingReadOnlyProperty = BooleanProperty.booleanProperty(isShowingProperty);

    private void weakHide() {
        isShowingProperty.set(false);
    }

    void show() {
        isShowingProperty.set(true);
    }

    void hide() {
        nextPresentationBody = Optional.empty();
        weakHide();
    }

    /**
     * Cache ce corps de présentation pour afficher le suivant.
     *
     * @param nextPresentationBody le corps de présentation à afficher
     */

    void showNext(@NonNull PresentationBody nextPresentationBody) {
        this.nextPresentationBody = Optional.of(nextPresentationBody);
        weakHide();
    }

    private void generateButtons(){
        buttons.addAll(presentationBody
                .getButtonsTemplates().stream()
                .map(ButtonTemplate::toButton)
                .collect(Collectors.toList()));
        buttons.stream().filter((button) -> button.getId() != null)
                .forEach((button) -> mappedButtonById.put(button.getId(), button));
        presentationBody.onMappedButtonUpdate();
    }

    PresentationDelegate(PresentationBody presentationBody){
        this.presentationBody = presentationBody;
        isFirstLaunchProperty.addListener((obs,old,neu)->{
            if(old&&!neu) {
                generateButtons();
            }
        });
        isShowingProperty.addListener((obs,old,neu)->{
            if(!old&&neu){
                isFirstLaunchProperty.set(false);
            }
        });
    }
}
