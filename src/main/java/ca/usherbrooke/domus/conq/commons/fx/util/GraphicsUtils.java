package ca.usherbrooke.domus.conq.commons.fx.util;

import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.WritableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Duration;
import org.controlsfx.control.PopOver;

/**
 * @author Claire
 */
public class GraphicsUtils {

    /**
     * Ouvre ou ferme le panneau de donné en argument avec une animation (slide in/out)
     *
     * @param property    la propriété à changer
     * @param size        la taille finale voulue
     * @param duration_ms la durée de l'animation
     */
    public static void showHidePane(WritableValue property, int size, double duration_ms, EventHandler<ActionEvent> onFinishCallback) {
        // Animation du panneau : sliding in
        Timeline timeline = buildTimeLine(property, size, duration_ms);
        timeline.setOnFinished(onFinishCallback);
        Platform.runLater(timeline::play);
    }

    /**
     * Ouvre ou ferme le panneau de donné en argument avec une animation (slide in/out)
     *
     * @param property    la propriété à changer
     * @param size        la taille finale voulue
     * @param duration_ms la durée de l'animation
     */
    public static void showHidePane(WritableValue property, int size, double duration_ms) {
        // Animation du panneau : sliding in
        Timeline timeline = buildTimeLine(property, size, duration_ms);
        timeline.play();
    }

    public static PopOver createOkCancelDialog(String title, String text, Runnable onOkRunnable) {
        final PopOver popOver = new PopOver();
        ButtonTemplate onOk = GlyphHelper.CONFIRM_HELPER
                .newButtonTemplteBuildr()
                .onAction(()->{
                    onOkRunnable.run();
                    popOver.hide();

                }).build();
        ButtonTemplate onCancel = GlyphHelper.CANCEL_HELPER.newButtonTemplteBuildr()
                .onAction(popOver::hide)
                .build();

        Label titleLabel = new Label(title);
        Text textBox = new Text(text);
        textBox.getStyleClass().addAll("marqee", "label");
        popOver.setDetached(false);
        BorderPane borderPane = new BorderPane();
        borderPane.setTop(titleLabel);
        titleLabel.getStyleClass().add("title");

        HBox hbox = new HBox();
        hbox.getStyleClass().add("defaultButtonBox");
        hbox.setAlignment(Pos.CENTER);
        hbox.getChildren().addAll(onCancel.toButton(),onOk.toButton());
        borderPane.setBottom(hbox);
        borderPane.setCenter(textBox);
        borderPane.setMargin(hbox, new Insets(5));
        BorderPane.setAlignment(textBox, Pos.CENTER);
        BorderPane.setAlignment(hbox, Pos.BOTTOM_CENTER);
        BorderPane.setAlignment(titleLabel, Pos.TOP_CENTER);
        borderPane.getStyleClass().add("dialogPane");

        popOver.setContentNode(borderPane);
        popOver.setArrowSize(0);
        popOver.setDetachable(false);
        popOver.setAutoHide(true);
        return popOver;
    }

    public static void fadeIn(double duration_ms, Node node, EventHandler<ActionEvent> onFinishCallback) {
        fadeOnce(0, 1, duration_ms, node, onFinishCallback);
    }

    public static void fadeOut(double duration_ms, Node node, EventHandler<ActionEvent> onFinishCallback) {
        fadeOnce(1, 0, duration_ms, node, onFinishCallback);
    }

    public static void fadeOnce(double from, double to, double duration_ms, Node node, EventHandler<ActionEvent> onFinishCallback) {
        FadeTransition ft = new FadeTransition(Duration.millis(duration_ms), node);
        ft.setFromValue(from);
        ft.setToValue(to);
        ft.setCycleCount(0);
        ft.setAutoReverse(false);
        if (onFinishCallback != null)
            ft.setOnFinished(onFinishCallback);
        ft.play();
    }

    private static Timeline buildTimeLine(WritableValue property, int size, double duration) {
        Timeline timeline = new Timeline();
        timeline.setCycleCount(1);
        timeline.setAutoReverse(false);
        KeyValue kv = new KeyValue(property, size);
        KeyFrame kf = new KeyFrame(Duration.millis(duration), kv);
        timeline.getKeyFrames().add(kf);
        return timeline;
    }

}
