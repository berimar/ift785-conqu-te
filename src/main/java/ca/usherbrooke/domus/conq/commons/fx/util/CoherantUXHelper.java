package ca.usherbrooke.domus.conq.commons.fx.util;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.Control;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.controlsfx.validation.ValidationSupport;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Utilitaire pour faire des opérations sur l'arborescence graphique
 * Permet notamment d'enregistrer des {@link org.controlsfx.validation.Validator}, d'insérer
 * des {@link org.controlsfx.glyphfont.Glyph}s, etc...
 *
 * @author sv3inburne | jules.randolph@reseau.eseo.fr
 */
public class CoherantUXHelper {

    @Getter
    private final ValidationSupport validationSupport = new ValidationSupport();

    @Getter
    private final Set<Control> registeredTooltipedControls = new HashSet<>();

    @Getter
    private final Stage stage;

    @Setter
    @Getter
    private Node rootNode;

    /**
     * Instancie un utilitaire pour faire des opérations sur l'arborescence graphique
     *
     * @param stage    le {@code Stage} associé
     * @param rootNode le {@code Pane} à partir duquel faire des opérations
     */
    public CoherantUXHelper(@NonNull Stage stage, @NonNull Node rootNode) {
        this.stage = stage;
        this.rootNode = rootNode;
        validationSupport.setValidationDecorator(new ValidatorHelper.CustomValidationDecorator());
    }

    /**
     * Ajoute un tooltip au {@link Control} identifié par le sélecteur.
     * Un warning sera affiché si le noeud est introuvable
     *
     * @param cssIdSelector l'identifiant css (sous la forme #identifiant)
     * @param tooltip       le message d'aide
     * @throws java.lang.ClassCastException si le noeud trouvé n'est pas un {@code Control}
     */
    public void applyTooltipToId(String cssIdSelector, String tooltip) {
        FXUtils.safeDeepIdLookup(rootNode, cssIdSelector)
                .ifPresent(node -> registeredTooltipedControls.add(FXUtils.castAndSetTooltip(node, tooltip)));
    }

    /**
     * Ajoute un tooltip aux {@link Control}s identifiés par le sélecteur
     *
     * @param cssClassSelector l'identifiant css (sous la forme #identifiant)
     * @param tooltip          le message d'aide
     * @throws java.lang.ClassCastException si le noeud trouvé n'est pas un {@code Control}
     */
    public void applyTooltipToClasses(String cssClassSelector, String tooltip) {
        FXUtils.deepPseudoClassLookup(rootNode, cssClassSelector)
                .forEach((node) -> registeredTooltipedControls.add(FXUtils.castAndSetTooltip(node, tooltip)));
    }

    /**
     * Ajout dynamiquement des glyphs aux classes identifiées par {@link GlyphHelper#getCssClassSelector()}
     *
     * @param glyphHelpers les {@link GlyphHelper}s associés
     * @see GlyphHelper
     */
    public void addGlyphToButtonClasses(GlyphHelper... glyphHelpers) {
        for (GlyphHelper glyphHelper : glyphHelpers) {
            FXUtils.deepPseudoClassLookup(rootNode, glyphHelper.getCssClassSelector())
                    .forEach((node) -> ((ButtonBase) node).setGraphic(glyphHelper.newGlyph()));
        }
    }

    /**
     * Force l'affichage des tooltips dont les {@link Control}s associés ont été enregistrés par un appel à {@link #applyTooltipToClasses(String, String)} } ou {@link #applyTooltipToId(String, String)}
     */
    public void toggleRegisteredTooltips() {
        toggleTooltips(registeredTooltipedControls);
    }

    /**
     * Force l'affichage des tooltips associés à chacun des {@link Control}
     *
     * @param controlsToBeTiped les controls pour lesquels on souhaite afficher les tooltips
     */
    public void toggleTooltips(Collection<? extends Control> controlsToBeTiped) {
        controlsToBeTiped.stream()
                .filter(control -> control.getTooltip() != null)
                .forEach((control) -> showTooltip(stage, control, control.getTooltip()));
    }

    /**
     * Force l'affichage des tooltips associés à tous les noeuds instance de {@code Button}
     */
    public void toggleAllButtonTooltips() {
        FXUtils.deepPseudoClassLookup(rootNode, ".button").stream()
                .map(node -> (Control) node)
                .filter(control -> control.getTooltip() != null)
                .forEach(control -> showTooltip(stage, control, control.getTooltip()));
    }

    /**
     * Ajoute une contrainte de validation aux champs de saisie identifié par {@link ValidatorHelper#getCssClassSelector}
     *
     * @param validatorHelper les {@code ValidatorHelper}s à appliquer
     * @see ValidatorHelper
     */
    public void addValidationSupport(ValidatorHelper validatorHelper,Node node) {
        validationSupport.registerValidator((Control) node, validatorHelper.getValidator());
    }


    /**
     * Active la validation dans les champs associés aux classes css définies dans les ValidationHelper
     *
     * @param validatorHelpers une suite de {@link ValidatorHelper}
     */
    public void addValidationSupport(ValidatorHelper... validatorHelpers) {
        for (ValidatorHelper validatorHelper : validatorHelpers) {
            FXUtils.deepPseudoClassLookup(rootNode, validatorHelper.getCssClassSelector())
                    .forEach((node) -> validationSupport.registerValidator((Control) node, validatorHelper.getValidator()));
        }
    }

    private void showTooltip(Stage owner, Control control, Tooltip tooltip) {
        Point2D p = control.localToScene(0.0, 0.0);
        tooltip.setAutoHide(true);

        if (control.isVisible()) {
            tooltip.show(owner, p.getX()
                    + control.getScene().getX() + control.getScene().getWindow().getX(), p.getY()
                    + control.getScene().getY() + control.getScene().getWindow().getY());
        }

    }

}