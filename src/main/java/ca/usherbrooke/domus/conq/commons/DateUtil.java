package ca.usherbrooke.domus.conq.commons;

import lombok.extern.java.Log;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.logging.Level;

/**
 * @author Marco Jakob http://code.makery.ch/library/javafx-8-tutorial/part3/
 */
@Log
public class DateUtil {

    /**
     * The date pattern that is used for conversion. Change as you wish.
     */
    private static final String DATE_PATTERN = "dd/MM/yyyy";

    /**
     * The date formatter.
     */
    public static final DateTimeFormatter DATE_FORMATTER =
            DateTimeFormatter.ofPattern(DATE_PATTERN);
    /**
     * A new StringConverter capable of handling parse errors.
     */
    public static SecureLocalDateStringConverter newLocalDateStringConverter() {
        return new SecureLocalDateStringConverter() {
            @Override
            public String toString(LocalDate localDate) {
                return DATE_FORMATTER.format(localDate);
            }

            @Override
            public LocalDate fromString(String formattedString) {
                parsedString=formattedString;
                try {
                    LocalDate date =  LocalDate.from(DATE_FORMATTER.parse(formattedString));
                    hasParseError=false;
                    return date;
                } catch (DateTimeParseException parseExc){
                    hasParseError=true;
                    return null;
                }
            }
        };
    }

    /**
     * Returns the given date as a well formatted String. The above defined
     * {@link DateUtil#DATE_PATTERN} is used.
     *
     * @param date the date to be returned as a string
     * @return formatted string
     */
    public static String format(LocalDate date) {
        if (date == null) {
            return null;
        }
        return DATE_FORMATTER.format(date);
    }

    public static String format(Calendar calendar) {
        return format(LocalDate.from(calendar.toInstant()));
    }

    /**
     * Converts a String in the format of the defined {@link DateUtil#DATE_PATTERN}
     * to a {@link LocalDate} object.
     * <p>
     * Returns null if the String could not be converted.
     *
     * @param dateString the date as String
     * @return the date object or null if it could not be converted
     */
    public static LocalDate parse(String dateString) {
        try {
            return DATE_FORMATTER.parse(dateString, LocalDate::from);
        } catch (DateTimeParseException e) {
            log.log(Level.WARNING,"Impossible d'extraire une instance de LocalDate à partir du string.",e);
            return null;
        }
    }

    /**
     * Checks the String whether it is a valid date.
     *
     * @param dateString the date to valid
     * @return true if the String is a valid date
     */
    public static boolean validDate(String dateString) {
        // Try to parse the String.
        return DateUtil.parse(dateString) != null;
    }
}
