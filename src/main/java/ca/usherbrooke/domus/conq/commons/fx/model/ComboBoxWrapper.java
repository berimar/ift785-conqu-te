package ca.usherbrooke.domus.conq.commons.fx.model;

import ca.usherbrooke.domus.conq.db.DataProvider;
import ca.usherbrooke.domus.conq.db.WrappedListDAO;
import ca.usherbrooke.domus.conq.db.model.ApartmentEntity;
import ca.usherbrooke.domus.conq.db.model.RelationTypeEntity;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.util.StringConverter;

/**
 * date 27/04/15 <br/>
 * Classe utilitaire permettant d'insérer des convertisseur {@code Entity} -> {@code String} dans des {@code ComboBox<Entity>}
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class ComboBoxWrapper {

    public static void wrapRelationTypeCB(ComboBox<RelationTypeEntity> relationTypeComboBox){
        WrappedListDAO<RelationTypeEntity> relationTypes = DataProvider.get().getRelationTypes();
        relationTypeComboBox.setCellFactory(comboBox -> new ListCell<RelationTypeEntity>() {
            @Override
            protected void updateItem(RelationTypeEntity item, boolean empty) {
                super.updateItem(item, empty);

                if (item == null || empty) {
                    setText(null);
                } else {
                    setText(item.getType());
                }
            }
        });
        relationTypeComboBox.setConverter(new StringConverter<RelationTypeEntity>() {
            @Override
            public String toString(RelationTypeEntity rel) {
                if (rel == null) {
                    return null;
                } else {
                    return rel.getType();
                }
            }
            @Override
            public RelationTypeEntity fromString(String rel) {
                return null;
            }
        });
        relationTypeComboBox.setItems(relationTypes.getUnmodifiableList());
    }
    public static void wrapApartmentCB(ComboBox<ApartmentEntity> apartmentComboBox){
        WrappedListDAO<ApartmentEntity> apartments = DataProvider.get().getApartments();
        apartmentComboBox.setCellFactory(
                comboBox -> new ListCell<ApartmentEntity>() {
                    @Override
                    protected void updateItem(ApartmentEntity apartment, boolean empty) {
                        super.updateItem(apartment, empty);
                        if (apartment == null || empty) {
                            setText(null);
                        } else {
                            setText(String.valueOf(apartment.getApartmentNumber()));
                        }
                    }
                }
        );
        apartmentComboBox.setConverter(new StringConverter<ApartmentEntity>() {
            @Override
            public String toString(ApartmentEntity rel) {
                if (rel == null) {
                    return null;
                } else {
                    return String.valueOf(rel.getApartmentNumber());
                }
            }

            @Override
            public ApartmentEntity fromString(String rel) {
                return null;
            }
        });
        apartmentComboBox.setItems(apartments.getUnmodifiableList());
    }
}
