package ca.usherbrooke.domus.conq.commons.fx.internal;

import javafx.beans.property.SimpleLongProperty;
import javafx.scene.image.Image;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.concurrent.atomic.AtomicLong;

/**
 * date 28/04/15 <br/>
 * Classe "enveloppe" pour gérer les méta-donnée sur une image disponible en cache
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@ToString
@EqualsAndHashCode
public class MetaImageWrapper implements Comparable<MetaImageWrapper> {

    @Getter
    private final Image wrapperImage ;
    @Getter
    private final Long approxPixelSize;
    private final AtomicLong numberOfWatchEvents = new AtomicLong(0);
    @Getter
    private final String uriPath;

    MetaImageWrapper(final Image wrappedImage, SimpleLongProperty currentCacheSize_px, String uriPath){
        this.wrapperImage=wrappedImage;
        this.uriPath = uriPath ;
        approxPixelSize =Math.round(wrappedImage.getWidth()*wrappedImage.getHeight());
        currentCacheSize_px.set(currentCacheSize_px.add(approxPixelSize).get());
    }

    public Image getWrappedImage(){
        numberOfWatchEvents.incrementAndGet();
        return wrapperImage;
    }

    public Long getNumberOfWatchEvents(){
        return numberOfWatchEvents.get();
    }

    @Override
    public int compareTo(MetaImageWrapper o) {
        int statComparision = ((Long) numberOfWatchEvents.get()).compareTo(o.numberOfWatchEvents.get());
        return statComparision==0?approxPixelSize.compareTo(o.approxPixelSize):statComparision;
    }
}
