package ca.usherbrooke.domus.conq.commons.fx.internal;

import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.Clipboard;
import javafx.scene.input.DataFormat;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import org.controlsfx.control.PopOver;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * date 19/04/15 <br/>
 * Encapsule un {@link Node} définissant une méthode {@code public String getText()} et laisse apparaître un petit menu sur Hover pour copier le contenu dans le clipboard
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 * @implNote implémentation du patron Encapsuleur
 */
public class CopiableTextWrapper {
    private static final String ERROR_MSG = "La classe enrobée doit définir une méthode 'public String getText()'";
    private static final Clipboard CLIPBOARD = Clipboard.getSystemClipboard();
    private final Button copyButton = GlyphHelper.COPY_HELPER.newButton();
    private final AnchorPane container = new AnchorPane();
    private final PopOver onHoverPop = new PopOver();
    private final Timer timer = new java.util.Timer();
    private final Node textable;
    private final Method getText;

    /**
     * @param textable un {@code Node} muni d'une méthode {@code public String getText()}
     * @return l'encapsuleur
     */
    public static CopiableTextWrapper wrap(Node textable) {
        return new CopiableTextWrapper(textable);
    }

    private CopiableTextWrapper(Node labeled) {
        this.textable = labeled;
        getText = searchGetTextMethodByReflection();
        container.getStyleClass().add("CopiableLabelContainer");
        container.getChildren().add(copyButton);
        AnchorPane.setRightAnchor(copyButton, 0d);
        AnchorPane.setLeftAnchor(copyButton, 0d);
        AnchorPane.setTopAnchor(copyButton, 0d);
        AnchorPane.setBottomAnchor(copyButton, 0d);
        onHoverPop.setAutoHide(false);
        onHoverPop.setDetachable(false);
        copyButton.setOnAction(this::copyToClipBoard);
        onHoverPop.setContentNode(container);
        copyButton.setOnMouseExited((e) -> onHoverPop.hide(Duration.millis(1000)));

        textable.setOnMouseEntered((e) -> showPopOver());
        textable.setOnTouchPressed((e) -> showPopOver());
        textable.setOnMouseExited(this::schedulePopoverHideIfButtonNotHovered);
    }

    private Method searchGetTextMethodByReflection() {
        try {
            Method getText = textable.getClass().getMethod("getText");
            if (!getText.getReturnType().equals(String.class)) {
                throw new IllegalArgumentException(ERROR_MSG);
            }
            return getText;
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException(ERROR_MSG);
        }
    }


    private void schedulePopoverHideIfButtonNotHovered(MouseEvent mouseEvent) {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!copyButton.isHover() && onHoverPop.isShowing()) {
                    onHoverPop.hide(Duration.millis(400));
                }
            }
        }, 1500L);
    }


    private void copyToClipBoard(ActionEvent event) {
        Platform.runLater(() -> {
            Map<DataFormat, Object> data = new HashMap<>();
            data.put(DataFormat.PLAIN_TEXT, invoqueGetText());
            CLIPBOARD.setContent(data);
            onHoverPop.hide();
        });
    }


    private void showPopOver() {
        if (!isTextEmpty()) onHoverPop.show(textable);
    }

    private String invoqueGetText() {
        try {
            return (String) getText.invoke(textable);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new IllegalStateException(ERROR_MSG);
        }
    }

    private boolean isTextEmpty() {
        String text = invoqueGetText();
        return text == null || text.equals("");
    }
}
