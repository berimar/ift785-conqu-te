package ca.usherbrooke.domus.conq.commons;

/**
 * date 21/03/15 <br/>
 * Représente une entité ayant un "début" et une "fin" de vie.
 *
 * @author sv3inburn3
 */
public interface LifeCycle {
    /**
     * Méthode exécutée au début du cycle de vie de l'entité.
     */
    void start();

    /**
     * Méthode exécutée à la fin du cycle de vie de l'entité.
     */
    void stop();
}
