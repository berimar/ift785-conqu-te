package ca.usherbrooke.domus.conq.commons.fx;

import ca.usherbrooke.domus.conq.commons.fx.internal.CopiableTextWrapper;
import javafx.scene.text.Text;

/**
 * date 19/04/15 <br/>
 * Un {@code Text} qui laisse apparaître un petit menu sur Hover pour copier le contenu dans le clipboard
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class CopiableText extends Text {


    /**
     * Une instance de {@code Text} qui laisse apparaître un petit menu sur Hover pour copier le contenu dans le clipboard
     */
    public CopiableText() {
        CopiableTextWrapper.wrap(this);
    }


}
