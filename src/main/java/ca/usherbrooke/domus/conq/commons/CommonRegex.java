package ca.usherbrooke.domus.conq.commons;


/**
 * Classe qui rassemble les expressions régulières utilisées dans les applications pour valider les inputs de l'utilisateur
 *
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class CommonRegex {
    private CommonRegex(){};
    public static final String ACCENTS_AND_LETTERS_REGEX = "^[\\w\\u00C0-\\u00FC]+((\\-| )[\\w\\u00C0-\\u00FC]+)*$";
    public static final String PHONE_REGEX = "^(\\(?\\d\\d\\d\\)?\\s*-?\\d\\d\\d\\s*-?\\d\\d\\d\\d)?$";
    public static final String EMAIL_REGEX = "^([\\w\\-_\\d]+(\\.[\\w\\-_\\d]+)*@([\\w\\-_\\d]+\\.)+[\\w\\d]+)?$";
    public static final String NUMBERS_REGEX = "^\\d+$";
    public static final String LETTERS_AND_NUMBERS_REGEX = "^\\w+$";
}
