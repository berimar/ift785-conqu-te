package ca.usherbrooke.domus.conq.commons.fx;

import ca.usherbrooke.domus.conq.commons.fx.internal.Presentation;
import ca.usherbrooke.domus.conq.commons.fx.internal.PresentationBody;
import ca.usherbrooke.domus.conq.commons.fx.util.GraphicsUtils;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import lombok.Setter;

/**
 * date 13/04/15 <br/>
 * Une {@link Presentation} qui peut être montrée/cachée avec un effet de slide
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class SlidingPresentation extends Presentation {

    private BooleanProperty isSlidingPrezVisible;

    public static final int DEDAULT_SHOW_TIME_MS = 500;
    public static final int DEFAULT_HIDE_TIME_MS = 500;


    @Setter
    private double showTime_ms = DEDAULT_SHOW_TIME_MS;
    @Setter
    private double hideTime_ms = DEFAULT_HIDE_TIME_MS;


    @Override
    protected void onHiding() {
        GraphicsUtils.showHidePane(prefWidthProperty(), 0, hideTime_ms, (e) -> {
            wrapper.setCenter(null); //déréférencement pour éventuelle garbage collection
            isSlidingPrezVisible.setValue(false);
            presentationBody.get().getChildren().remove(help);
        });
    }

    public void onInitialize() {
        isSlidingPrezVisible = new SimpleBooleanProperty(false);
        buttonsBox.setAlignment(Pos.BOTTOM_CENTER);
        wrapper.minWidthProperty().bind(prefWidthProperty());
        wrapper.maxWidthProperty().bind(prefWidthProperty());
        minWidthProperty().bind(prefWidthProperty());
        maxWidthProperty().bind(prefWidthProperty());
        buttonsBox.visibleProperty().bind(isSlidingPrezVisible);
        wrapper.visibleProperty().bind(isSlidingPrezVisible);
        visibleProperty().bind(isSlidingPrezVisible);
        help.visibleProperty().bind(isSlidingPrezVisible);
        wrapper.getStyleClass().add("slidingPane");
    }

    @Override
    protected void insertPresentationBody(PresentationBody body) {
        isSlidingPrezVisible.setValue(true);
        if (!isShowing()) {
            GraphicsUtils.showHidePane(prefWidthProperty(), (int) (presentationBody.get().getPrefWidth() + getWidtheOffset()), showTime_ms);
        }
        wrapper.setCenter(body);
    }

    @Override
    protected Double getWidtheOffset() {
        return 35.0d;
    }


    /**
     * Force le {@code SlidingPane} à se rétracter
     */
    public void hide() {
        onHiding();
    }
}
