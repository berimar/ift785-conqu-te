package ca.usherbrooke.domus.conq.commons.fx.internal;

import ca.usherbrooke.domus.conq.commons.fx.util.*;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * date 13/04/15 <br/>
 * Une {@code AnchorPane} munie d'une liste de templates de boutons, conçu pour être utilisé en association avec une instance de {@code Presentation}
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 * @implSpec la méthode {@link #bindToFXML} doit être appellée par le constructeur de la <strong>dernière classe dans la hierarchie d'héritage munie d'annotations {@code @Fxml}</strong> <br>
 * Exemple de fichier fxml :
 * <pre>
 * {@code<?xml version="1.0" encoding="UTF-8"?>
 * <?import javafx.scene.layout.Region?>
 * <?import the.java.package.path.to.InheritingClass?>
 * <fx:root type="InheritingClass"
 *      prefWidth="500" styleClass="anchor"
 *      xmlns="http://javafx.com/javafx/8"
 *      xmlns:fx="http://javafx.com/fxml/1" >
 *      <Region prefWidth="100" maxWidth="100"/>
 * </fx:root>
 * }
 * </pre> ou <strong>InheritingClass</strong> est la  <strong>dernière classe dans la hierarchie d'héritage munie d'annotations {@code @Fxml}</strong> <br><br>
 * Le constructeur de {@code InheritingClass} aura donc l'allure suivante
 * <pre>
 * {@code
 *     public InheritingClass(){
 *          bindToFXML();
 *          //autres traitements
 *     }
 * }
 * </pre>
 * @see ca.usherbrooke.domus.conq.commons.fx.util.ButtonTemplate
 * @see Presentation
 */
public abstract class PresentationBody extends AnchorPane implements FxmlBindable {

    /**
     * La liste de templates de boutons qui seront insérés horizontalement, dans l'ordre d'énumération, en bas du menu
     */
    @Getter(AccessLevel.MODULE)
    protected final List<ButtonTemplate> buttonsTemplates = new ArrayList<>();

    @Getter(AccessLevel.MODULE)
    private PresentationDelegate prezDelegate = new PresentationDelegate(this);

    @Getter(AccessLevel.MODULE)
    protected final CoherantUXHelper coherantUXHelper = new CoherantUXHelper(SupportApp.get().getPrimaryStage(), this);

    @Getter
    private final ReadOnlyBooleanProperty isShowingReadOnlyProperty = prezDelegate.getIsShowingReadOnlyProperty();

    protected final BooleanProperty isShowingProperty = prezDelegate.getIsShowingProperty();

    /**
     * Affiche le corps de présentation
     */
    protected void show() {
        prezDelegate.show();
    }

    /**
     * Cache le corps présentation
     */
    protected void hide() {
        prezDelegate.hide();
    }

    /**
     * Cache ce corps de présentation pour afficher le suivant.
     *
     * @param nextPresentationBody le corps de présentation à afficher
     */
    protected void showNext(PresentationBody nextPresentationBody) {
        prezDelegate.showNext(nextPresentationBody);
    }


    /**
     * Appellée lors de la première installation dans une {@code Presentation}
     */
    public void onMappedButtonUpdate(){}

    /**
     * @return une instance de bouton dont le ButtonTemplate associé lors de l'initialisation a été pourvu
     * d'un id css, si elle existe, {@code null} sinon.
     * @param cssId l'identifiant sans le préfixe '#'
     */
    protected Button getButtonById(String cssId){
        return prezDelegate.getMappedButtonById().get(cssId);
    }

    /**
     * Créée une nouvelle instance
     */
    public PresentationBody() {
        prefWidthProperty().bind(FXSizer.PREZ_WIDTH);
        maxWidthProperty().bind(FXSizer.PREZ_WIDTH);
        prefHeightProperty().bind(FXSizer.PREF_PREZ_BODY_HEIGHT);
        maxHeightProperty().bind(FXSizer.PREF_PREZ_BODY_HEIGHT);
        getStyleClass().addAll(this.getClass().getSimpleName(), "PresentationBody");
    }

    /**
     * @return une liste de {@code GlyphHelper}s dont les {@code Glyph} seront insérés dans les classes associées
     * @implNote Overrider cette méthode pour enregistrer ses propres {@code GlyphHelper}s
     * @see ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper#getCssClassSelector()
     */
    public GlyphHelper[] getGlyphHelpers() {
        return new GlyphHelper[]{};
    }

    /**
     * @return une liste de {@code ValidatorHelper}s dont les {@code Validator} seront insérés dans les classes associées
     * @implNote Overrider cette méthode pour enregistrer ses propres {@code ValidatorHelper}s
     * @see ca.usherbrooke.domus.conq.commons.fx.util.ValidatorHelper#getCssClassSelector()
     */
    public ValidatorHelper[] getValidatorHelpers() {
        return new ValidatorHelper[]{};
    }


}
