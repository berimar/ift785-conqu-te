package ca.usherbrooke.domus.conq.commons.fx;

import ca.usherbrooke.domus.conq.commons.fx.internal.CopiableTextWrapper;
import javafx.scene.control.Label;


/**
 * date 19/04/15 <br/>
 * Un label qui laisse apparaître un petit menu sur Hover pour copier le contenu dans le clipboard
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class CopiableLabel extends Label {

    /**
     * Instancie un label qui laisse apparaître un petit menu sur Hover pour copier le contenu dans le clipboard
     */
    public CopiableLabel() {
        CopiableTextWrapper.wrap(this);
    }
}
