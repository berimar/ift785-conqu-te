package ca.usherbrooke.domus.conq.commons.fx;

import ca.usherbrooke.domus.conq.commons.TimeUtils;
import ca.usherbrooke.domus.conq.commons.fx.util.FAGlyphs;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import jfxtras.scene.control.LocalDateTimePicker;
import org.controlsfx.control.PopOver;
import org.controlsfx.glyphfont.FontAwesome;

import java.time.LocalDateTime;

/**
 * date 17/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class DateTimePickerPopable extends Button {
    private final LocalDateTimePicker localDateTimePicker = new LocalDateTimePicker();
    private final PopOver popOver = new PopOver();
    private LocalDateTime savedLocalDateTime;

    public DateTimePickerPopable() {
        HBox buttonBox = new HBox();
        BorderPane borderPane = new BorderPane();
        buttonBox.getStyleClass().add("defaultButtonBox");
        Button cancel = GlyphHelper.CANCEL_HELPER.newButtonTemplteBuildr().onAction(popOver::hide).build().toButton();
        Button valid = GlyphHelper.CHOOSE_HELPER.newButtonTemplteBuildr().onAction(this::updateDate).build().toButton();
        buttonBox.getChildren().addAll(cancel,valid);
        borderPane.getStyleClass().add("stylishPopup");
        setGraphic(FAGlyphs.getIcon(FontAwesome.Glyph.CLOCK_ALT, Color.GHOSTWHITE));
        borderPane.setCenter(localDateTimePicker);
        borderPane.setBottom(buttonBox);
        buttonBox.setAlignment(Pos.CENTER);
        BorderPane.setAlignment(buttonBox, Pos.BOTTOM_CENTER);
        BorderPane.setMargin(buttonBox, new Insets(15));
        setOnAction(this::togglePop);
        popOver.setDetachable(false);
        popOver.setAutoHide(true);
        popOver.setContentNode(borderPane);
        localDateTimePicker.setAllowNull(false);
    }

    private void togglePop(ActionEvent actionEvent) {
        if (popOver.isShowing()) {
            popOver.hide();
        } else {
            popOver.show(this);
        }
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        localDateTimePicker.setLocalDateTime(localDateTime);
        setText(TimeUtils.SIMPLE_DATE_TIME.format(localDateTime).replace(' ','\n'));
        setTextAlignment(TextAlignment.CENTER);
        savedLocalDateTime = localDateTime;
    }

    public LocalDateTime getLocalDateTime() {
        return savedLocalDateTime;
    }

    private void updateDate() {
        if (localDateTimePicker.getLocalDateTime() != null) {
            savedLocalDateTime = localDateTimePicker.getLocalDateTime();
            setText(TimeUtils.SIMPLE_DATE_TIME.format(savedLocalDateTime));
            popOver.hide();
        }
    }

}
