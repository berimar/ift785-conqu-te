/**
 * date 12/03/15 <br/>
 * Un package commun à tout ce qui à trait d'une part au MVC JavaFX, d'autre part à la notion de cycle de vie d'une application.
 * Tout ce qui a trait à la sérialisation devrait se trouver ici.
 * @author sv3inburn3
 */
package ca.usherbrooke.domus.conq.commons.app;