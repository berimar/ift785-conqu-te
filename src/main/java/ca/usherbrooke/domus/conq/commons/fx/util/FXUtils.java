package ca.usherbrooke.domus.conq.commons.fx.util;

import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Labeled;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.Tooltip;
import javafx.scene.text.TextAlignment;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * date 08/03/15 <br/>
 * <p>
 * Une classe pour toutes les méthodes statiques utiles pour l'utilisation de javaFX
 *
 * @author sv3inburn3
 */
@Log
public final class FXUtils {
    /**
     * Charge le noeud javafx de type {@code <T>} à partir du {@code builder} passé en paramètre.
     *
     * @param context la classe java à partir de laquelle charger la ressource
     * @param path    le chemin relatif à partir de la classe
     * @param builder la fonction permetant d'instancier le noeud.
     * @param <T>     le type de retour spécifié par la première balise du fichier fxml, par exemple AnchorPane
     *                ou BorderPane
     * @return l'élément de type T chargé par le loader.
     * @throws java.lang.IllegalStateException si le fichier fxml est mal configuré ou introuvable
     */
    @SuppressWarnings("unchecked")
    public static <T extends Node> T loadFXML(Class<?> context, String path, Function<URL, ? extends Node> builder) {
        try {
            URL url = context.getResource(path);
            if (url != null) {
                return (T) builder.apply(url);
            } else {
                //noinspection RedundantStringToString
                throw new IllegalStateException("Le fichier fxml est manquant : "
                        + path.toString() + " à partir du contexte " + context.getPackage().toString()
                        + ".\n Assurez vous que la ressource apparait dans le dossier de build dans le fichier relatif à "
                        + "la classe compilée .class.");
            }
        } catch (RuntimeException io) {
            IllegalStateException ise = new IllegalStateException("Le fichier fxml est probablement erronné: "
                    + path + " à partir du contexte " + context.getPackage()
                    + ".\n Assurez vous que la ressource apparait dans le dossier de build dans le fichier relatif à "
                    + "la classe compilée .class. Stacktrace:\n");
            ise.addSuppressed(io);
            throw ise;
        }

    }

    /**
     * Charge le noeud javafx de type {@code <T>}
     *
     * @param context la classe java à partir de laquelle charger la ressource
     * @param path    le chemin relatif à partir de la classe
     * @param <T>     le type de retour spécifié par la première balise du fichier fxml, par exemple AnchorPane
     *                ou BorderPane
     * @return
     */
    public static <T extends Node> T loadFXML(Class<?> context, String path) {
        return loadFXML(context, path, (url) -> {
            try {
                return FXMLLoader.load(url);
            } catch (IOException io) {
                RuntimeException runtimeException = new RuntimeException(io.getMessage());
                runtimeException.addSuppressed(io);
                throw runtimeException;
            }
        });
    }

    /**
     * @param context           la classe java à partir de laquelle charger la ressource
     * @param path              le chemin relatif à partir de la classe
     * @param fxmlLoaderHandler une fonction qui va consommer l'instance de FXMLloader
     */
    public static void loadFXML(Class<?> context, String path, Consumer<FXMLLoader> fxmlLoaderHandler) {
        loadFXML(context, path, (url) -> {
            FXMLLoader fxmlLoader = new FXMLLoader(url);
            fxmlLoaderHandler.accept(fxmlLoader);
            try {
                fxmlLoader.load();
            } catch (IOException io) {
                RuntimeException runtimeException = new RuntimeException(io.getMessage());
                runtimeException.addSuppressed(io);
                throw runtimeException;
            }
            return null;
        });
    }

    /**
     * Associe l'instance passée en paramètre au fichier fxml
     *
     * @param instance       l'instsance à associer
     * @param fxmlPathToBing le fichier de description
     */
    public static void bindToFXML(@NonNull Object instance, @NonNull String fxmlPathToBing) {
        FXUtils.loadFXML(instance.getClass(), fxmlPathToBing, (loader) -> {
            loader.setController(instance);
            loader.setRoot(instance);
        });
    }

    /**
     * lookup limité à un ensemble de pseudo class css, mais qui traverse les collections d'items.\n
     * Attention! : la sélection ne fonctionne que pour des pseudoClass concommitantes, ex :
     * {@code .pseudo-class-a.pseudo-class-b} et <strong>pas</strong> pour une sélection par arborescence
     * comme {@code .pseudo-class-a .pseudo-class-b}
     * @param root        la racine à partir de laquelle chercher
     * @param cssSelector le sélecteur css
     * @return une collection de noeuds trouvés
     */
    public static List<Node> deepPseudoClassLookup(Node root, String cssSelector) {
        if(cssSelector.contains(" ")) throw new IllegalArgumentException("Le sélecteur ne doit pas contenir d'espaces.");
        List<Node> foundNodes = new ArrayList<>();
        LookupHelper fromGetItems = getIfMethodAvailable(root, "getItems");
        LookupHelper fromGetChildren = getIfMethodAvailable(root, "getChildren");
        if (pseudoClassEquals(root, cssSelector)) {
            foundNodes.add(root);
        } else {
            if (fromGetItems.isApplicable) {
                foundNodes.addAll(fromGetItems.lookupAll(cssSelector));
            } else if (fromGetChildren.isApplicable) {
                foundNodes.addAll(fromGetChildren.lookupAll(cssSelector));
            } else {
                LookupHelper fromUnmodifiableChildren = getIfMethodAvailable(root, "getChildrenUnmodifiable");
                if (fromUnmodifiableChildren.isApplicable) {
                    foundNodes.addAll(fromUnmodifiableChildren.lookupAll(cssSelector));
                }
            }
        }
        return foundNodes;
    }

    /**
     * lookup limité à un sélecteur d'identifiant, mais qui traverse les collections d'items.
     * Lève une exception si aucun noeud n'est trouvé.
     *
     * @param root          la racine à partir de laquelle chercher
     * @param cssIdSelector le sélecteur css
     * @return le noeud enrobé dans un {@code Optional}
     * @throws java.lang.IllegalStateException si aucun noeud n'a été trouvé
     * @see java.util.Optional
     */
    public static Optional<Node> strictDeepIdLookup(@NonNull Node root, @NonNull String cssIdSelector) {
        Optional<Node> optionalNode = deepIdLookup(root, cssIdSelector);
        optionalNode.orElseThrow(() -> new IllegalStateException("Aucun noeud n'a été trouvé à partir du sélecteur : " + cssIdSelector));
        return optionalNode;
    }

    /**
     * lookup limité à un sélecteur d'identifiant, mais qui traverse les collections d'items.
     * Log une information si aucun noeud n'a été trouvé.
     *
     * @param root          la racine à partir de laquelle chercher
     * @param cssIdSelector le sélecteur css
     * @return le noeud enrobé dans un {@code Optional}
     * @throws java.lang.IllegalStateException si aucun noeud n'a été trouvé
     * @see java.util.Optional
     */
    public static Optional<Node> safeDeepIdLookup(@NonNull Node root, @NonNull String cssIdSelector) {
        Optional<Node> optionalNode = deepIdLookup(root, cssIdSelector);
        if (!optionalNode.isPresent()) log.warning("Noeud attendu : " + cssIdSelector);
        return optionalNode;
    }

    /**
     * lookup limité à un sélecteur d'identifiant, mais qui traverse les collections d'items.
     * Ne lève aucune exception si aucun noeud n'est trouvé.
     *
     * @param root          la racine à partir de laquelle chercher
     * @param cssIdSelector le sélecteur css
     * @return le noeud enrobé dans un {@code Optional}
     * @see java.util.Optional
     */
    public static Optional<Node> deepIdLookup(@NonNull Node root, @NonNull String cssIdSelector) {
        Optional<Node> foundNode = Optional.empty();
        LookupHelper fromGetItems = getIfMethodAvailable(root, "getItems");
        LookupHelper fromGetChildren = getIfMethodAvailable(root, "getChildren");
        if (idEquals(root, cssIdSelector)) {
            foundNode = Optional.of(root);
        } else {
            if (fromGetItems.isApplicable) {
                foundNode = fromGetItems.lookup(cssIdSelector);
            } else if (fromGetChildren.isApplicable) {
                foundNode = fromGetChildren.lookup(cssIdSelector);
            } else {
                LookupHelper fromUnmodifiableChildren = getIfMethodAvailable(root, "getChildrenUnmodifiable");
                if (fromUnmodifiableChildren.isApplicable) {
                    foundNode = fromUnmodifiableChildren.lookup(cssIdSelector);
                }
                //sinon, non applicable
            }
        }
        return foundNode;
    }

    /**
     * Force le noeud en {@code Labeled} et applique le texte
     *
     * @param node le noeud à forcer
     * @param text le texte à insérer
     * @return le noeud forcé
     * @throws java.lang.ClassCastException si le {@code node} n'est pas un {@code Labeled}
     * @see Labeled
     * @see Node
     */
    public static Labeled castAndSetLabeled(Node node, String text) {
        Labeled labeled = (Labeled) node;
        labeled.setText(text);
        return labeled;
    }


    /**
     * Force le noeud en {@code Control} et applique un tooltip
     *
     * @param node le noeud à forcer
     * @param tip  le message d'aide à afficher
     * @return le noeud forcé
     * @throws java.lang.ClassCastException si le {@code node} n'est pas un {@code Control}
     * @see Control
     * @see Node
     */
    public static Control castAndSetTooltip(Node node, String tip) {
        Control control = (Control) node;
        Tooltip tooltip = new Tooltip(tip);
        tooltip.setTextOverrun(OverrunStyle.ELLIPSIS);
        tooltip.setTextAlignment(TextAlignment.JUSTIFY);
        tooltip.wrapTextProperty().set(true);
        control.setTooltip(tooltip);
        return control;
    }

    private static LookupHelper getIfMethodAvailable(Node node, String methodName) {
        List<Node> nodes = new ArrayList<>();
        boolean isMethodApplicable = true;
        Class<? extends Node> nodeClass = node.getClass();
        try {
            Method getter = nodeClass.getMethod(methodName);
            Class<?> returnType = getter.getReturnType();
            if (returnType.isAssignableFrom(ObservableList.class)) {
                ObservableList<?> list = (ObservableList<?>) getter.invoke(node);
                list.forEach((element) -> nodes.add((Node) element));
            }
        } catch (InvocationTargetException | ClassCastException | NoSuchMethodException | IllegalAccessException e) {
            isMethodApplicable = false;
        }
        return new LookupHelper(nodes, isMethodApplicable);
    }

    @RequiredArgsConstructor
    private static class LookupHelper {
        private final List<Node> nodes;
        private final boolean isApplicable;

        Optional<Node> lookup(String cssSelector) {
            Optional<Node> foundNode = Optional.empty();
            if (isApplicable) {
                Optional<Optional<Node>> found = nodes.stream().map(
                        (node) -> deepIdLookup(node, cssSelector)).filter(Optional::isPresent).findFirst();
                if (found.isPresent())
                    foundNode = found.get();
            }
            return foundNode;
        }

        List<Node> lookupAll(String cssSelector) {
            List<Node> foundNode = new ArrayList<>();
            if (isApplicable) {
                Optional<List<Node>> optionalNodes = nodes.stream().map((node) -> deepPseudoClassLookup(node, cssSelector))
                        .reduce(((result, element) -> {
                            result.addAll(element);
                            return result;
                        }));
                if (optionalNodes.isPresent()) {
                    foundNode = optionalNodes.get();
                }
            }
            return foundNode;
        }
    }

    private static boolean idEquals(@NonNull Node node, String cssIdSelector) {
        return node.getId() != null && node.getId().equals(cssIdSelector.substring(1));
    }

    private static boolean pseudoClassEquals(@NonNull Node node, String cssClassSelector) {
        String[] selectors = cssClassSelector.split("\\.");
        List<String> selectorList = Arrays.asList(Arrays.copyOfRange(selectors,1,selectors.length));
//        System.out.println(selectorList);
        return selectorList.stream().allMatch((selector)->node.getStyleClass().contains(selector));
    }
}
