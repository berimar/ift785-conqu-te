package ca.usherbrooke.domus.conq.commons.fx.internal;

import ca.usherbrooke.domus.conq.commons.fx.util.FXUtils;
import lombok.NonNull;

/**
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public interface FxmlBindable {
    /**
     * Renvoie le chemin vers le fichier fxml utilisé pour décrire le corps du menu contextuel
     *
     * @return le chemin <strong>relatif à la classe qui implémente de {@code FxmlBindable}</strong>
     */
    @NonNull
    String getFXMLPath();


    /**
     * @implSpec Méthode à appeller par le constructeur <strong>dernière classe dans la hierarchie d'héritage munie d'annotations {@code @Fxml}</strong>
     */
    default void bindToFXML() {
        FXUtils.bindToFXML(this, getFXMLPath());
    }
}
