package ca.usherbrooke.domus.conq.commons.app;

import ca.usherbrooke.domus.conq.supp.SupportApp;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.java.Log;

import java.lang.reflect.ParameterizedType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * date 11/03/15 <br/>
 * Classe permettant un accès direct à l'application {@code UApp} attachée au controlleur via {@link ca.usherbrooke.domus.conq.commons.app.AppController#getBoundApp}.
 *
 * @author sv3inburn3
 * @implSpec Seuls les héritiers directs de la classe peuvent (pour le moment) accéder à getUApp()
 */
@Log
public abstract class AppController<T extends GraphicalLCApp<?>> {

    private static final Pattern CLASS_PATTERN = Pattern.compile(".*\\.([^\\.]*)$", Pattern.MULTILINE);
    /**
     * L'{@code UApp} associée au controlleur.
     */
    @SuppressWarnings("unchecked")
    @Getter(lazy = true, value = AccessLevel.PROTECTED)
    private final T boundApp = (T) SupportApp.get().getUApps().get(getParametrizedUAppName());

    private String getParametrizedUAppName() {
        String fullName = ((ParameterizedType) (this.getClass().getGenericSuperclass())).getActualTypeArguments()[0].getTypeName();
        Matcher matcher = CLASS_PATTERN.matcher(fullName);
        //noinspection ResultOfMethodCallIgnored
        matcher.find();
        return matcher.group(1);
    }

    /**
     * Renvoie la uApp avec le nom spécifié
     * @param parametrizedUAppName le nom de la uApp
     * @return la uApp (ex: Gestion)
     */
    protected final T getBoundApp(String parametrizedUAppName) {
        return (T) SupportApp.get().getUApps().get(parametrizedUAppName);
    }

}
