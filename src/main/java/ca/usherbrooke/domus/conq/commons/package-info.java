/**
 * date 11/03/15 <br/>
 * Package destiné à tous les outils communs à l'ensemble de l'application.
 *
 * @see ca.usherbrooke.domus.conq
 * @author sv3inburn3
 */
package ca.usherbrooke.domus.conq.commons;