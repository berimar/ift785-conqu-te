package ca.usherbrooke.domus.conq.commons;

import lombok.extern.java.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

/**
 * date 12/03/15 <br/>
 * Une classe pour les utilitaires liés à {@link java.util.Calendar} <br/>
 * L'utilisation de {@link java.util.Date} est dépréciée.
 *
 * @author sv3inburn3
 */
@Log
public class TimeUtils {

    private TimeUtils(){}

    public static final SimpleDateFormat COMPLETE_DATE_TIME = new SimpleDateFormat("yyyy.MM.dd.HH:mm:ss X");
    public static final DateTimeFormatter SIMPLE_DATE_TIME = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");

    /**
     * Renvoie une instance de ZonedDateTime extraite à partir de la chaine de caractère.
     * Cette chaine doit respecter le format suivant : {@code yyyy.MM.dd.HH:mm:ss X}
     * ou bien une ParseException sera levée.
     *
     * @param formattedDate la chaine de caractère respectant le format spécifié ci-dessus.
     * @return une instance de {@code ZonedDateTime} à la date passée en paramètre
     * @throws ParseException si la chaine ne respecte pas le format {@code yyyy.MM.dd.HH:mm:ss X}
     * @see SimpleDateFormat
     */
    public static ZonedDateTime extractLocalDateTime(String formattedDate) throws ParseException {
        synchronized (COMPLETE_DATE_TIME) {
            String[] buffer = formattedDate.split("\\s"); //récupère la timezone avec une regex
            return ZonedDateTime.ofInstant(COMPLETE_DATE_TIME.parse(formattedDate).toInstant(), ZoneId.of(buffer[buffer.length - 1]));
        }
    }

    /**
     * Renvoie une instance de String qui est la version littérale d'une instance de {@code ZonedDateTime}
     * suivant le format suivant : {@code yyyy.MM.dd.HH:mm:ss X}
     *
     * @param zonedDateTime l'instance de {@code ZonedDateTime} dont on veut extraire la chaine de caractères.
     * @return un {@code String} suivant le format sus-cité.
     * @see SimpleDateFormat
     */
    public static String formatZonedDateTime(ZonedDateTime zonedDateTime) {
        synchronized (COMPLETE_DATE_TIME) {
            return COMPLETE_DATE_TIME.format(java.util.Date.from(zonedDateTime.toInstant()));
        }
    }

    public static String getTime() {
        return getTime(Calendar.getInstance());
    }

    /**
     * Renvoie l'heure sous forme HH:mm pour être affichée en haut à gauche de l'application
     * @return l'heure formatée
     */
    public static String getFormattedTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(Calendar.getInstance().getTime());
    }

    /**
     * Renvoie la date sous un format lisible, par exemple : "jeudi 02/03/2015"
     * @param cal l'instance de Calendar
     * @return la date
     */
    public static String getFormattedDate(Calendar cal) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE dd/MM/yyyy");
        return sdf.format(cal.getTime());
    }

    /**
     * Renvoie l'heure d'un calendrier donnée sour forme HH:mm:ss
     * @param cal le calendrier d'où l'heure doit être extraite
     * @return l'heure formatée
     */
    public static String getTime(Calendar cal) {
        return cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND) + " ";
    }

    /**
     * Retourne un timestamp de l'heure et la date actuelle
     * @return la date et heure actuelle formatée en timestamp
     */
    public static String getTimeStamped() {
        return getTimeStamped(Calendar.getInstance());
    }

    /**
     * Retourne un timestamp d'une heure et date donnée extraite d'un calendrier
     * @param cal le calendrier d'où le timestamp doit être extrait
     * @return le timestamp
     */
    public static String getTimeStamped(Calendar cal) {
        return cal.get(Calendar.YEAR) + "/" + getMonthAndDay(cal) + "-" + getTime();
    }


    public static LocalDate localDatefromCalendar(Calendar cal) {
        return LocalDate.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
    }

    public static String getMonthAndDay(Calendar cal) {
        return String.format("%1$td/%1$tm", cal);
    }

    /**
     * @param dateTime1 la première instsance à comparer
     * @param dateTime2 la deuxième instance à comparer
     * @return {@code true} si les deux instances proviennent du même jour
     */
    public static boolean areLocalDateTimeOfSameDay(LocalDateTime dateTime1, LocalDateTime dateTime2) {
        return dateTime1.getYear() == dateTime2.getYear()
                && dateTime1.getDayOfYear() == dateTime2.getDayOfYear();
    }
}
