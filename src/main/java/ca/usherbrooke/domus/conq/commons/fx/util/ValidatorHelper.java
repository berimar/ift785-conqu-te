package ca.usherbrooke.domus.conq.commons.fx.util;

import ca.usherbrooke.domus.conq.commons.CommonRegex;
import ca.usherbrooke.domus.conq.commons.DateUtil;
import ca.usherbrooke.domus.conq.commons.SecureLocalDateStringConverter;
import javafx.scene.control.Control;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.controlsfx.control.decoration.Decoration;
import org.controlsfx.control.decoration.StyleClassDecoration;
import org.controlsfx.validation.Severity;
import org.controlsfx.validation.ValidationMessage;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.AbstractValidationDecoration;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Utilitaire pour appliquer aisément un style de validation dynamique aux éléments graphiques identifiés par une classe css.
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@RequiredArgsConstructor
public enum ValidatorHelper {
    ACCENT_AND_LETTERS(".validated",
            Validator.createRegexValidator("Not a match",
                    CommonRegex.ACCENTS_AND_LETTERS_REGEX, Severity.ERROR)),
    NUMBER(".validatedNumber",
            Validator.createRegexValidator("Not a match",
                    CommonRegex.NUMBERS_REGEX, Severity.ERROR)),
    TELEPHONE(".validatedPhone",
            Validator.createRegexValidator("Not a match",
                    CommonRegex.PHONE_REGEX, Severity.ERROR)),
    EMAIL(".validatedEmail",
            Validator.createRegexValidator("Not a match",
                    CommonRegex.EMAIL_REGEX, Severity.ERROR)),
    SIMPLE_DATE(".validateSimpleDate",
            Validator.createPredicateValidator(
                    ValidatorHelper::textMatch, "Not a match",Severity.ERROR)),
    NOT_EMPTY(".validateNotEmpty",
            Validator.createEmptyValidator("", Severity.ERROR));

    /**
     * La classe css utilisée pour appliquer le validateur.
     */
    @Getter
    private final String cssClassSelector;
    private static final SecureLocalDateStringConverter STRING_DATE_CONVERTER = DateUtil.newLocalDateStringConverter();

    /**
     * L'instance de {@code Validator}
     */
    @Getter
    private final Validator validator;

    private static boolean textMatch(Object text){
        String string = (String) text;
        return string==null||string.isEmpty()||STRING_DATE_CONVERTER.stringMatch(string);
    }


    static class CustomValidationDecorator extends AbstractValidationDecoration {

        @Override
        protected Collection<Decoration> createValidationDecorations(ValidationMessage message) {
            ArrayList<Decoration> decorations = new ArrayList<>();
            decorations.add(new StyleClassDecoration("invalid"));
            //TODO rendre le message visible
            return decorations;
        }

        @Override
        protected Collection<Decoration> createRequiredDecorations(Control target) {
            ArrayList<Decoration> decorations = new ArrayList<>();
            decorations.add(new StyleClassDecoration("required"));
            return decorations;
        }
    }

}