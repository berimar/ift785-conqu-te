package ca.usherbrooke.domus.conq.commons.fx;

import ca.usherbrooke.domus.conq.commons.fx.internal.FxmlBindable;
import ca.usherbrooke.domus.conq.commons.fx.util.FXSizer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import lombok.Getter;


/**
 * date 28/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class ExtendedImageView extends StackPane implements FxmlBindable {


    @FXML
    private ImageView imageView ;

    @FXML
    private ProgressIndicator loadingIndicator ;

    @Getter
    private BooleanProperty isLoadingProperty = new SimpleBooleanProperty(false);

    public void setIsLoadingProperty(boolean isLoadingProperty){
        imageView.setImage(null);
        this.isLoadingProperty.set(isLoadingProperty);
    }

    public boolean getIsLoading(){
        return isLoadingProperty.get();
    }

    public void setImage(Image image){
        setIsLoadingProperty(false);
        imageView.setImage(image);
    }

    @FXML private void initialize(){
        imageView.setPreserveRatio(true);
        loadingIndicator.setProgress(-1.0d);
        loadingIndicator.visibleProperty().bind(isLoadingProperty);
        loadingIndicator.maxWidthProperty().bind(FXSizer.PREF_BUTTON_BOX_HEIGHT);
        loadingIndicator.maxHeightProperty().bind(FXSizer.PREF_BUTTON_BOX_HEIGHT);

    }

    @Override
    public String getFXMLPath() {
        return "ExtendedImageView.fxml";
    }

    public ExtendedImageView(){
        bindToFXML();
    }

    public void setFitWidth(double fitWidth) {
        imageView.setFitWidth(fitWidth);
    }

    public void setFitHeight(Double fitHeight) {
        imageView.setFitHeight(fitHeight);
    }

    public Image getImage() {
        return imageView.getImage();
    }
}
