package ca.usherbrooke.domus.conq.commons.app;

import ca.usherbrooke.domus.conq.supp.msg.MsgHelperFactory;
import lombok.RequiredArgsConstructor;

/**
 * Date : 15-03-23 <br/>
 * Application munie d'un contexte graphique et d'une fabrique à messages.
 *
 * @author sv3inburn3
 */
@RequiredArgsConstructor
public abstract class GraphicalLCApp<T extends GraphicalContext<?>> extends LCApp  {

    public abstract T getAppGC();

    public abstract MsgHelperFactory getMsgFactory();
}
