package ca.usherbrooke.domus.conq.commons.app;

import ca.usherbrooke.domus.conq.commons.LifeCycle;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import lombok.ToString;
import lombok.extern.java.Log;

/**
 * date 08/03/15 <br/>
 * <p>
 * Représente une application ayant un Cycle de Vie (Life Cycle Application)
 *
 * @author sv3inburn3
 */
@Log
@ToString
public abstract class LCApp implements LifeCycle {
    /**
     * L'état de l'application
     *
     * @see LCApp.State
     */
    private ObjectProperty<State> observableState = new SimpleObjectProperty<>(LCApp.State.UNLOADED);


    public LCApp() {
        observableState.addListener((obsver, oldSts, newSts) -> {
            if (oldSts == State.LOADING && newSts == State.LOADED) {
                log.info("démarrage de " + getName());
                onStart();
            } else if (oldSts == State.LOADED && newSts == State.UNLOADED) {
                log.info("arrêt de " + getName());
                onStop();
            }
        });
    }

    public State getState() {
        return observableState.get();
    }

    /**
     * Permet de s'abonner à une transition d'état
     *
     * @param stateChangeListener le  "listener" en écoute
     * @implNote implémentation du pattern Observer
     */
    public void addStateListener(ChangeListener<State> stateChangeListener) {
        observableState.addListener(stateChangeListener);
    }

    /**
     * Permet de se désabonner à une transition d'état
     *
     * @param stateChangeListener le  "listener" en écoute
     * @implNote implémentation du pattern Observer
     */
    public void removeStateListener(ChangeListener<State> stateChangeListener) {
        observableState.removeListener(stateChangeListener);
    }

    /**
     * Méthode executée au lancement de l'application
     */
    public final void start() {
        observableState.setValue(State.PRELOADING);
        observableState.setValue(State.LOADING);
        observableState.setValue(State.LOADED);
    }

    /**
     * Méthode executée à la fermeture de l'application
     */
    public final void stop() {
        observableState.setValue(State.UNLOADED);
    }

    /**
     * Méthode executée au lancement de l'application,
     * appellée par {@link #start()} lors de la transition LOADING->LOADED
     */
    protected abstract void onStart();

    /**
     * Méthode executée à la fermeture de l'application,
     * appellée par {@link #stop()}
     */
    protected abstract void onStop();

    /**
     * @return le nom de l'application (pour l'identifier dans les logs, les threads...)
     */
    protected abstract String getName();

    /**
     * L'état d'une LCApp
     */
    public enum State {
        /**
         * Chargé
         */
        LOADED,
        /**
         * En cours de chargement
         */
        LOADING,
        /**
         * Préchargement (pour instancier des éléments de feedback utilisateur par exemple)
         */
        PRELOADING,
        /**
         * Pas chargé
         */
        UNLOADED
    }

}
