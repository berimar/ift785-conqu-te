package ca.usherbrooke.domus.conq.commons.fx.model;

import ca.usherbrooke.domus.conq.commons.fx.CopiableLabel;
import ca.usherbrooke.domus.conq.commons.fx.ExtendedImageView;
import ca.usherbrooke.domus.conq.commons.fx.internal.PictureResourceHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.FXSizer;
import ca.usherbrooke.domus.conq.commons.fx.internal.PresentationBody;
import ca.usherbrooke.domus.conq.commons.fx.internal.PictureResourceHelper.*;
import ca.usherbrooke.domus.conq.db.model.Person;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;

/**
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public abstract class SeePersonBody extends PresentationBody {


    @FXML
    protected Label identityLabel;
    @FXML
    protected CopiableLabel phoneLabel;
    @FXML
    protected ExtendedImageView imageView;
    @FXML
    protected Label photoPathLabel;
    @Getter(value = AccessLevel.PROTECTED, lazy = true)
    private final PictureResourceHelper pictureResourceHelper = new PictureResourceHelper(imageView);


    /**
     * Met à jour les champs à partir de l'entité passée en paramètre
     * @param person l'entité à partir de laquelle les champs seront mis à jour.
     */
    protected void updatePersonFields(@NonNull Person person) {
        identityLabel.setText(person.toDisplayableName());
        phoneLabel.setText(person.getPhoneNumber());
        photoPathLabel.setText(person.getPhotoUri());
        getPictureResourceHelper().loadImageAsync(person.getPhotoUri(), OnErrorStrategy.DO_NOTHING);
        setPrefSizeFields();
    }

    private void setPrefSizeFields(){
        imageView.setFitWidth(FXSizer.get30PrcPrezWidth() * 2);
        imageView.setFitHeight(FXSizer.get20PrcUappHeight());
        phoneLabel.setMaxWidth(FXSizer.getPrezFieldWidth());
        identityLabel.setMaxWidth(FXSizer.getPrezFieldWidth());
    }

}
