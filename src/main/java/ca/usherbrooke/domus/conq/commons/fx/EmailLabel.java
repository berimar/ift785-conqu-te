package ca.usherbrooke.domus.conq.commons.fx;

import javafx.scene.control.Label;
import lombok.extern.java.Log;

import java.awt.*;
import java.io.IOException;
import java.net.URI;

/**
 * date 19/04/15 <br/>
 * Un {@code Label} pouvant être pressé pour lancer le client mail de l'OS
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 * @implNote Fonctionne aussi bien avec un évènement TouchPressed ou MouseClicked
 */
@Log
public class EmailLabel extends Label {
    public EmailLabel() {
        getStyleClass().add(EmailLabel.class.getSimpleName());
        onMouseClickedProperty().setValue(e -> onAction());
        onTouchPressedProperty().setValue(e -> onAction());
    }

    private void onAction() {
        new Thread(() -> {
            if (Desktop.getDesktop().isSupported(Desktop.Action.MAIL)) {
                try {
                    Desktop.getDesktop().mail(URI.create("mailTo:" + getText()));
                } catch (IOException e1) {
                    log.severe("Impossible de lancer le client mail");
                } catch (IllegalArgumentException ie) {
                    log.warning("L'email est mal formaté");
                }
            }
        }).start();
    }
}
