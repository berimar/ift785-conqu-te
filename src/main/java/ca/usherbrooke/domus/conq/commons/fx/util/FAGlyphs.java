package ca.usherbrooke.domus.conq.commons.fx.util;

import ca.usherbrooke.domus.conq.Launcher;
import javafx.scene.paint.Color;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.java.Log;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

/**
 * date 24/03/15 <br/>
 * Utilitaire de chargement des îcones graphiques FontAwesome. La version implémentée est FontAwesome 4.1
 *
 * @author sv3inburn3
 * @implNote Pour le moment la méthode 'duplicate' est bugguée, cf <a href="http://www.bytebucket.org/controlsfx/controlsfx/issue/482/glyph-bug">leur git</a>
 * dès que le bug sera corrigé, il faudra renvoyer une version dupliquée plutôt que de faire un rechargement depuis la police de fonts.
 */
@SuppressWarnings("ConstantNamingConvention")
@Log
public class FAGlyphs {

    private FAGlyphs(){}

    private static final int FA_SIZE = 16;
    //NE PAS EFFACER
    //Va être utile pour dupliquer les glyphs une fois qu'ils seront duplicable, cf implNote de la classe
    //private static final ConcurrentHashMap<FontAwesome.Glyph,Glyph> ICONS = new ConcurrentHashMap<>();

    @Getter(lazy = true)
    private static final GlyphFont fontAwesome = GlyphFontRegistry.font("FA");


    /**
     * @param color    La couleur de l'icône.
     * @param glyphRef La référence de l'item
     * @return le Glyph correspondant dupliqué (javafx interdit la présence d'un {@link javafx.scene.Node} à deux endroits).
     * @throws IllegalStateException s'il est impossible de trouver le {@code char} associé à l'icone.
     * @implNote ThreadSafe
     * @implNote l'argument {@code Color color} n'est pas pris en compte
     */
    public static
    @NonNull
    Glyph getIcon(FontAwesome.Glyph glyphRef, Color color) {
        try {
            //NE PAS EFFACER
            //en attendant le fix de controlsfx, cf implNote de la classe
            //Glyph gl =  ICONS.computeIfAbsent(glyphRef, FAGlyphs::configureIcon);
            Glyph glyph = FAGlyphs.configureIcon(glyphRef, color);
            glyph.applyCss();
            return glyph;
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Impossible de trouver le glyphicon : " + glyphRef.toString(),npe);
        }
    }

    /**
     * @param color       La couleur de l'icône.
     * @param glyphRef    La référence de l'item
     * @param fontSize_em la taille <strong>en 'em'</strong> du glyph
     * @return le Glyph correspondant dupliqué (javafx interdit la présence d'un {@link javafx.scene.Node} à deux endroits).
     * @throws IllegalStateException s'il est impossible de trouver le {@code char} associé à l'icone.
     * @implNote ThreadSafe
     * @implNote l'argument {@code Color color} n'est pas pris en compte
     */
    public static
    @NonNull
    Glyph getIcon(FontAwesome.Glyph glyphRef, Color color, float fontSize_em) {
        try {
            //NE PAS EFFACER
            //en attendant le fix de controlsfx, cf implNote de la classe
            //Glyph gl =  ICONS.computeIfAbsent(glyphRef, FAGlyphs::configureIcon);
            Glyph glyph = FAGlyphs.configureIcon(glyphRef, color, fontSize_em);
            glyph.applyCss();
            return glyph;
        } catch (NullPointerException npe) {
            throw new IllegalStateException("Impossible de trouver le glyphicon : " + glyphRef.toString(),npe);
        }
    }

    /**
     * Contourne un bug de controlsfx 8.20.8, cf <a href="http://www.bytebucket.org/controlsfx/controlsfx/issue/433/glyph-in-graphic-of-dialog-doesnt-work">ce lien</>
     *
     * @param ref la référence du glyph
     * @return le glyph référencé
     */
    private static
    @NonNull
    Glyph configureIcon(FontAwesome.Glyph ref, Color color) {
        Glyph glyph = getFontAwesome().create(ref);
        glyph.setStyle("-fx-font-family: 'FontAwesome';" +
                "-fx-text-fill: #" + color.toString().substring(2) + ";");
        return glyph;

    }

    /**
     * Contourne un bug de controlsfx 8.20.8, cf <a href="http://www.bytebucket.org/controlsfx/controlsfx/issue/433/glyph-in-graphic-of-dialog-doesnt-work">ce lien</>
     *
     * @param ref la référence du glyph
     * @return le glyph référencé
     */
    private static
    @NonNull
    Glyph configureIcon(FontAwesome.Glyph ref, Color color, float size_em) {
        Glyph glyph = getFontAwesome().create(ref);
        glyph.setStyle("-fx-font-family: 'FontAwesome';" +
                "-fx-text-fill: #" + color.toString().substring(2) + ";" +
                "-fx-font-size:" + size_em + "em;");
        return glyph;

    }

    /**
     * Charge la police FontAwesome depuis les resources.
     */
    public static void init() {
        InputStream inputStream = Launcher.class.getResourceAsStream("fontawesome/FontAwesome.ttf");
        if(inputStream!=null) {
            GlyphFontRegistry.register("FA", inputStream, FA_SIZE);
            log.info("Polices FontAwesome chargées");
        } else {
            log.severe("Impossible de trouver le fichier de police FontAwesome.ttf");
        }

    }

}
