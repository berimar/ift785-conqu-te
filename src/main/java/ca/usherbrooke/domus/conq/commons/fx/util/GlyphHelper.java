package ca.usherbrooke.domus.conq.commons.fx.util;

import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

/**
 * Un utilitaire pour faciliter l'insertion de {@code Glyph}s dans l'interface graphique, et
 * assurer un thème homogène entre les {@code UApps}
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@RequiredArgsConstructor
public enum GlyphHelper {
    ADD_HELPER(".addButton", FontAwesome.Glyph.PLUS, Color.LAWNGREEN),
    PICTURE_HELPER(".choosePhoto", FontAwesome.Glyph.PICTURE_ALT, Color.BLACK),
    HELP_HELPER(".helpButton", FontAwesome.Glyph.QUESTION, Color.WHITE),
    EDIT_HELPER(".editButton", FontAwesome.Glyph.PENCIL, Color.YELLOW),
    SAVE_HELPER(".saveButton", FontAwesome.Glyph.FLOPPY_ALT, Color.LAWNGREEN),
    DELETE_HELPER(".deleteButton", FontAwesome.Glyph.BAN, Color.RED),
    CANCEL_HELPER(".cancelButton", FontAwesome.Glyph.TIMES, Color.RED),
    CONFIRM_HELPER(".cancelButton", FontAwesome.Glyph.CHECK, Color.LAWNGREEN),
    UNBIND_HELPER(".unboundButton", FontAwesome.Glyph.UNLINK, Color.DARKGOLDENROD),
    CHOOSE_HELPER(".chooseButton", FontAwesome.Glyph.CHECK, Color.LAWNGREEN),
    PARAMS_HELPER(".parameters", FontAwesome.Glyph.COGS, Color.WHITE),
    TODAY_HELPER(".todayButton", FontAwesome.Glyph.HOME, Color.WHITE),
    TO_NEXT_HELPER(".toNext", FontAwesome.Glyph.CHEVRON_CIRCLE_RIGHT, Color.AQUA),
    TO_LAST_HELPER(".toLast", FontAwesome.Glyph.CHEVRON_CIRCLE_LEFT, Color.AQUA),
    CHOOSE_EXISTING_HELPER(".chooseExistingButton", FontAwesome.Glyph.BOOK, Color.WHITE),
    COPY_HELPER(".copyButton", FontAwesome.Glyph.COPY, Color.WHITE),
    RETURN_RIGHT_HELPER(".returnButton", FontAwesome.Glyph.FORWARD, Color.WHITE),
    RETURN_LEFT_HELPER(".returnButton", FontAwesome.Glyph.BACKWARD, Color.WHITE);

    @Getter
    private final String cssClassSelector;

    @Getter
    private final FontAwesome.Glyph glyph;

    @Getter
    private final Color buttonColor;


    /**
     * Génère une instance de {@code Glyph} unique
     *
     * @return le {@code Glyph} construit à partir de ce {@code GlyphHelper}
     */
    public Glyph newGlyph() {
        return FAGlyphs.getIcon(glyph, buttonColor);
    }

    /**
     * @return un nouveau bouton à partir du helper
     */
    public Button newButton() {
        Button button = new Button();
        button.setGraphic(newGlyph());
        return button;
    }

    /**
     * @param label le label à afficher
     * @return un nouveau bouton à partir du helper
     */
    public Button newButton(String label) {
        Button button = newButton();
        button.setText(label);
        return button;
    }

    /**
     * @return un template builder
     * @see ca.usherbrooke.domus.conq.commons.fx.util.ButtonTemplate#of
     */
    public ButtonTemplate.Builder newButtonTemplteBuildr() {
        return ButtonTemplate.of(this);
    }

}
