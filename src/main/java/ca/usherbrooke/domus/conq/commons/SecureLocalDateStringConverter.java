package ca.usherbrooke.domus.conq.commons;

import javafx.util.StringConverter;
import lombok.Getter;

import java.time.LocalDate;

/**
 * date 27/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public abstract class SecureLocalDateStringConverter extends StringConverter<LocalDate> {
    protected boolean hasParseError = false;
    @Getter
    protected String parsedString = null;

    /**
     * @return {@code true} si une erreur de conversion a eu lieu
     */
    public boolean hasParseError(){
        return hasParseError;
    }

    SecureLocalDateStringConverter(){}

    /**
     * Vérifie si la chaine de caractère est bien formattée
     * @param formattedString la chaine de caractère à contrôler
     * @return {@code true} si la chaine de caractère est bien formattée
     */
    public final boolean stringMatch(String formattedString){
        return fromString(formattedString)!=null;
    }
}
