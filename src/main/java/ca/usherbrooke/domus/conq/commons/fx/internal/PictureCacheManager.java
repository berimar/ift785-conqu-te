package ca.usherbrooke.domus.conq.commons.fx.internal;

import javafx.beans.property.SimpleLongProperty;
import javafx.scene.image.Image;
import lombok.extern.java.Log;

import java.util.HashMap;

/**
 * date 28/04/15 <br/>
 * Utilitaire pour gérer le cache des images distantes
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 * @implNote Singleton, Façade
 */
@Log
class PictureCacheManager {

    public static final long MAX_PICTURE_CACHE_PX = 15000000 ; //15 Mega pixel

    private final SimpleLongProperty currentCacheSize_px = new SimpleLongProperty();

    private static final PictureCacheManager SINGLETON = new PictureCacheManager();

    private final HashMap<String,MetaImageWrapper> cachedRemoteImages = new HashMap<>();

    private PictureCacheManager(){
        //listener pour vider du cache les images les moins consultées
        //en cas d'égalité, enlève l'image supposée la plus lourde en priorité
        currentCacheSize_px.addListener((obs,old,neu)->{
            if(neu.longValue()>MAX_PICTURE_CACHE_PX){
                synchronized (cachedRemoteImages) {
                    cachedRemoteImages.values().stream()
                            .sorted()
                            .findFirst()
                            .ifPresent((imageWrapper) ->{
                                cachedRemoteImages.remove(imageWrapper.getUriPath());
                                currentCacheSize_px.set(currentCacheSize_px.subtract(imageWrapper.getApproxPixelSize()).get());
                                log.info("Image retirée du cache : "+imageWrapper.getUriPath());
                            });
                }
            }
        });
    }

    /**
     * @return l'instance unique (singleton)
     */
    public static PictureCacheManager get(){return SINGLETON;}


    /**
     *
     * @param pictureResourceHelper le helper dont l'{@code ImageView} sera chargée avec l'image en cache
     * si elle est trouvée.
     * @param pictureURI l'adresse de la ressource, utilisé comme indentifiant unique
     * @return {@code true} si l'image a été trouvée en cache
     */
    boolean setImageIfCached(PictureResourceHelper pictureResourceHelper, String pictureURI) {

        MetaImageWrapper wrapper ;
        synchronized (cachedRemoteImages) {
            wrapper = cachedRemoteImages.get(pictureURI);
        }
        if(wrapper!=null){
            //si l'image est déjà en cache, alors on la charge
            pictureResourceHelper.getImageView().setImage(wrapper.getWrappedImage());
        }
        return wrapper!=null;
    }

    /**
     * Met en cache l'image si elle ne l'est pas déjà
     * @param uriPath l'adresse de la ressource à mettre en cache
     * @param cachedImage l'image à mettre en cache
     */
    void putIfAbsent(String uriPath,Image cachedImage){
        synchronized (cachedRemoteImages) {
            cachedRemoteImages.putIfAbsent(uriPath,
                    new MetaImageWrapper(cachedImage,currentCacheSize_px,uriPath)
            );
        }
    }

}
