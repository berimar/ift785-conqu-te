package ca.usherbrooke.domus.conq.commons.fx.util;

import com.sun.javafx.binding.ExpressionHelper;
import javafx.beans.InvalidationListener;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import lombok.Getter;
import lombok.NonNull;

import java.util.Optional;

/**
 * date 17/04/15 <br/>
 * Un {@link ObjectProperty} simple qui peut être lié de façon unidirectionnelle ou bidirectionnelle à un autre {@code ObjectProperty}
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class OptionalObservable<T> extends ObjectProperty<T> {

    @Getter
    private
    @NonNull
    Optional<T> optional = Optional.empty();

    private T backedObject = null;
    private ChangeListener<T> listener = null;
    private ExpressionHelper<T> helper = null;
    private ObservableValue<? extends T> boundObservable;

    @Override
    public T getValue() {
        return backedObject;
    }

    @Override
    public T get() {
        return getValue();
    }

    @Override
    public void set(T value) {
        if (!isBound()) {
            setUnconditionally(value);
        } else throw new IllegalStateException("Cannot set a bound value");
    }

    @Override
    public void addListener(ChangeListener<? super T> listener) {
        helper = ExpressionHelper.addListener(helper, this, listener);
    }

    @Override
    public void removeListener(ChangeListener<? super T> listener) {
        helper = ExpressionHelper.removeListener(helper, listener);
    }


    @Override
    public void addListener(InvalidationListener listener) {
        helper = ExpressionHelper.addListener(helper, this, listener);
    }

    @Override
    public void removeListener(InvalidationListener listener) {
        helper = ExpressionHelper.removeListener(helper, listener);
    }

    @Override
    public void bind(final ObservableValue<? extends T> newObservable) {
        if (newObservable == null) {
            throw new NullPointerException("Cannot bind to null");
        }
        listener = (observable, oldValue, newValue) -> setUnconditionally(newValue);
        newObservable.addListener(listener);
        boundObservable = newObservable;
    }

    @Override
    public void unbind() {
        if (isBound()) {
            optional = Optional.empty().of(boundObservable.getValue());
            boundObservable.removeListener(listener);
            boundObservable = null;
        }
    }

    @Override
    public boolean isBound() {
        return boundObservable != null;
    }

    @Override
    public Object getBean() {
        return optional.get();
    }

    @Override
    public String getName() {
        return OptionalObservable.class.getSimpleName();
    }

    protected void invalidated() {
        //
    }

    protected void fireValueChangedEvent() {
        ExpressionHelper.fireValueChangedEvent(helper);
    }

    private void setUnconditionally(T value) {
        if (value == null) {
            optional = Optional.empty();
            backedObject = null;
        } else {
            optional = Optional.of(value);
            backedObject = value;
        }
        fireValueChangedEvent();
    }
}
