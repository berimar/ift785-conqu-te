package ca.usherbrooke.domus.conq.commons.fx.util;

import javafx.scene.image.Image;
import lombok.extern.java.Log;

import java.util.logging.Level;


/**
 * date 01/05/15 <br/>
 * Utilitaire pour charger des images à partir du dossier attendu, suivant qu'elle sont disponibles
 * au déploiement ou dans un dossier associé au répertoire d'installation
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class ImageLoader {
    private ImageLoader(){}

    /**
     * Charge l'image statique (déployée à la compilation)
     * @param name le nom de l'image (ne doit pas être préfixé)
     * @return l'instance d'{@code Image} associée
     */
    public static Image staticLoad(String name){
        try {
            return new Image("file:/img/"+name);
        } catch (NullPointerException npe){
            log.log(Level.SEVERE,"Impossible de charger l'image "+name+" depuis la classe "+ImageLoader.class.getName(),npe);
            return null;
        }
    }
}
