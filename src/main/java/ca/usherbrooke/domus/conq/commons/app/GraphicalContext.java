package ca.usherbrooke.domus.conq.commons.app;

import ca.usherbrooke.domus.conq.commons.fx.util.CoherantUXHelper;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 * date 12/03/15 <br/>
 * Représente le contexte graphique d'une application.
 *
 * @author sv3inburn3
 */
public interface GraphicalContext<T extends Pane> {
    /**
     * @return le {@link javafx.scene.layout.Pane} à la racine de ce contexte graphique.
     */
    T getRootPane();

    /**
     * @return le {@link CoherantUXHelper} utilisé pour manipuler des éléments graphiques
     */
    CoherantUXHelper getCoherantUXHelper();


    /**
     * Cache l'ensemble des {@link Node}s sélectionnés par {@code cssSelector} à partir du {@code Pane} associé au contexte graphique.  <br/>
     * <p>
     * Exemple :
     * {@code hideAll(".dumbButton")}
     * <p>
     * va cacher l'ensemble des bouttons ayant la classe css "dumbButton".
     *
     * @param cssSelection la selection css à câcher
     */
    default void hideAll(String cssSelection) {
        getRootPane().lookupAll(cssSelection).forEach((node) -> {
            node.setVisible(false);
            node.setManaged(false);

        });
    }


    /**
     * Cache l'ensemble des {@link Node}s sélectionnés par {@code cssSelector} à partir du {@code Pane} associé au contexte graphique.  <br/>
     * <p>
     * Exemple : <br/>
     * {@code showAll(".dumbButton")}
     * <p>
     * va rendre visible l'ensemble des bouttons ayant la classe css "dumbButton".
     *
     * @param cssSelectionToShow la selection css à rendre visible
     */
    default void showAll(String cssSelectionToShow) {
        getRootPane().lookupAll(cssSelectionToShow).forEach((node) -> {
            node.setVisible(true);
            node.setManaged(true);
        });
    }

    /**
     * Cache l'ensemble des {@link Node}s sélectionnés par {@code cssSelectorTohide} puis
     * affiche l'ensemble des {@link Node}s sélectionnés par {@code cssSelectorShow} à partir du {@code Pane} associé au contexte graphique.  <br/>
     * <p>
     * Exemple : <br/>
     * {@code swap(".buttonText", ".buttonLabel")}
     * <p>
     * va cacher l'ensemble des bouttons ayant la classe css "buttonText" et rendre visible l'ensemble des bouttons ayant la classe css "buttonLabel".
     *
     * @param cssSelectionToHide le selecteur css des éléments à câcher
     * @param cssSelectionToShow le selecteur css des éléments à rendre visible
     */
    default void swap(String cssSelectionToHide, String cssSelectionToShow) {
        hideAll(cssSelectionToHide);
        showAll(cssSelectionToShow);
    }

    /**
     * Active (enables) l'ensemble des {@link Node}s sélectionnés par {@code cssSelectionToEnable}.
     *
     * @param cssSelectionToEnable le sélecteur des éléments à activer
     */
    default void enableAllButtons(String cssSelectionToEnable) {
        getRootPane().lookupAll(cssSelectionToEnable).forEach((node) -> node.setDisable(false));
    }

    /**
     * Désactive (disables) l'ensemble des {@link Node}s sélectionnés par {@code cssSelectionToDisable}.
     *
     * @param cssSelectionToDisable le sélecteur des éléments à activer
     */
    default void disableAllButtons(String cssSelectionToDisable) {
        getRootPane().lookupAll(cssSelectionToDisable).forEach((node) -> node.setDisable(true));
    }

    /**
     * Affiche ce contexte graphique dans le contexte parent.
     */
    void show();

    /**
     * Masque ce contexte graphique du contexte parent.
     */
    void hide();
}
