/**
 * date 11/03/15 <br/>
 * Le Package associé à l'application développée pour la supervision de la résidence La Conquête.
 * @see ca.usherbrooke.domus.conq.Launcher
 * @see ca.usherbrooke.domus.conq.uapps
 * @see ca.usherbrooke.domus.conq.supp
 * @see ca.usherbrooke.domus.conq.commons
 * @author sv3inburn3
 */
package ca.usherbrooke.domus.conq;

