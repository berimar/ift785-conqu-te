package ca.usherbrooke.domus.conq.launch;

import com.lexicalscope.jewel.cli.Option;
import lombok.Getter;

/**
 * date 10/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class UserOption {


    @Getter
    private String pseudo;

    @Getter
    private String password;

    @Option(defaultToNull = true, pattern = ".+,.+")
    public void setUsr(String usr) {
        int indexOfComa = usr.indexOf(',');
        int length = usr.length();
        pseudo = usr.substring(0, indexOfComa);
        password = usr.substring(indexOfComa + 1, length);
    }

    public boolean isValid() {
        return pseudo != null && password != null;
    }


}
