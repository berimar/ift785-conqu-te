package ca.usherbrooke.domus.conq.launch;

import ca.usherbrooke.domus.conq.commons.TimeUtils;
import lombok.ToString;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.function.Function;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * date 12/03/15 <br/>
 * Une classe pour gérer la configuration de {@link java.util.logging.Logger}
 *
 * @author sv3inburn3
 */
@ToString
final public class LoggingConfigurer {
    private final Level level;
    private final Time tconf;

    /**
     * Configurateur de {@link java.util.logging.Logger}
     *
     * @param level le niveau minimum (inclu) des logs qui ne seront pas filtrés
     * @param tconf permet d'horodater ou non les logs
     * @implNote Un niveau bas peut être très couteux en ressources.
     * @see java.util.logging.Level
     */
    public LoggingConfigurer(Level level, Time tconf) {
        this.level = level;
        this.tconf = tconf;
    }

    /**
     * Applique la configuration à l'instance de {@link java.util.logging.Logger} situé à la racine.
     */
    public void apply() {
        Logger rootLogger = Logger.getGlobal().getParent();
        rootLogger.setLevel(level);
        rootLogger.getHandlers()[0].setFormatter(getFormatter(tconf.formatFun));
    }

    private Formatter getFormatter(Function<LogRecord, String> formatFun) {
        return new Formatter() {
            @Override
            public String format(LogRecord record) {
                return formatFun.apply(record);
            }
        };
    }


    /**
     * Configuration d'un horodatage des logs.
     *
     * @implNote Exemple typique de "Patron de Fonction" qui utilise des lambda à la place de l'héritage
     */
    @SuppressWarnings("UnusedDeclaration")
    public enum Time {
        NO_TIME(Time::buildLogSkeletton),
        TIME_ALONE(Time::buildTimedLog),
        TIMESTAMP(Time::buildTimeStampedLog);
        private final Function<LogRecord, String> formatFun;

        Time(Function<LogRecord, String> formatFun) {
            this.formatFun = formatFun;
        }

        private static String buildLogSkeletton(LogRecord record) {
            //L'utilisation d'un StringBuilder n'est pas nécessaire, cf code inspection
            StringWriter errors = new StringWriter();
            if(record.getThrown()!=null)
                record.getThrown().printStackTrace(new PrintWriter(errors));
            return record.getLevel() + " {" +
                    record.getLoggerName() + "} : " +
                    record.getMessage() +"\n"+
                    errors.toString();
        }

        private static String buildTimedLog(LogRecord record) {
            return TimeUtils.getTime() + buildLogSkeletton(record);
        }

        private static String buildTimeStampedLog(LogRecord record) {
            return TimeUtils.getTimeStamped() + buildLogSkeletton(record);
        }
    }

}
