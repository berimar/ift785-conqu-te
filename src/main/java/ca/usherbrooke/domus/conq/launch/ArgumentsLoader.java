package ca.usherbrooke.domus.conq.launch;

import ca.usherbrooke.domus.conq.db.model.UserEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import com.lexicalscope.jewel.cli.CliFactory;
import lombok.extern.java.Log;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

/**
 * date 10/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class ArgumentsLoader {

   private UserOption userOption;

    /**
     * @param args les arguments passé en paramètre de l'exécutable
     * @throws com.lexicalscope.jewel.cli.ArgumentValidationException si une option est mal renseignée.
     */
    public ArgumentsLoader(String... args) {
        userOption = new UserOption();
        CliFactory.parseArgumentsUsingInstance(userOption, args);

    }

    public void applyPreStartupOptions() {
        //
    }

    public void applyPostStartupOptions() {
        if (userOption != null && userOption.isValid()) {
            logIn();
        } else {
            log.info("Aucun argument 'usr' passé en argument");
        }
    }

    private void logIn() {
        DetachedCriteria auth = UserEntity.DAO.buildCriteria();
        auth.add(Restrictions.eq("pseudo", userOption.getPseudo()));
        auth.add(Restrictions.eq("password", userOption.getPassword()));
        UserEntity queriedUser = UserEntity.DAO.getByCriteria(auth);
        if (queriedUser != null) {
            log.info("Authentification réussie : " + userOption.getPseudo());
            SupportApp.get().logIn(queriedUser);
        } else {
            log.info("Authentification échouée : " + userOption.getPseudo());
        }
    }
}
