package ca.usherbrooke.domus.conq;

import ca.usherbrooke.domus.conq.launch.LoggingConfigurer;
import ca.usherbrooke.domus.conq.commons.fx.util.FAGlyphs;
import ca.usherbrooke.domus.conq.commons.fx.util.ImageLoader;
import ca.usherbrooke.domus.conq.launch.ArgumentsLoader;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import com.lexicalscope.jewel.cli.ArgumentValidationException;
import javafx.application.Application;
import javafx.stage.Stage;
import lombok.extern.java.Log;

import java.util.logging.Level;

/**
 * date 09/03/15 <br/>
 * Point d'entrée du programme. <br/>
 * On sépare le lanceur de l'application support.
 * Ainsi on peut faire des traitements lorsque le programme est quitté, ou bien si on souhaite avoir
 * une interprétation des arguments au lancement, configurer le logger ou des bibliothèques qui n'ont
 * pas de rapport direct avec la {@link ca.usherbrooke.domus.conq.supp.SupportApp}.
 *
 * @author sv3inburn3
 * @see ca.usherbrooke.domus.conq
 */
@Log
@SuppressWarnings({"WeakerAccess", "UnusedDeclaration"})

public final class Launcher extends Application {

    private static ArgumentsLoader argumentsLoader;

    /**
     * Point d'entrée 'main'.
     *
     * @param args arguments en ligne de commande
     */
    public static void main(String... args) {
        //TODO interpréteur pour gérer le niveau de logging, le fichier de log, le format...
        new LoggingConfigurer(Level.INFO, LoggingConfigurer.Time.NO_TIME).apply();

        try {
            argumentsLoader = new ArgumentsLoader(args);
        } catch (ArgumentValidationException ave) {
            log.severe("Erreur durant le parcours des arguments, l'application va être quittée : " + ave.getMessage());
            System.exit(1);
        }
        launch(args);
    }

    @Override
    public void start(Stage primaryStage)  {
        try {
            argumentsLoader.applyPreStartupOptions();
            FAGlyphs.init();
            SupportApp.init(primaryStage).start();
            argumentsLoader.applyPostStartupOptions();
            // Why is applyPostStartupOptions() screwing up the icon ? It works if I add the icon after the method but not before...
            primaryStage.getIcons().add(ImageLoader.staticLoad("iconTry.png"));
            primaryStage.getIcons().add(ImageLoader.staticLoad("home-icon.png"));
        } catch (Exception e){
            log.log(Level.SEVERE,"Erreur lors du chargement de l'application centrale",e);
        }
    }


    @Override
    public void stop() {
        log.info("Le Launcher a reçu une commande d'arrêt ");
        SupportApp.get().stop();
        System.exit(0);
    }

}
