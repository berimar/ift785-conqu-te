package ca.usherbrooke.domus.conq.uapps.jfxtagenda;

import ca.usherbrooke.domus.conq.commons.fx.util.FAGlyphs;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.supp.SupportAppGC;
import ca.usherbrooke.domus.conq.uapps.UApp;
import ca.usherbrooke.domus.conq.uapps.UAppGC;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import jfxtras.internal.scene.control.skin.agenda.AgendaWeekSkin;
import jfxtras.scene.control.agenda.Agenda;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.java.Log;
import org.controlsfx.control.PopOver;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

/**
 * Contexte graphique de l'application JFXTrasAgenda
 * <br />
 * Created by Jonathan on 27/03/2015.
 */
@ToString
@Log
public class JFXTrasAgendaGC extends UAppGC {


    private PopOver eventDialog;

    @Getter(lazy = true, value = AccessLevel.PRIVATE)
    private final Agenda agenda = ((JFXTrasAgenda) uapp).getAgenda();

    @Getter(lazy = true, value = AccessLevel.MODULE)
    private final AgendaWeekSkin weekSkin = ((AgendaWeekSkin) getAgenda().getSkin());

    @Getter
    private final Glyph glyphApp = FAGlyphs.getIcon(FontAwesome.Glyph.CALENDAR, Color.LIGHTGOLDENRODYELLOW);

    /**
     * Instancie le contexte graphique de la micro application.
     *
     * @param uapp         la micro application
     * @param supportAppGC le contexte graphique de l'application support
     * @throws IllegalStateException si le nom du fichier .fxml attaché à l'{@link ca.usherbrooke.domus.conq.uapps.UApp} ne respecte pas les conventions de nommage ou si son contenu est erronné.
     */
    public JFXTrasAgendaGC(UApp<?> uapp, SupportAppGC supportAppGC) {
        super(uapp, supportAppGC);
    }

    @Override
    public void onRootPaneLoading(AnchorPane rootPane) {
        coherantUXHelper.addGlyphToButtonClasses(
                GlyphHelper.TO_NEXT_HELPER,
                GlyphHelper.TO_LAST_HELPER,
                GlyphHelper.TODAY_HELPER);
    }

    /**
     * Méthode utilisée pour afficher le panneau de création/modification/visualisation d'activité s'il y en avait un
     * d'ouvert avant que l'application soit cachée
     */
    @Override
    public void onFocusGained() {
        if (eventDialog != null)
            eventDialog.show(getAgenda());
    }

    /**
     * Méthode utilisée pour cacher le panneau de création/modification/visualisation d'activité s'il y en a un
     * d'ouvert lorsque que l'application est cachée
     */
    @Override
    public void onFocusLost() {
        if (eventDialog != null)
            eventDialog.hide();
    }


    public void refresh() {
        getAgenda().refresh();
    }
}
