package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.OptionalObservable;
import ca.usherbrooke.domus.conq.commons.fx.model.SeeConnectedPersonBody;
import ca.usherbrooke.domus.conq.db.model.ContactEntity;
import ca.usherbrooke.domus.conq.db.model.ContactsResidentsEntity;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import lombok.NonNull;
import lombok.extern.java.Log;

/**
 * Classe qui s'occupe du comportement lors de la visualisation d'un contact
 * <br />
 * date 17/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class SeeContactBody extends SeeConnectedPersonBody {


    @FXML
    private Label relationLabel;

    private ContactListView contactView;
    private EditContactBody editContactMenu;
    private final OptionalObservable<ContactsResidentsEntity> selectedAssociation = new OptionalObservable<>();
    private final OptionalObservable<ResidentEntity> selectedResident = new OptionalObservable<>();


    public SeeContactBody() {
        bindToFXML();
        buildButtonsTemplates();
    }

    public void bind(@NonNull final ContactListView contactView,
                     @NonNull final EditContactBody editContactMenu,
                     @NonNull final OptionalObservable<ContactsResidentsEntity> selectedAssociation,
                     @NonNull final OptionalObservable<ResidentEntity> selectedResident) {
        this.contactView = contactView;
        this.editContactMenu = editContactMenu;
        this.selectedAssociation.bindBidirectional(selectedAssociation);
        this.selectedResident.bindBidirectional(selectedResident);
        isShowingProperty.addListener((observable, oldValue, newValue) -> {
            if (newValue && !oldValue) {
                selectedAssociation.getOptional().ifPresent(this::updateAssociationFields);
            }
        });
        selectedAssociation.addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                //auto hide car lié à un SlidingPresentation
                hide();
                System.out.println("CALLING HIDE FROM SEECONTACT...");
            } else {
                updateAssociationFields(newValue);
                show();
            }
        });
    }

    public void buildButtonsTemplates() {
        buttonsTemplates.add(GlyphHelper.RETURN_RIGHT_HELPER.newButtonTemplteBuildr()
                .tooltip("Cacher le menu")
                .onAction(this::hide)
                .build());
        buttonsTemplates.add(GlyphHelper.EDIT_HELPER.newButtonTemplteBuildr()
                .onAction(this::editContact)
                .label("Modifier")
                .tooltip("Modifier le contact")
                .build());
        buttonsTemplates.add(GlyphHelper.UNBIND_HELPER.newButtonTemplteBuildr()
                .label("Détacher")
                .tooltip("Détacher le contact.\nIl ne sera pas supprimé")
                .onAction(this::detachContact)
                .build());
        buttonsTemplates.add(GlyphHelper.DELETE_HELPER.newButtonTemplteBuildr()
                .label("Supprimer")
                .tooltip("Supprimer définitivement le contact")
                .onAction(this::deleteContact)
                .build());
    }


    @Override
    public String getFXMLPath() {
        return "SeeContactBody.fxml";
    }

    /**
     * Détachement du contact par rapport au résident, mais le contact ne sera pas supprimé
     */
    private void detachContact() {
        log.info("Détachement du contact");
        selectedResident.get().getContactEntities().remove(selectedAssociation.get().getContactEntity());
        ResidentEntity.DAO.update(selectedResident.get());
        contactView.detachPerson(selectedAssociation.get().getContactEntity());
        SupportApp.get().getMsgFactory().informer("Le contact a bien été détaché de " + selectedResident.get().toDisplayableName()).show();
    }

    private void editContact() {
        showNext(editContactMenu);
    }

    /**
     * Suppression d'un contact, avec notification permettant d'annuler l'opération.
     */
    private void deleteContact() {
        final ContactsResidentsEntity association = selectedAssociation.get();
        ContactEntity.DAO.delete(association.getContactEntity());
        contactView.getPersons().remove(association.getContactEntity());
        SupportApp.get().getMsgFactory().canceller("Le contact " + association.getContactEntity().toDisplayableName() +
                        " a bien été supprimé",
                () -> {
                    association.getContactEntity().setId(0);
                    ContactEntity.DAO.save(association.getContactEntity());
                    ContactsResidentsEntity.DAO.save(association);
                    contactView.getPersons().add(association.getContactEntity());
                    SupportApp.get().getMsgFactory().confirmer("Le contact " + association.getContactEntity().toDisplayableName() +
                            " a bien été restauré").show();
                }
        ).show();
    }

    protected void updateAssociationFields(ContactsResidentsEntity association) {
        super.updateConnectedPersonFields(association.getContactEntity());
        relationLabel.setText(association.getRelationType().getType());
    }


}
