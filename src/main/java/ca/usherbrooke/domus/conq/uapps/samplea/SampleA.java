package ca.usherbrooke.domus.conq.uapps.samplea;

import ca.usherbrooke.domus.conq.supp.SupportAppGC;
import ca.usherbrooke.domus.conq.uapps.Ignore;
import ca.usherbrooke.domus.conq.uapps.UApp;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.java.Log;

/**
 * date 09/03/15 <br/>
 * Une application simplicime pour l'exemple.
 *
 * @author sv3inburn3(jules.randolph[at]reseau.eseo.fr)
 * @see ca.usherbrooke.domus.conq.uapps
 */
@Log
@ToString
@SuppressWarnings({"UnusedDeclaration", "MethodParameterOfConcreteClass"})
@Ignore
public class SampleA extends UApp<SampleAGC> {

    @Getter
    private final String displayedName = "ExempleA";

    @SuppressWarnings({"JavaDoc"})
    public SampleA(SupportAppGC supportAppGC) {
        super(supportAppGC, SampleAGC.class);
    }

    @Override
    public void onStart() {
        log.info("Hello world!");
    }

    @Override
    public void onStop() {
        log.info("Good bye world!");
    }
}
