package ca.usherbrooke.domus.conq.uapps.jfxtagenda.viewctrl;


import ca.usherbrooke.domus.conq.commons.TimeUtils;
import ca.usherbrooke.domus.conq.commons.fx.CopiableText;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.uapps.jfxtagenda.JFXActivity;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;
import lombok.ToString;
import lombok.extern.java.Log;

/**
 * Classe qui s'occupe du comportement de la visualisation d'activités (pas la modification)
 * <br />
 * Created by Jonathan on 03/04/2015.
 */
@SuppressWarnings("MalformedFormatString")
@Log
@ToString
public class SeeActivityBody extends ActivityBody {

    @FXML
    private AnchorPane anchor;

    @FXML
    private Text titre;
    @FXML
    private CopiableText lieu;
    @FXML
    private Label timePeriod;
    @FXML
    private Label optionalDate;
    @FXML
    private Text residents;
    @FXML
    private Text accompagnateurs;
    @FXML
    private CopiableText description;

    @FXML
    private Region colorChosenGroup;
    @FXML
    private Label chosenGroupEvent;

    public SeeActivityBody(JFXActivity activity1) {
        super(activity1);
        bindToFXML();
        buttonsTemplates.add(GlyphHelper.RETURN_LEFT_HELPER.newButtonTemplteBuildr()
                        .label("Cacher")
                        .onAction(this::hide)
                        .build()
        );

    }

    @FXML
    private void initialize() {
        Platform.runLater(() -> {
            titre.setText(activity.getSummary());
            lieu.setText(activity.getLocation());
            setTime();
            description.setText(activity.getDescription());

            String residentString;
            if (activity.getResidents().isEmpty()) {
                residentString = "Pas de resident pour cette activité";
            } else {
                residentString = activity.getResidents().stream().map((resident) -> resident.getFirstName() + " " + resident.getLastName()).reduce("", (residentStr, resident) -> residentStr + resident + "\n");
            }

            String personnelString;
            if (activity.getPersonnels().isEmpty()) {
                personnelString = "Pas d'accompagnateur pour cette activité";
            } else {
                personnelString = activity.getPersonnels().stream().map((personnel) -> personnel.getFirstName() + " " + personnel.getLastName()).reduce("", (personnelStr, personnel) -> personnelStr + personnel + "\n");
            }
            residents.setText(residentString);
            accompagnateurs.setText(personnelString);
            chosenGroupEvent.setText(activity.getAppointmentGroup().getDescription());
            colorChosenGroup.getStyleClass().add(activity.getAppointmentGroup().getStyleClass());
        });
    }

    /**
     * Met à jour le temps sur la vue en fonction du temps de l'activité
     */
    private void setTime() {
        if (TimeUtils.areLocalDateTimeOfSameDay(activity.getStartLocalDateTime(), activity.getEndLocalDateTime())) {
            timePeriod.setText(String.format("%1$tR", activity.getStartLocalDateTime()) + " → " + String.format("%1$tR", activity.getEndLocalDateTime()));
            optionalDate.setText(String.format("%1$ta %1$te %1$tb %1$tY", activity.getStartLocalDateTime()));
        } else {
            timePeriod.setText(String.format("%1$td/%1$tm/%1$ty %1$tR", activity.getStartLocalDateTime()) + " → " + String.format("%1$td/%1$tm/%1$ty  %1$tR", activity.getEndLocalDateTime()));
        }
    }

    /**
     * Méthode qui enlève les 2 dernières lettres d'une string.
     * Utilisée pour enlever la ", " à la fin des listes de résidents et d'accompagnateurs
     *
     * @param s la string à réduire
     * @return la string réduite
     */
    private String removeTwoLastChar(String s) {
        if (s == null || (s.length() < 2)) {
            return s;
        }
        return s.substring(0, s.length() - 2);
    }

    @Override
    public String getFXMLPath() {
        return "SeeActivityBody.fxml";
    }
}
