/**
 * date 11/03/15 <br/>
 * Package destiné à l'implémentation des {@link ca.usherbrooke.domus.conq.uapps.UApp}s <br/>
 * S'inspirer de l'implémentation {@link ca.usherbrooke.domus.conq.uapps.samplea.SampleA} pour développer sa propre micro-application.
 * @implSpec
 * C'est l'application centrale (application support) qui instancie les UApps en utilisant la réfléxivité.
 * Rien à faire de votre côté, sinon <strong>respecter ces spécifications</strong> : <br/>
 * <ul>
 *     <li>La classe <strong>'C'</strong> héritant de UApp doit être contenue au premier niveau d'un sous-package de uapps, que l'on nomera pour l'exemple <strong>'upkg'</strong>.</li>
 *     <li>La classe <strong>'C'</strong> doit avoir comme type paramétré <strong>une implémentation concrête de {@link ca.usherbrooke.domus.conq.uapps.UAppGC}</strong> pour déléguer la gestion graphique (l'instanciation d'éléménts javafx etc...)</li>
 *     <li>La classe <strong>'C'</strong> doit définir un constructeur dont la signature est {@code public C(SupportAppGC supportAppGC)}</li>
 *     <li>Il ne peut pas y avoir deux implémentations concrêtes de {@link ca.usherbrooke.domus.conq.uapps.UApp} portant le même nom, <strong>même dans des packages distincts</strong>.</li>
 *     <li>Le package <strong>'upkg'</strong> doit comprendre :  {<strong>'C'</strong>},{'viewctrl'}, et éventuellement {'model'} pour les classes relatives au modèle</li>
 *     <li>Le package 'viewctrl' doit comporter un fichier de description .fxml dont le nom est <strong>'C'Layout.fxml</strong>, avec <strong>'C'</strong> le nom de la classe</li>
 *     <li>Une UApp ne sera pas instanciée au démarrage de l'application si la classe <strong>'C'</strong> est annotée par {@code @Ignore}</li>
 * </ul>
 *  <strong>Le non respect de ces spécifications ne donne aucune garantie sur le bon fonctionnement de l'UApp.</strong>
 * @implNote
 * Il est de plus <strong> très recommandé </strong> - mais pas obligatoire - de suivre les conventions suivantes :
 * <ul>
 *     <li> Il est souhaitable que le package 'viewctrl' comporte un controlleur héritant de {@code AppController} dont le type paramétré est 'C'.</li>
 *     <li> Pour être cohérant, utiliser le format <strong>'C'Controller</strong> pour nommer ce controlleur</li>
 *     <li> Donner un nom de package <strong>'upkg'</strong> semblable au nom de 'C', mais n'excédant pas 8 caractères</li>
 * <ul>
 *
 * @see ca.usherbrooke.domus.conq.uapps.Ignore
 * @see ca.usherbrooke.domus.conq.commons.app.AppController
 */
package ca.usherbrooke.domus.conq.uapps;