package ca.usherbrooke.domus.conq.uapps.dossresid;

import ca.usherbrooke.domus.conq.commons.fx.util.FAGlyphs;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.supp.SupportAppGC;
import ca.usherbrooke.domus.conq.uapps.UApp;
import ca.usherbrooke.domus.conq.uapps.UAppGC;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import lombok.Getter;
import lombok.extern.java.Log;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

/**
 * Contexte graphique de l'application Dossier Résidents
 *
 * @author Claire
 */
@Log
public class DossierResidentsGC extends UAppGC {

    @Getter
    private final Glyph glyphApp = FAGlyphs.getIcon(FontAwesome.Glyph.USERS, Color.DARKCYAN);

    /**
     * Instancie le contexte graphique de la micro application.
     *
     * @param uapp         la micro application
     * @param supportAppGC le contexte graphique de l'application support
     * @throws IllegalStateException si le nom du fichier .fxml attaché à l'{@link ca.usherbrooke.domus.conq.uapps.UApp} ne respecte pas les conventions de nommage ou si son contenu est erronné.
     */
    public DossierResidentsGC(UApp<?> uapp, SupportAppGC supportAppGC) {
        super(uapp, supportAppGC);
    }

    /**
     * Méthode appellée juste après le chargement "paresseux" de l'ancre de l'{@code UApp}.
     *
     * @param rootPane l'ancre javafx de l'{@link UApp}
     * @implNote il est strictement interdit d'appeller getRootPane() depuis cette méthode, ou bien une {@code StackOverflowException} sera levée.
     */
    @Override
    public void onRootPaneLoading(AnchorPane rootPane) {
        coherantUXHelper.addGlyphToButtonClasses(
                GlyphHelper.ADD_HELPER);
        addToolTip();
    }

    /**
     * Ajoute un texte quand on hover
     */
    // TODO : est-ce que ça a un effet sur une tablette touch ?
    private void addToolTip() {
        coherantUXHelper.applyTooltipToId("#addResident", "Ajouter un résident");
        coherantUXHelper.applyTooltipToClasses(".choosePhoto", "choisir une photo\npeut être un fichier local\nou une adresse http");
    }
}
