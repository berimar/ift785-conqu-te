package ca.usherbrooke.domus.conq.uapps.gestion.viewctrl;

import ca.usherbrooke.domus.conq.commons.CommonRegex;
import ca.usherbrooke.domus.conq.commons.fx.util.GraphicsUtils;
import ca.usherbrooke.domus.conq.db.DataProvider;
import ca.usherbrooke.domus.conq.db.WrappedListDAO;
import ca.usherbrooke.domus.conq.db.model.ApartmentEntity;
import ca.usherbrooke.domus.conq.db.model.GestionTypeEntity;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import lombok.extern.java.Log;


/**
 * Décrit le comportement de la partie des appartements dans l'app Gestion. Permet de visualiser, d'ajouter, de
 * supprimer des appartements.
 *
 * date 08/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class ApartmentsController extends GestionTemplate {

    @FXML private TableView<ApartmentEntity> appartments;
    @FXML private TableColumn<ApartmentEntity, Integer> numsAppartment;

    @FXML private TableView<ResidentEntity> residentList;
    @FXML private TableColumn<ResidentEntity, String> residentNameColumn;

    @FXML private HBox slidingNewApartmentPane;
    @FXML private Button deleteButton;
    @FXML private TextField numberNewApartment;

    private final WrappedListDAO<ApartmentEntity> apartmentEntities = DataProvider.get().getApartments();


    @FXML
    public void initialize() {
        numsAppartment.setCellValueFactory(new PropertyValueFactory<>("apartmentNumber"));
        appartments.setItems(apartmentEntities.getUnmodifiableList());

        // Listener pour écouter le changement de sélection dans la liste d'apparts
        appartments.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    GraphicsUtils.showHidePane(slidingNewApartmentPane.minWidthProperty(), 0, SLIDING_DURATION);
                    slidingNewApartmentPane.setVisible(false);
                    showResidentsApartment(newValue);
                });
        // Disable le boutton supprimer si des résidents dans l'appart
        deleteButton.disableProperty().bind(Bindings.isNotEmpty(residentList.getItems()));
        residentNameColumn.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getFirstName() + " " + cell.getValue().getLastName()));
    }


    /**
     * Affiche les résidents associés à un appartement donné en argument.
     * <br>
     * Si des résidents sont affichés, alors le bouton "Supprimer" est grisé (disabled) pour empêcher la suppression d'un
     * appartement occupé par des résidents
     *
     * @param apartment l'appartement pour lequel afficher les résidents associés
     */
    @FXML
    private void showResidentsApartment(ApartmentEntity apartment) {
        residentList.getItems().clear();
        residentList.getItems().addAll(ResidentEntity.DAO.getListByHQLStatement("FROM ResidentEntity res WHERE res.apartmentEntity.apartmentNumber=" + apartment.getApartmentNumber()));
    }


    /**
     * Affiche la partie d'ajout d'appartement (sliding panel)
     *
     * @param actionEvent l'évènement déclenché par le clic
     */
    @FXML
    public void add(ActionEvent actionEvent) {
        GraphicsUtils.showHidePane(slidingNewApartmentPane.minWidthProperty(), 600, SLIDING_DURATION);
        slidingNewApartmentPane.setVisible(true);
    }




    /**
     * Méthode appelée quand on clique sur le bouton "Supprimer" pour supprimer un appartement
     * @param actionEvent l'évènement du clic
     */
    @FXML public void delete(ActionEvent actionEvent) {
        ApartmentEntity selectedApartment = (ApartmentEntity) getListSelectedItem(appartments);
        deleteElement(selectedApartment, appartments);
    }

    @Override
    protected void deleteInDB(GestionTypeEntity selectedApartment) {
        apartmentEntities.delete((ApartmentEntity) selectedApartment);
    }




    /**
     * Méthode appelée lorsqu'on clique sur le bouton "OK" pour ajouter un appartement.
     * <br>
     * Vérifie l'input dans le textfield et l'enregistre en base si correct, sinon affiche un message d'erreur
     * pour que l'utilisateur corrige son input.
     */
    @FXML
    public void saveApartmentNumber() {
        // TODO : visual validation

        saveGestionElement(numberNewApartment, slidingNewApartmentPane, CommonRegex.NUMBERS_REGEX);
    }

    /*
     * {@inheritDoc}
     */
    @Override
    protected void dbSaveAndUpdateView(GestionTypeEntity newNumber) {
        apartmentEntities.save((ApartmentEntity) newNumber);
    }

    /*
     * {@inheritDoc}
     */
    @Override
    protected String getNameOfGestion() {
        return "Le numéro d'appartement";
    }

    /*
     * {@inheritDoc}
     */
    @Override
    protected ObservableList getExistingGestionList(GestionTypeEntity newNumber) {
        return appartments.getItems().filtered(item -> (item.getApartmentNumber().equals(((ApartmentEntity) newNumber).getApartmentNumber())));
    }


    /*
     * {@inheritDoc}
     */
    @Override
    protected ApartmentEntity createTypeEntity(TextField textFieldToSave) {
        ApartmentEntity newNumber = apartmentEntities.newInstance();
        newNumber.setApartmentNumber(Integer.parseInt(numberNewApartment.getText()));
        return newNumber;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getErrorMessageForGestion() {
        return "Le numéro d'appartement doit être composé de chiffres uniquement";
    }

}
