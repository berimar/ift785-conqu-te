package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.db.model.ContactEntity;
import ca.usherbrooke.domus.conq.db.model.ContactsResidentsEntity;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import lombok.extern.java.Log;

/**
 * Classe qui s'occupe du comportement lors de l'édition d'un contact
 * <br />
 * date 19/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class EditContactBody extends WriteContactBody {

    /**
     * Nouvelle instance
     */
    public EditContactBody() {
        buttonsTemplates.add(GlyphHelper.SAVE_HELPER.newButtonTemplteBuildr()
                .label("Sauver")
                .tooltip("Sauvegarder les modification")
                .onAction(this::saveContactAssociation)
                .build());
        isShowingProperty.addListener((observable, oldValue, newValue) -> {
            if (newValue && !oldValue) {
                uploadAssociationFields(selectedAssociation.get());
            }
        });
    }

    @Override
    protected void onContactAssociationSaving() {
        log.info("Updating contact...");
        commitSelectedAssociation(selectedAssociation.get());
        selectedAssociation.get().setRelationType(relationField.getValue());
        ContactEntity.DAO.update(selectedAssociation.get().getContactEntity());
        ContactsResidentsEntity.DAO.update(selectedAssociation.get());
        selectedAssociation.set(selectedAssociation.get());
        ResidentEntity.DAO.refresh(selectedResident.get());
        contactView.setData(selectedResident.get().getContactEntities());
        contactView.refreshView();
        SupportApp.get().getMsgFactory().informer("Le contact a bien été modifié").show();
    }


    /**
     * Méthode appelée lors de la modification , pour afficher les informations dans les textfields et labels
     * @param contactAssociation l'association à afficher
     */
    private void uploadAssociationFields(ContactsResidentsEntity contactAssociation) {
        ContactEntity contact = contactAssociation.getContactEntity();
        uploadConnectedPersonFields(contact);
        relationField.getSelectionModel().select(contactAssociation.getRelationType());
    }
}
