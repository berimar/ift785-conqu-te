package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.db.model.ContactEntity;
import ca.usherbrooke.domus.conq.db.model.ContactsResidentsEntity;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import lombok.extern.java.Log;

/**
 * Classe qui s'occupe du comportement lors de la création d'un nouveau contact
 * <br >
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class NewContactBody extends WriteContactBody {

    public NewContactBody() {
        buttonsTemplates.add(GlyphHelper.SAVE_HELPER.newButtonTemplteBuildr()
                .label("Sauver les modifications")
                .tooltip("Enregistrer le contact")
                .onAction(this::saveContactAssociation)
                .build());
    }

    @Override
    protected void onContactAssociationSaving() {
        log.info("New contact...");
        ContactEntity selectedContact = new ContactEntity();
        // Ajout de la relation dans la BD
        ContactsResidentsEntity newAssociation = new ContactsResidentsEntity();
        newAssociation.setContactEntity(selectedContact);
        newAssociation.setResidentEntity(selectedResident.get());
        commitSelectedAssociation(newAssociation);
        selectedAssociation.set(newAssociation);
        //après persistance, car l'id ne doit pas être null
        contactView.getPersons().add(selectedContact);
        ContactEntity.DAO.save(selectedContact);
        ContactsResidentsEntity.DAO.save(selectedAssociation.get());

        ResidentEntity.DAO.refresh(selectedResident.get());
        SupportApp.get().getMsgFactory().confirmer("Le contact a bien été enregistré").show();
    }
}
