package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.db.model.ResidentEntity;

/**
 * Classe qui s'occupe du comportement lors de la création d'un nouveau résident
 * <br />
 *
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class NewResidentBody extends WriteResidentBody {

    @Override
    protected void onInputValidation() {
        // New
        commitResidentFields(new ResidentEntity());
        selectedResident.getOptional().ifPresent(residents::add);
    }

    @Override
    protected String getSaveLabel() {
        return "Sauver le nouveau Résident";
    }

    @Override
    protected String getCancelLabel() {
        return "Annuler la création";
    }

    @Override
    protected void onInitialize() {
        isShowingProperty.addListener((obs, old, neu) -> {
            if (!old && neu) {
                clearResidentFields();
            }
        });
    }
}
