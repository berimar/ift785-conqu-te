package ca.usherbrooke.domus.conq.uapps.samplea.viewctrl;

import ca.usherbrooke.domus.conq.commons.app.AppController;
import ca.usherbrooke.domus.conq.uapps.samplea.SampleA;

/**
 * date 08/03/15
 *
 * @author sv3inburn3(jules.randolph[at]reseau.eseo.fr)
 */
public class SampleAController extends AppController<SampleA> {

}
