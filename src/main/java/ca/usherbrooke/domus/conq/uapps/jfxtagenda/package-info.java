/**
 * date 26/03/15 <br/>
 *
 * Un package dédié à l'application Agenda permettant aux accompagnateurs de gérer les évènements de la résidence.
 * @author sv3inburn3
 */
package ca.usherbrooke.domus.conq.uapps.jfxtagenda;