package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;


import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;

/**
 * Classe qui s'occupe du comportement lors de l'édition d'un résident
 * <br />
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class EditResidentBody extends WriteResidentBody {


    @Override
    protected void onInputValidation() {
        commitResidentFields(selectedResident.get());
        ResidentEntity.DAO.update(selectedResident.get(), (constraintViolation) -> SupportApp.get().getMsgFactory().warner("Le numéro d'appartement doit exister!").show());
        residentListView.refreshView();
    }

    @Override
    protected String getSaveLabel() {
        return "Sauver les modifications";
    }

    @Override
    protected String getCancelLabel() {
        return "Annuler les modification";
    }

    @Override
    protected void onInitialize() {
        isShowingProperty.addListener((obs, old, neu) -> {
            if (!old && neu) {
                uploadResidentFields(selectedResident.get());
            }
        });
    }
}
