package ca.usherbrooke.domus.conq.uapps;

import ca.usherbrooke.domus.conq.commons.app.GraphicalLCApp;
import ca.usherbrooke.domus.conq.supp.SupportAppGC;
import ca.usherbrooke.domus.conq.supp.msg.MsgHelperFactory;
import lombok.Getter;
import lombok.ToString;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

/**
 * date 08/03/15 <br/>
 * La superclasse d'une micro application exécutée par l'application support. </br>
 *
 * @author sv3inburn3
 * @implSpec <strong>Pour les spécifications de l'implémentation</strong>, voir {@link ca.usherbrooke.domus.conq.uapps}
 */

@SuppressWarnings("MethodParameterOfConcreteClass")
@ToString
public abstract class UApp<T extends UAppGC> extends GraphicalLCApp<UAppGC> {

    @Getter(lazy = true)
    private final T appGC = buildGC();

    @Getter
    private final String name;
    private final SupportAppGC supportAppGC;
    private final Class<T> tClass;

    @Getter(lazy = true)
    private final MsgHelperFactory msgFactory = supportAppGC.getMsgFactory();

    /**
     * Ce constructeur ne doit jamais être appellée directement. <br/>
     * C'est la classe {@link ca.usherbrooke.domus.conq.supp.SupportApp}
     * qui s'en charge via la reflexivité. <br/>
     * <strong>Pour garantir que l'implémentation fonctionne, il faut se conformer aux spécifications décrites
     * dans le package {@link ca.usherbrooke.domus.conq.uapps}.</strong>
     *
     * @param supportAppGC le contexte graphique de l'application support.
     * @param tClass       la classe GC correspondante
     * @throws IllegalStateException si le constructeur de la classe paramétrée {@code T} ne respecte pas les spécifications de {@link UAppGC}
     */
    public UApp(SupportAppGC supportAppGC, Class<T> tClass) {
        super();
        if (Modifier.isAbstract(tClass.getModifiers())) {
            throw new IllegalArgumentException("tClass doit être une classe concrête. " +
                    "Il est très souhaitable d'implémenter cette classe soi même pour lui déléguer toute " +
                    "la gestion graphique de l'UApp et avoir une bonne séparation modèle/vue/contrôleur." +
                    "Mais vous pouvez au début du dévelpopement utiliser par défault la classe UAppGC.Default");
        }
        this.name = this.getClass().getSimpleName();
        this.supportAppGC = supportAppGC;
        this.tClass = tClass;

    }

    private T buildGC() {
        try {

            Constructor<T> constructor = tClass.getDeclaredConstructor(UApp.class, SupportAppGC.class);
            return constructor.newInstance(this, supportAppGC);
        } catch (Exception e) {
            IllegalStateException ise = new IllegalStateException(e);
            throw ise;
        }
    }

    /**
     * @return le nom affiché dans la barre de lancement.
     */
    public abstract String getDisplayedName();

    @Override
    public void onStart() {
    }

    @Override
    public void onStop() {
        getAppGC().hide();
    }

}
