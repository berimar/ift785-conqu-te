package ca.usherbrooke.domus.conq.uapps.gestion.viewctrl;

import ca.usherbrooke.domus.conq.commons.CommonRegex;
import ca.usherbrooke.domus.conq.commons.fx.internal.PresentationBody;
import ca.usherbrooke.domus.conq.commons.fx.model.ComboBoxWrapper;
import ca.usherbrooke.domus.conq.commons.fx.util.ValidatorHelper;
import ca.usherbrooke.domus.conq.db.model.PersonnelEntity;
import ca.usherbrooke.domus.conq.db.model.PersonnelTypeEntity;
import ca.usherbrooke.domus.conq.db.model.UserEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;
import lombok.Getter;
import lombok.extern.java.Log;

import java.util.List;

/**
 * --- Patron de conception Template ---
 * <br />
 * Contrôle les actions communes à l'ajout et la modification de personnel.
 *
 * @author Claire
 */
@Log
public abstract class WriteUserBody extends PresentationBody {

    @FXML protected TextField lastName;
    @FXML protected TextField firstName;
    @FXML protected TextField email;
    @FXML protected TextField phone;
    @FXML protected TextField login;
    @FXML protected TextField password;
    @FXML protected ComboBox<PersonnelTypeEntity> comboType;
    @FXML protected CheckBox isUserCheckBox;
    @FXML protected GridPane loginPassPane;

    @Getter
    @FXML protected Label labelPersonnel;

    @FXML protected TableView<PersonnelEntity> usersTableView;
    protected ObservableList<UserEntity> userEntities;

    protected final ObservableList<PersonnelTypeEntity> typePersonnels = FXCollections.observableArrayList(PersonnelTypeEntity.DAO.getAll());

    WriteUserBody() {
        bindToFXML();
    }

    @FXML
    private void initialize() {
        comboType.setItems(typePersonnels);
        comboType.setVisibleRowCount(4);
        comboType.setCellFactory((comboBox) -> new ListCell<PersonnelTypeEntity>() {
            @Override
            protected void updateItem(PersonnelTypeEntity item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                } else {
                    setText(item.getType());
                }
            }
        });
        comboType.setConverter(new StringConverter<PersonnelTypeEntity>() {
            @Override
            public String toString(PersonnelTypeEntity item) {
                if (item == null) {
                    return null;
                } else {
                    return item.getType();
                }
            }
            @Override
            public PersonnelTypeEntity fromString(String groupString) {
                return null;
            }
        });
        isUserCheckBox.selectedProperty().addListener((ov, oldValue, newValue) -> {
            if(newValue) {
                loginPassPane.setDisable(false);
            } else {
                loginPassPane.setDisable(true);
            }
        });
    }

    protected void clearFields() {
        lastName.clear();
        firstName.clear();
        email.clear();
        phone.clear();
        login.clear();
        password.clear();
        comboType.getSelectionModel().clearSelection();
        isUserCheckBox.setSelected(false);
    }

    /**
     * Lie les différents composants fxml passés en arguments avec ceux de ce controller
     * @param usersTableView le tableView contenant le personnel
     * @param userEntities la liste d'utilisateurs
     */
    protected void associate(TableView<PersonnelEntity> usersTableView, ObservableList<UserEntity> userEntities) {
        this.usersTableView = usersTableView;
        usersTableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    hide();
                });

        this.userEntities = userEntities;
    }

    /**
     * Méthode utilisée pour la création ou l'édition
     */
    @FXML abstract public void createUpdateUser();


    /**
     * Met à jour l'utilisateur donné en argument avec les valeurs des TextFields
     * @param newUser l'utilisateur à mettre à jour
     * @return l'utilisateur mis à jour
     */
    protected UserEntity setUser(UserEntity newUser) {
        newUser.setPseudo(login.getText());
        newUser.setPassword(password.getText());
        return newUser;
    }

    /**
     * Met à jour le personnel donné en argument avec les valeurs des TextFields
     * @param newPerson le personnel à mettre à jour
     * @return le personnel mis à jour
     */
    protected PersonnelEntity setPerson(PersonnelEntity newPerson) {
        newPerson.setFirstName(firstName.getText());
        newPerson.setLastName(lastName.getText());
        newPerson.setEmailAddress(email.getText());
        newPerson.setPhoneNumber(phone.getText());

        if(comboType.getSelectionModel().getSelectedItem() != null) {
            newPerson.setPersonnelTypeId(comboType.getSelectionModel().getSelectedItem().getId().toString());
        } else {
            newPerson.setPersonnelTypeId(typePersonnels.get(0).getId().toString());
        }

        return newPerson;
    }


    /**
     * Vérifie que chaque champs du personnel est valide, et renvoie {@code true} le cas échéant
     * @return {@code true} si tous les champs sont valides
     */
    protected boolean checkUserInput() {
        boolean isWellFormed = true;
        if (!firstName.getText().matches(CommonRegex.ACCENTS_AND_LETTERS_REGEX)
                || !lastName.getText().matches(CommonRegex.ACCENTS_AND_LETTERS_REGEX)) {
            SupportApp.get().getMsgFactory().warner("Il faut remplir les champs (nom, prénom) avec \nles lettres autorisées : accents, lettres et '-'").show();
            isWellFormed = false;
        } else if (!phone.getText().matches(CommonRegex.PHONE_REGEX)) {
            SupportApp.get().getMsgFactory().warner("Le numéro de téléphone n'est pas valide").show();
            isWellFormed = false;
        } else if (!email.getText().matches(CommonRegex.EMAIL_REGEX)){
            SupportApp.get().getMsgFactory().warner("L'adresse courriel n'est pas valide").show();
            isWellFormed=false;
        }

        return isWellFormed && checkLogin();
    }


    /**
     * Vérifie que le login est correctement formé
     * @return {@code true} si tous les champs sont valides
     */
    private boolean checkLogin() {
        boolean isCorrect = true;

        if(isUserCheckBox.isSelected()) {
            if (!login.getText().matches(CommonRegex.LETTERS_AND_NUMBERS_REGEX)
                    || !password.getText().matches(CommonRegex.LETTERS_AND_NUMBERS_REGEX)) {
                SupportApp.get().getMsgFactory().warner("Le nom d'utilisateur et le mot de passe doivent être \ncomposés de lettres autorisées : lettres, chiffres et '_'").show();
                isCorrect = false;
            }
        }
        return isCorrect;
    }

    /**
     * Vérifie que le login n'est pas déjà utilisé
     * @return {@code false} si le login n'est pas déjà utilisé
     */
    protected boolean userExistsAlready() {
        boolean existsAlready = false;

        List<UserEntity> existingUsers = userEntities.filtered(value-> value.getPseudo().equals(login.getText()));

        if (!existingUsers.isEmpty()) {
            existsAlready = true;
        }

        return existsAlready;
    }

    /**
     * Met à jour le tableView pour afficher les changements
     */
    protected void refreshView() {
        usersTableView.getColumns().get(0).setVisible(false);
        usersTableView.getColumns().get(0).setVisible(true);
    }

    @Override
    public String getFXMLPath() {
        return "AddUser.fxml";
    }

    @Override
    public ValidatorHelper[] getValidatorHelpers() {
        return new ValidatorHelper[]{
                ValidatorHelper.ACCENT_AND_LETTERS,
                ValidatorHelper.NOT_EMPTY,
                ValidatorHelper.EMAIL,
                ValidatorHelper.TELEPHONE
        };
    }
}
