package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.db.HibernateDAO;
import ca.usherbrooke.domus.conq.db.model.ContactEntity;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.extern.java.Log;

/**
 * Représente le tableView des contacts
 * <br />
 * date 11/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class ContactListView extends PersonListView<ContactEntity> {

    @Override
    protected void onInitialize() {
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));

    }

    @Override
    protected HibernateDAO<ContactEntity> getHibernateDAO() {
        return ContactEntity.DAO;
    }


}
