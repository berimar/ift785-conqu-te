package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.commons.app.AppController;
import ca.usherbrooke.domus.conq.commons.fx.CorePresentation;
import ca.usherbrooke.domus.conq.commons.fx.util.FXSizer;
import ca.usherbrooke.domus.conq.commons.fx.SlidingPresentation;
import ca.usherbrooke.domus.conq.commons.fx.util.OptionalObservable;
import ca.usherbrooke.domus.conq.db.model.ContactsResidentsEntity;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import ca.usherbrooke.domus.conq.uapps.dossresid.DossierResidents;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.SplitPane;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.java.Log;


/**
 * Controller principal de l'application Dossier Résident
 * <br />
 * @author Thierry
 *         Created by krys on 2015-03-21.
 *         Modified by Claire
 */
@Log
@ToString
public class DossierResidentsController extends AppController<DossierResidents> {

    public static final double EXISTING_ELEM_SLIDE_DURATION = 500.0;

    @FXML
    private Button addNewResident;

    @Getter
    @FXML
    private CorePresentation residentPresentation;

    @FXML
    private SplitPane splitPane;

    @Getter
    @FXML
    private ResidentListView residentListView;

    @Getter
    @FXML
    private SlidingPresentation slidingPresentation;


    @Getter
    private final NewResidentBody newResidentBody = new NewResidentBody();
    @Getter
    private final SeeResidentBody seeResidentBody = new SeeResidentBody();

    @Getter
    private final ContactsBox contactsBox = new ContactsBox();

    @Getter
    private final OptionalObservable<ResidentEntity> selectedResident = new OptionalObservable();
    @Getter
    private final OptionalObservable<ContactsResidentsEntity> selectedContactAssociation = new OptionalObservable();
    @Getter
    private final ObservableList<ResidentEntity> residentData = FXCollections.observableArrayList();


    @FXML
    private void initialize() {
        Platform.runLater(() -> {
            //gestion du redimentionnement dynamique
            splitPane.minHeightProperty().bind(FXSizer.UAPP_HEIGHT);
            splitPane.maxHeightProperty().bind(FXSizer.UAPP_HEIGHT);
            residentPresentation.maxHeightProperty().bind(splitPane.heightProperty());
            residentListView.maxWidthProperty().bind(FXSizer.UAPP_WIDTH.multiply(0.24d));
            residentListView.minWidthProperty().bind(FXSizer.UAPP_WIDTH.multiply(0.25d));
            // Initialisation de la table de résidents
            residentData.addAll(ResidentEntity.DAO.getAll());
            residentListView.setData(residentData);
            residentListView.setOnSelectionCallback(this::showResidentDetails);
            selectedResident.bindBidirectional(residentListView.getSelectedPerson());
            contactsBox.bind(this);
            contactsBox.prefWidthProperty().bind(residentPresentation.prefWidthProperty());
            residentPresentation.visibleProperty().bind(Bindings.isNotNull(selectedResident)
                    .or(newResidentBody.getIsShowingReadOnlyProperty()));
            newResidentBody.bind(this);
            residentListView.disableProperty().bind(seeResidentBody
                    .getEditResidentBody().getIsShowingReadOnlyProperty()
                    .or(newResidentBody.getIsShowingReadOnlyProperty())
                    .or(seeResidentBody.getContactsBox().isEditingProperty()));
            addNewResident.disableProperty().bind(seeResidentBody
                    .getEditResidentBody().getIsShowingReadOnlyProperty()
                    .or(newResidentBody.getIsShowingReadOnlyProperty())
                    .or(seeResidentBody.getContactsBox().isEditingProperty()));
            seeResidentBody.bind(this);
        });
    }


    /**
     * Méthode appelée quand on clique sur un résident dans le panneau le plus à gauche.
     * Affiche les détails du résident et met en place sa liste de contacts
     *
     * @param resident le résident à afficher
     */
    private void showResidentDetails(ResidentEntity resident) {
        if (resident != null) {
            // Remplir les labels avec les infos des résidents
            residentPresentation.show(seeResidentBody);
        }
    }

    /**
     * Méthode appelée lorsqu'on clique sur le bouton "Nouveau"
     */
    @FXML
    public void newResident() {
        log.info("Nouveau résident");
        residentPresentation.show(newResidentBody);
    }
}
