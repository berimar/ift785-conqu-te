package ca.usherbrooke.domus.conq.uapps.jfxtagenda.viewctrl;

import ca.usherbrooke.domus.conq.commons.fx.util.ButtonTemplate;
import ca.usherbrooke.domus.conq.commons.fx.DateTimePickerPopable;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.db.model.PersonnelEntity;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import ca.usherbrooke.domus.conq.supp.msg.FeedbackMsgHelper;
import ca.usherbrooke.domus.conq.uapps.jfxtagenda.JFXActivity;
import ca.usherbrooke.domus.conq.uapps.jfxtagenda.JFXTrasAgenda;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;
import jfxtras.scene.control.agenda.Agenda;
import lombok.extern.java.Log;
import org.controlsfx.control.CheckListView;

/**
 * Classe qui rassemble le comportement des opérations d'écriture sur les activités (création et modification)
 * <br />
 * date 07/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public abstract class WriteActivityBody extends ActivityBody {
    @FXML
    protected AnchorPane anchor;

    @FXML
    protected TextField titre;

    @FXML
    protected TextField lieu;
    @FXML
    protected TextArea description;
    @FXML
    protected DateTimePickerPopable dateTimeDebut;
    @FXML
    protected DateTimePickerPopable dateTimeFin;

    @FXML
    protected CheckListView<ResidentEntity> participants;
    @FXML
    protected CheckListView<PersonnelEntity> accompagnateurs;

    @FXML
    protected ComboBox<Agenda.AppointmentGroupImpl> comboChooseGroups;
    @FXML
    protected ObservableList<Agenda.AppointmentGroupImpl> observableListColorGroups;

    private ObservableList<ResidentEntity> residentEntities = FXCollections.observableArrayList();
    private ObservableList<PersonnelEntity> personnelEntities = FXCollections.observableArrayList();


    public WriteActivityBody(JFXActivity activity) {
        super(activity);
        init();
        bindToFXML();
    }

    /**
     * Méthode qui initialise les listes de résidents et de personnel en appelant la BD
     */
    private void init() {
        residentEntities.addAll(ResidentEntity.DAO.getAll());
        personnelEntities.addAll(PersonnelEntity.DAO.getAll());
        observableListColorGroups = FXCollections.observableArrayList();
        observableListColorGroups.addAll(JFXTrasAgenda.getListColorGroups());
        buttonsTemplates.add(getCancelTemplate()
                .onAction(this::onCancel)
                .build());
        buttonsTemplates.add(GlyphHelper.SAVE_HELPER.newButtonTemplteBuildr()
                .label("Valider")
                .styleClass("saveButton")
                .onAction(this::onValidate)
                .build());
    }


    @FXML
    private void initialize() {
        Platform.runLater(() -> {
            participants.setItems(residentEntities);
            accompagnateurs.setItems(personnelEntities);
            dateTimeFin.setLocalDateTime(activity.getEndLocalDateTime());
            dateTimeDebut.setLocalDateTime(activity.getStartLocalDateTime());
            titre.setText(activity.getSummary());
            description.setText(activity.getDescription());
            lieu.setText(activity.getLocation());
            comboChooseGroups.setItems(observableListColorGroups);
            comboChooseGroups.setVisibleRowCount(4);
            comboChooseGroups.setCellFactory((comboBox) -> new ListCell<Agenda.AppointmentGroupImpl>() {
                @Override
                protected void updateItem(Agenda.AppointmentGroupImpl item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(item.getDescription());
                    }
                }
            });
            comboChooseGroups.setConverter(new StringConverter<Agenda.AppointmentGroupImpl>() {
                @Override
                public String toString(Agenda.AppointmentGroupImpl group) {
                    if (group == null) {
                        return null;
                    } else {
                        return group.getDescription();
                    }
                }

                @Override
                public Agenda.AppointmentGroupImpl fromString(String groupString) {
                    return null;
                }
            });

            // TODO : trouver un moyen plus joli de checker les cases dans edit ?
            // J'ai essayé de faire ça mieux, mais check(object) ne marche pas, seulement check(int) fonctionne
            // Et vu que activity.getPersonnels().indexOf(person) ne retourne jamais vrai, que -1 (donc les objets sont copiés quelque part) j'ai pas trouvé mieux.
            if ((activity.getPersonnels() != null) && (activity.getPersonnels().size() > 0)) {
                activity.getPersonnels().forEach(person -> {
                    int i = 0;
                    for (PersonnelEntity p : personnelEntities) {
                        if (p.toString().equals(person.toString())) {
                            break;
                        }
                        i++;
                    }
                    accompagnateurs.getCheckModel().check(i);
                });
            }
            if ((activity.getResidents() != null) && (activity.getResidents().size() > 0)) {
                activity.getResidents().forEach(person -> {
                    int i = 0;
                    for (ResidentEntity r : residentEntities) {
                        if (r.toString().equals(person.toString())) {
                            break;
                        }
                        i++;
                    }
                    participants.getCheckModel().check(i);
                });
            }
        });
    }

    /**
     * Méthode appelée quand on clique sur le bouton "Valider" d'un évènement en édition
     */
    protected void onValidate() {
        activity.setSummary(titre.getText());
        activity.setDescription(description.getText());
        activity.setLocation(lieu.getText());
        activity.setResident(participants.getCheckModel().getCheckedItems());
        activity.setPersonnel(accompagnateurs.getCheckModel().getCheckedItems());
        activity.setEndLocalDateTime(dateTimeFin.getLocalDateTime());
        activity.setStartLocalDateTime(dateTimeDebut.getLocalDateTime());

        if (comboChooseGroups.getValue() != null) {
            activity.setAppointmentGroup(comboChooseGroups.getValue());
        } else {
            activity.setAppointmentGroup(observableListColorGroups.get(0));
        }
        runPersistingStrategy();
        getBoundApp().getAppGC().refresh();
        getSucces().show();
        hide();
    }

    protected void onCancel() {
        hide();
    }

    @Override
    public String getFXMLPath() {
        return "WriteActivityBody.fxml";
    }

    protected abstract void runPersistingStrategy();

    protected abstract FeedbackMsgHelper getSucces();

    protected abstract ButtonTemplate.Builder getCancelTemplate();
}
