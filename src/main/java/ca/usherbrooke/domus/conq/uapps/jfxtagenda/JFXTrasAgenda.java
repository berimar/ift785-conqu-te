package ca.usherbrooke.domus.conq.uapps.jfxtagenda;

import ca.usherbrooke.domus.conq.db.model.ActivityEntity;
import ca.usherbrooke.domus.conq.supp.SupportAppGC;
import ca.usherbrooke.domus.conq.supp.msg.FeedbackMsgHelper;
import ca.usherbrooke.domus.conq.uapps.UApp;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import jfxtras.scene.control.agenda.Agenda;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.java.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Application Agenda
 * <br />
 *
 * Created by Jonathan on 16/03/2015.
 */

@Log
@ToString
public class JFXTrasAgenda extends UApp<JFXTrasAgendaGC> {

    /**
     * Liste de groupes d'évènements, à enlever lorsque ces derniers seront intégrés à la DB
     */
    @Getter
    @Setter
    public static List<Agenda.AppointmentGroupImpl> listColorGroups = new ArrayList<>();

    @Getter
    private final String displayedName = "Agenda";

    @Getter(lazy = true)
    private final Agenda agenda = (Agenda) getAppGC().getRootPane().lookup("#agenda");

    @Getter(lazy = true)
    private final ObservableList<Agenda.Appointment> activities = getAgenda().appointments();

    private final ListChangeListener<Agenda.Appointment> listener = (ListChangeListener<Agenda.Appointment>) jfxActivityChange -> {
        while (jfxActivityChange.next()) {
            jfxActivityChange.getAddedSubList().forEach((jfxActivity) -> ((JFXActivity) jfxActivity).bindAndPersist());
            jfxActivityChange.getRemoved().forEach((jfxActivity) -> {
                ((JFXActivity) jfxActivity).delete();
                FeedbackMsgHelper message = getMsgFactory().canceller("L'activité a bien été supprimée.",
                        () -> {
                            ((JFXActivity) jfxActivity).rebind();
                            getActivities().add(jfxActivity);
                            getMsgFactory().informer("L'activité a bien été restituée").show();
                        });
                message.show();
            });
        }
    };

    public JFXTrasAgenda(SupportAppGC supportAppGC) {
        super(supportAppGC, JFXTrasAgendaGC.class);
    }

    @Override
    public void onStop() {
        getActivities().removeListener(listener);
        getActivities().clear();
    }

    @Override
    public void onStart() {
        getAgenda().createDefaultSkin();
        getAgenda().setAllowDragging(false);
        getAgenda().setAllowResize(false);

        this.getActivities().addListener(listener);
        getActivities().addAll(ActivityEntity.DAO.getAll().stream().map(ActivityEntity::getBoundJFXActivity).collect(Collectors.toList()));

        // TODO : intégrer la liste des groupes dans la BD, ajouter la possibilité de créer d'autres groupes, d'en supprimer, ...
        getListColorGroups().add(new Agenda.AppointmentGroupImpl().withDescription("Présentations").withStyleClass("group0"));
        getListColorGroups().add(new Agenda.AppointmentGroupImpl().withDescription("Personnel").withStyleClass("group1"));
        getListColorGroups().add(new Agenda.AppointmentGroupImpl().withDescription("Expérimentations").withStyleClass("group2"));

    }

}
