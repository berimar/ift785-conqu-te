package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;


import ca.usherbrooke.domus.conq.commons.fx.model.ComboBoxWrapper;
import ca.usherbrooke.domus.conq.commons.fx.util.FXUtils;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.ValidatorHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.OptionalObservable;
import ca.usherbrooke.domus.conq.commons.fx.model.WriteConnectedPersonBody;
import ca.usherbrooke.domus.conq.db.model.ContactEntity;
import ca.usherbrooke.domus.conq.db.model.ContactsResidentsEntity;
import ca.usherbrooke.domus.conq.db.model.RelationTypeEntity;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import lombok.NonNull;
import lombok.extern.java.Log;

/**
 * Classe qui s'occupe du comportement lors de comportements communs à l'édition et la création d'un contact
 * <br />
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public abstract class WriteContactBody extends WriteConnectedPersonBody {


    @FXML
    protected ComboBox<RelationTypeEntity> relationField;

    protected ContactListView contactView;
    private SeeContactBody seeContactBody;

    protected final OptionalObservable<ContactsResidentsEntity> selectedAssociation = new OptionalObservable<>();
    protected final OptionalObservable<ResidentEntity> selectedResident = new OptionalObservable<>();

    public WriteContactBody() {
        FXUtils.bindToFXML(this, getFXMLPath());
        buttonsTemplates.add(GlyphHelper.CANCEL_HELPER.newButtonTemplteBuildr()
                .label("Annuler l'édition du contact")
                .onAction(() -> {
                    if(!contactView.getPersons().isEmpty() && (selectedAssociation.getValue() != null)) {
                        log.warning("PRESENT : " + selectedAssociation.getValue());
                        showNext(seeContactBody);
                    } else {
                        hide();
                    }
                })
                .tooltip("Cacher le menu")
                .build());
    }

    @FXML
    protected final void initialize() {
        Platform.runLater(() -> {
            // Initialisation du type de relation pour les contacts
            ComboBoxWrapper.wrapRelationTypeCB(relationField);
            clearConnectedPersonFields();
        });
    }


    @Override
    public String getFXMLPath() {
        return "WriteContactBody.fxml";
    }


    /**
     * Met à jour le contact en updatant ses champs avec ce qui est dans les TextFields
     * @return le contact à jour
     */
    protected ContactsResidentsEntity commitSelectedAssociation(ContactsResidentsEntity selectedAssociation) {
        ContactEntity contactSelected = selectedAssociation.getContactEntity();
        commitConnectedPersonFields(contactSelected);
        selectedAssociation.setRelationType(relationField.getValue());
        return selectedAssociation;
    }


    /**
     * Vérifie les réponses du contact et sauve ce dernier dans la bd temporaire si pas de problèmes avec les réponses
     * @return true si le contact a bien été sauvé, false sinon
     */
    public final void saveContactAssociation() {
        if (checkAssociationInput()) {
            onContactAssociationSaving();
            showNext(seeContactBody);
        }
    }

    protected abstract void onContactAssociationSaving();

    public void bind(@NonNull ContactsBox contactsBox) {
        this.contactView = contactsBox.getResidentContactsView();
        this.selectedAssociation.bindBidirectional(contactsBox.getSelectedAssociation());
        this.selectedResident.bind(contactsBox.getSelectedResident());
        this.seeContactBody = contactsBox.getSeeContactMenu();
    }


    /**
     * Vérifie que les champs ont été remplis correctement
     * @return true si le formulaire a été rempli correctement, false sinon (et envoie un message si pas correct)
     */
    private boolean checkAssociationInput() {
        boolean isWellFormed = true;
        if (relationField.getSelectionModel().isEmpty()) {
            SupportApp.get().getMsgFactory().warner("Vous devez sélectionner un type de relation").show();
            isWellFormed = false;
        }
        return isWellFormed && checkConnectedPersonInput();
    }


    @Override
    public GlyphHelper[] getGlyphHelpers() {
        return new GlyphHelper[]{
                GlyphHelper.PICTURE_HELPER
        };
    }

    @Override
    public ValidatorHelper[] getValidatorHelpers() {
        return new ValidatorHelper[]{
                ValidatorHelper.ACCENT_AND_LETTERS,
                ValidatorHelper.EMAIL,
                ValidatorHelper.NUMBER,
                ValidatorHelper.NOT_EMPTY,
                ValidatorHelper.TELEPHONE
        };
    }


}
