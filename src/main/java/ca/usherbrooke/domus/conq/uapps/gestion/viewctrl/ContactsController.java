package ca.usherbrooke.domus.conq.uapps.gestion.viewctrl;

import ca.usherbrooke.domus.conq.commons.CommonRegex;
import ca.usherbrooke.domus.conq.db.DataProvider;
import ca.usherbrooke.domus.conq.db.WrappedListDAO;
import ca.usherbrooke.domus.conq.db.model.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import lombok.extern.java.Log;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Décrit le comportement de la partie des relations dans l'app Gestion. Permet de visualiser, d'ajouter, de
 * supprimer des relations.
 *
 * date 08/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class ContactsController extends GestionTemplate {

    @FXML
    private SplitPane splitPane;
    @FXML
    private TableView<RelationTypeEntity> relations;
    @FXML
    private TableColumn<RelationTypeEntity, String> relationType;

    @FXML
    private HBox slidingNewRelationPane;
    @FXML
    private Button deleteButton;
    @FXML
    private TextField newRelationName;

    private final WrappedListDAO<RelationTypeEntity> relationTypes = DataProvider.get().getRelationTypes();


    @FXML
    public void initialize() {
        relationType.setCellValueFactory(new PropertyValueFactory<>("type"));
        relations.setItems(relationTypes.getUnmodifiableList());

        // Listener pour écouter le changement de sélection dans la liste d'apparts
        relations.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    hidePane(slidingNewRelationPane);

                    DetachedCriteria detachedCriteria = ContactsResidentsEntity.DAO.buildCriteria();
                    detachedCriteria.add(Restrictions.eq("relationType", newValue));

                    List nonNullRelations = ContactsResidentsEntity.DAO.getListByCriteria(detachedCriteria);
                    if (nonNullRelations.size() != 0) {
                        deleteButton.setDisable(true);
                    } else {
                        deleteButton.setDisable(false);
                    }
                });

        // Move this into GestionGC ?
        relations.setTooltip(new Tooltip("Pour éditer, double cliquer sur la valeur à changer puis cliquer autre part"));

        Callback<TableColumn<RelationTypeEntity, String>, TableCell<RelationTypeEntity, String>> cellFactory = (p -> new EditingCell());
        relationType.setCellFactory(cellFactory);
        relationType.setOnEditCommit(cell -> {
                    log.info("Modification de relation");

                    RelationTypeEntity relationUpdate = cell.getTableView().getItems().get(cell.getTablePosition().getRow());
                    if(cell.getNewValue().matches(CommonRegex.ACCENTS_AND_LETTERS_REGEX)) {
                        relationUpdate.setType(cell.getNewValue());
                        relationTypes.update(relationUpdate);
                    } else {
                        relationUpdate.setType(cell.getOldValue());
                        refreshView();
                        getBoundApp().getMsgFactory().informer(getErrorMessageForGestion());
                        // FIXME : notification not working
                    }
                }
        );
    }

    /**
     * Met à jour le tableView pour afficher les changements
     */
    protected void refreshView() {
        relations.getColumns().get(0).setVisible(false);
        relations.getColumns().get(0).setVisible(true);
    }

    @Override
    protected String getErrorMessageForGestion() {
        return "La relation doit être composée de lettres, espaces ou -.";
    }

    /**
     * Affiche la partie d'ajout de relation (sliding panel)
     *
     * @param actionEvent l'évènement déclenché par le clic
     */
    @FXML public void add(ActionEvent actionEvent) {
        showPane(slidingNewRelationPane);
    }



    /**
     * Méthode appelée quand on clique sur le bouton "Supprimer" pour supprimer une relation
     * @param actionEvent l'évènement du clic
     */
    @FXML public void delete(ActionEvent actionEvent) {
        log.info("Delete Relation");
        RelationTypeEntity selectedContact = (RelationTypeEntity) getListSelectedItem(relations);
        deleteElement(selectedContact, relations);
    }

    /*
     * {@inheritDoc}
     */
    @Override
    protected void deleteInDB(GestionTypeEntity selectedContact) {
        relationTypes.delete((RelationTypeEntity) selectedContact);
    }


    /**
     * Méthode appelée lorsqu'on clique sur le bouton "OK" pour ajouter une relation.
     * <br>
     * Vérifie l'input dans le textfield et l'enregistre en base si correct, sinon affiche un message d'erreur
     * pour que l'utilisateur corrige son input.
     */
    @FXML
    public void saveRelation() {
        // TODO : visual validation

        //TODO améliorer la regex pour qu'elle accepte les parenthèses.
        saveGestionElement(newRelationName, slidingNewRelationPane, CommonRegex.ACCENTS_AND_LETTERS_REGEX);
    }


    /*
     * {@inheritDoc}
     */
    @Override
    protected ObservableList getExistingGestionList(GestionTypeEntity newRelation) {
        return relations.getItems().filtered(item -> (item.getType().equals(((RelationTypeEntity) newRelation).getType())));
    }

    /*
     * {@inheritDoc}
     */
    @Override
    protected RelationTypeEntity createTypeEntity(TextField textFieldToSave) {
        RelationTypeEntity newRelation = relationTypes.newInstance();
        newRelation.setType(textFieldToSave.getText());
        return newRelation;
    }

    /*
     * {@inheritDoc}
     */
    @Override
    protected void dbSaveAndUpdateView(GestionTypeEntity newRelation) {
        relationTypes.persist((RelationTypeEntity) newRelation);
    }

    /*
     * {@inheritDoc}
     */
    @Override
    protected String getNameOfGestion() {
        return "La relation";
    }
}
