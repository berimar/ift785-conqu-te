package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.commons.DateUtil;
import ca.usherbrooke.domus.conq.commons.SecureLocalDateStringConverter;
import ca.usherbrooke.domus.conq.commons.fx.model.ComboBoxWrapper;
import ca.usherbrooke.domus.conq.commons.fx.util.FXUtils;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.ValidatorHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.OptionalObservable;
import ca.usherbrooke.domus.conq.commons.fx.model.WritePersonBody;
import ca.usherbrooke.domus.conq.db.model.ApartmentEntity;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import lombok.NonNull;
import lombok.extern.java.Log;

import java.time.LocalDate;

/**
 * Classe qui s'occupe du comportement lors de comportements communs à l'édition et la création d'un résident
 * <br />
 *
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public abstract class WriteResidentBody extends WritePersonBody {

    private final SecureLocalDateStringConverter dateStringConverter = DateUtil.newLocalDateStringConverter();

    @FXML
    private ComboBox<ApartmentEntity> apartNumberCombo;
    @FXML
    private DatePicker birthDateField;

    protected ResidentListView residentListView;
    private SeeResidentBody seeResidentBody;

    protected final OptionalObservable<ResidentEntity> selectedResident = new OptionalObservable<>();
    protected ObservableList<ResidentEntity> residents;

    public WriteResidentBody() {
        bindToFXML();
        buttonsTemplates.add(GlyphHelper.CANCEL_HELPER.newButtonTemplteBuildr()
                .label(getCancelLabel())
                .tooltip("Annuler")
                .onAction(() -> {
                    clearResidentFields();
                    showNext(seeResidentBody);
                })
                .build());
        buttonsTemplates.add(GlyphHelper.SAVE_HELPER.newButtonTemplteBuildr()
                .label(getSaveLabel())
                .tooltip("Valider")
                .onAction(() -> validateResidentInput(selectedResident.get()))
                .build());
    }


    @FXML
    protected final void initialize() {
        Platform.runLater(() -> {
            onInitialize();
            coherantUXHelper.addValidationSupport(
                    ValidatorHelper.ACCENT_AND_LETTERS,
                    ValidatorHelper.NOT_EMPTY,
                    ValidatorHelper.TELEPHONE);
            coherantUXHelper.addGlyphToButtonClasses(GlyphHelper.PICTURE_HELPER);
            clearResidentFields();
            birthDateField.setConverter(dateStringConverter);
            addValidatorToDatePicker();
            ComboBoxWrapper.wrapApartmentCB(apartNumberCombo);
        });

    }

    @Override
    public final String getFXMLPath() {
        return "WriteResidentBody.fxml";
    }


    public void bind(@NonNull DossierResidentsController controller) {
        this.residents = controller.getResidentData();
        this.residentListView = controller.getResidentListView();
        this.selectedResident.bindBidirectional(controller.getSelectedResident());
        this.seeResidentBody = controller.getSeeResidentBody();
    }


    /**
     * Remplis les textFields et autres comboBoxes avec les informations du résident donné en argument
     * @param resident le résident avec lequel afficher les informations
     */
    protected void uploadResidentFields(ResidentEntity resident) {
        uploadPersonFields(resident);
        apartNumberCombo.getSelectionModel().select(resident.getApartmentEntity());
        apartNumberCombo.setValue(resident.getApartmentEntity());
        if(resident.getBirthday()!=null&&!resident.getBirthday().isEmpty()) {
            uploadDateField(resident);
        }
        birthDateField.setPromptText("dd/mm/yyyy");
    }

    /**
     * Efface les textes dans les textFields et autres comboBoxes du résident
     */
    protected void clearResidentFields() {
        clearPersonFields();
        apartNumberCombo.getSelectionModel().clearSelection();
        birthDateField.setValue(null);
    }

    /**
     * Vérifie que les champs ont été remplis correctement
     * @return true si le formulaire a été rempli correctement, false sinon (et envoie un message si pas correct)
     */
    private void validateResidentInput(ResidentEntity residentEntity) {
        boolean isWellFormed = true;
        if (apartNumberCombo.getSelectionModel().getSelectedItem()==null) {
            isWellFormed = false;
            SupportApp.get().getMsgFactory().warner("Le numéro d'appartement est manquant").show();
        }
        if(dateStringConverter.hasParseError()){
            isWellFormed = false;
            SupportApp.get().getMsgFactory().warner("La date "+dateStringConverter.getParsedString()+" n'est pas valide").show();
            uploadDateField(residentEntity);
        }
        if (isWellFormed && checkPersonInput()) {
            onInputValidation();
            SupportApp.get().getMsgFactory().confirmer("Sauvegarde réussie").show();
            showNext(seeResidentBody);
        }
    }

    protected abstract void onInputValidation();


    /**
     * Met à jour le résident en updatant ses champs avec ce qui est dans les TextFields
     * @return le résident à jour
     */
    protected void commitResidentFields(ResidentEntity residentSelected) {
        commitPersonFields(residentSelected);
        residentSelected.setApartmentEntity(apartNumberCombo.getSelectionModel().getSelectedItem());
        // TODO : add try/catch depending on DB ? since we have a pre-check...

        if (!(birthDateField.getValue() == null)) {
            residentSelected.setBirthday(dateStringConverter.toString(birthDateField.getValue()));
        } else {
            residentSelected.setBirthday("");
        }

        residentSelected.setPhoneNumber(phoneField.getText());

        selectedResident.set(residentSelected);
    }

    protected abstract String getSaveLabel();

    protected abstract String getCancelLabel();

    protected abstract void onInitialize();

    /**
     * Met à jour le textField avec la date d'anniversaire existante du résident
     * @param residentEntity
     */
    private void uploadDateField(ResidentEntity residentEntity){
        LocalDate date = dateStringConverter.fromString(residentEntity.getBirthday());
        birthDateField.setValue(date);
    }

    /**
     * Validation visuelle pour le datePicker
     */
    private void addValidatorToDatePicker(){
        birthDateField.skinProperty().addListener((observable, oldValue, newValue) -> {
            FXUtils.deepPseudoClassLookup(newValue.getNode(),".text-input").stream().findFirst().ifPresent((node) -> {
                node.getStyleClass().add(".validateSimpleDate");
                coherantUXHelper.addValidationSupport(ValidatorHelper.SIMPLE_DATE, node);
            });
        });
    }

}
