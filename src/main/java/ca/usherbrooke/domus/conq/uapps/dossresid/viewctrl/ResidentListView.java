package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.db.HibernateDAO;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.extern.java.Log;

/**
 * Représente le tableView affichant la liste des résidents
 * <br />
 * date 11/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class ResidentListView extends PersonListView<ResidentEntity> {

    @Override
    protected void onInitialize() {
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
    }


    @Override
    protected HibernateDAO<ResidentEntity> getHibernateDAO() {
        return ResidentEntity.DAO;
    }


}
