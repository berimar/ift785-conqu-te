package ca.usherbrooke.domus.conq.uapps.jfxtagenda.viewctrl;

import ca.usherbrooke.domus.conq.commons.fx.util.ButtonTemplate;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.supp.msg.FeedbackMsgHelper;
import ca.usherbrooke.domus.conq.uapps.jfxtagenda.JFXActivity;
import lombok.ToString;
import lombok.extern.java.Log;


/**
 * Classe qui s'occupe du comportement de la création d'activité
 * <br />
 * Created by Anthony on 27/03/2015.
 */

@Log
@ToString
public class CreateActivityMenu extends WriteActivityBody {

    private final FeedbackMsgHelper success = getBoundApp().getMsgFactory().confirmer("L'activité a bien été créée.");

    @Override
    protected void runPersistingStrategy() {
    }

    public CreateActivityMenu(JFXActivity activity1) {
        super(activity1);
    }

    @Override
    public void onCancel() {
        getBoundApp().getActivities().remove(activity);
        super.onCancel();
    }

    @Override
    protected FeedbackMsgHelper getSucces() {
        return success;
    }

    @Override
    protected ButtonTemplate.Builder getCancelTemplate() {
        return GlyphHelper.DELETE_HELPER.newButtonTemplteBuildr()
                .label("supprimer");
    }

}
