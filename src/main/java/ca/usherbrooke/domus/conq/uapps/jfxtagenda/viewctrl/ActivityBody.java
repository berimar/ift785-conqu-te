package ca.usherbrooke.domus.conq.uapps.jfxtagenda.viewctrl;

import ca.usherbrooke.domus.conq.commons.fx.internal.PresentationBody;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import ca.usherbrooke.domus.conq.uapps.jfxtagenda.JFXActivity;
import ca.usherbrooke.domus.conq.uapps.jfxtagenda.JFXTrasAgenda;
import lombok.extern.java.Log;

/**
 * Created by Jonathan on 04/04/2015.
 */
@Log
public abstract class ActivityBody extends PresentationBody {

    protected JFXActivity activity;

    public ActivityBody(JFXActivity activity1) {
        this.activity = activity1;
    }

    protected JFXTrasAgenda getBoundApp() {
        return (JFXTrasAgenda) SupportApp.get().getUApps().get(JFXTrasAgenda.class.getSimpleName());
    }
}
