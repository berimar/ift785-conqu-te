package ca.usherbrooke.domus.conq.uapps.dossresid;

import ca.usherbrooke.domus.conq.supp.SupportAppGC;
import ca.usherbrooke.domus.conq.uapps.UApp;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.java.Log;

/**
 * @author krys
 *         Created by krys on 2015-03-21.
 *         Modified by Claire
 */
@Log
@ToString
public class DossierResidents extends UApp<DossierResidentsGC> {

    @Getter
    private final String displayedName = "Résidents";

    @SuppressWarnings({"JavaDoc"})
    public DossierResidents(SupportAppGC supportAppGC) {
        super(supportAppGC, DossierResidentsGC.class);
    }

}


