package ca.usherbrooke.domus.conq.uapps.gestion;

import ca.usherbrooke.domus.conq.supp.SupportAppGC;
import ca.usherbrooke.domus.conq.uapps.UApp;
import lombok.Getter;

/**
 * date 07/04/15 <br/>
 * L'application permettant la gestion de la Résidence.
 *
 * @author sv3inburn3
 */
public class Gestion extends UApp<GestionGC> {

    @Getter
    private final String displayedName = "Gestion";

    @SuppressWarnings("javadoc")
    public Gestion(SupportAppGC supportAppGC) {
        super(supportAppGC, GestionGC.class);
    }

}
