package ca.usherbrooke.domus.conq.uapps.jfxtagenda.viewctrl;

import ca.usherbrooke.domus.conq.commons.TimeUtils;
import ca.usherbrooke.domus.conq.commons.app.AppController;
import ca.usherbrooke.domus.conq.commons.fx.SlidingPresentation;
import ca.usherbrooke.domus.conq.uapps.jfxtagenda.JFXActivity;
import ca.usherbrooke.domus.conq.uapps.jfxtagenda.JFXTrasAgenda;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import jfxtras.scene.control.agenda.Agenda;
import lombok.ToString;
import lombok.extern.java.Log;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Jonathan on 23/03/2015.
 */
@Log
@ToString
public class JFXTrasAgendaController extends AppController<JFXTrasAgenda> {

    private static final int DETAILS_PANE_HEIGHT = 200;

    //Utilisé pour ajouter des écoutes sur les modifications du Calendrier
    private ObjectProperty<Calendar> boundCalendar = new SimpleObjectProperty<>();

    //HideBar

    @FXML
    private VBox hideBar;
    @FXML
    private Label yearLabel;
    @FXML
    private Label monthLabel;
    @FXML
    private Label weekLabel;
    @FXML
    private SlidingPresentation contextualLeftMenu;

    //agenda
    @FXML
    private Agenda agenda;

    @FXML
    private AnchorPane agendaAnchor;

    @FXML
    private void initialize() {
        Platform.runLater(() -> {
            boundCalendar.bindBidirectional(agenda.displayedCalendar()); //TODO utiliser l'API time de java8
            boundCalendar.addListener((observable, oldValue, newValue) -> updateDate(newValue));
            updateDate(boundCalendar.get());

            contextualLeftMenu.minHeightProperty().bind(agendaAnchor.heightProperty());
            contextualLeftMenu.maxHeightProperty().bind(agendaAnchor.heightProperty());
            getBoundApp().getActivities().addListener((ListChangeListener<Agenda.Appointment>) c -> {
                while(c.next()) {
                    if (c.getRemovedSize() > 0) {
                        contextualLeftMenu.hide();
                        break;
                    }
                }
            });
        });
        agenda.setNewAppointmentCallback(this::createActivityCallback);
        agenda.selectedAppointments().addListener(this::onSelectionChange);
        agenda.setEditAppointmentCallback((appointment) -> {
            contextualLeftMenu.show(new EditActivityBody((JFXActivity) appointment));
            return null;
        });

        agenda.setDetailsAppointmentCallback((appointment) ->
                contextualLeftMenu.show(new SeeActivityBody((JFXActivity) appointment)));
    }


    /**
     * Mise à jour de la date lorsqu'un bouton est cliqué (méthode appelée par un listener de changement)
     * @param newValue la nouvelle valeur du calendrier
     */
    private void updateDate(Calendar newValue) {
        yearLabel.setText(String.valueOf(newValue.get(Calendar.YEAR)));
        Calendar start = clone(newValue);
        Calendar end = clone(newValue);
        Agenda.CalendarRange range = new Agenda.CalendarRange(start, end);
        range.getEndCalendar().add(Calendar.DAY_OF_MONTH, 8 - newValue.get(Calendar.DAY_OF_WEEK));
        range.getStartCalendar().add(Calendar.DAY_OF_MONTH, 2 + -1 * newValue.get(Calendar.DAY_OF_WEEK));
        monthLabel.setText(String.format("%1$tb", newValue));
        weekLabel.setText(TimeUtils.getMonthAndDay(range.getStartCalendar()) + " \u2192 " + TimeUtils.getMonthAndDay(range.getEndCalendar()));
    }

    @FXML
    public void addActivity() {
        contextualLeftMenu.show(new CreateActivityMenu(JFXActivity.empty()));
    }

    /**
     * Bouton : année suivante
     * Change la vue du calendrier à l'année suivante
     */
    @FXML
    public void moveToLastYear() {
        Calendar newInst = clone(boundCalendar.get());
        newInst.add(Calendar.YEAR, -1);
        boundCalendar.setValue(newInst);
        refresh();
    }

    /**
     * Bouton : année précédente
     * Change la vue du calendrier à l'année précédente
     */
    @FXML
    public void moveToNextYear() {
        Calendar newInst = clone(boundCalendar.get());
        newInst.add(Calendar.YEAR, 1);
        boundCalendar.setValue(newInst);
        refresh();
    }

    /**
     * Bouton : mois suivant
     * Change la vue du calendrier au mois suivant
     */
    @FXML
    public void moveToLastMonth() {
        Calendar newInst = clone(boundCalendar.get());
        newInst.add(Calendar.MONTH, -1);
        boundCalendar.setValue(newInst);
        refresh();
    }

    /**
     * Bouton : mois précédent
     * Change la vue du calendrier au mois précédent
     */
    @FXML
    public void moveToNextMonth() {
        Calendar newInst = clone(boundCalendar.get());
        newInst.add(Calendar.MONTH, 1);
        boundCalendar.setValue(newInst);
        refresh();
    }

    /**
     * Bouton : semaine suivante
     * Change la vue du calendrier à la semaine suivante
     */
    @FXML
    public void moveToNextWeek() {
        Calendar newInst = clone(boundCalendar.get());
        newInst.add(Calendar.WEEK_OF_MONTH, 1);
        boundCalendar.setValue(newInst);
        refresh();
    }

    /**
     * Bouton : semaine précédente
     * Change la vue du calendrier à la semaine précédente
     */
    @FXML
    public void moveToLastWeek() {
        Calendar newInst = clone(boundCalendar.get());
        newInst.add(Calendar.WEEK_OF_MONTH, -1);
        boundCalendar.setValue(newInst);
        refresh();
    }

    /**
     * Bouton : aujourd'hui
     */
    @FXML
    public void moveToToDay() {
        boundCalendar.setValue(Calendar.getInstance(Locale.getDefault()));
        refresh();
    }

    private void refresh() {
        getBoundApp().getAppGC().refresh();
    }

    private Calendar clone(Calendar cal) {
        return (Calendar) cal.clone();
    }

    /**
     * @param localDateTimeRange l'intervale de temps associé à l'évènement
     * @return l'instance d'évènement du calendrier
     */
    public JFXActivity createActivityCallback(Agenda.LocalDateTimeRange localDateTimeRange) {
        JFXActivity activity = JFXActivity.empty();
        activity.setStartLocalDateTime(localDateTimeRange.getStartLocalDateTime());
        activity.setEndLocalDateTime(localDateTimeRange.getEndLocalDateTime());
        contextualLeftMenu.show(new CreateActivityMenu(activity));
        log.info("Activity créée : " + activity.toString());
        return activity;
    }

    /**
     * Listener pour écouter le changement de sélection
     * @param change
     */
    public void onSelectionChange(ListChangeListener.Change<? extends Agenda.Appointment> change) {
        while (change.next()) {
            log.info("Sélection change detected");
            //TODO gérer la sélection multiple
        }
    }

}
