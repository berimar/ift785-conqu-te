package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.commons.app.GraphicalContext;
import ca.usherbrooke.domus.conq.commons.app.GraphicalLCApp;
import ca.usherbrooke.domus.conq.commons.fx.model.ComboBoxWrapper;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.OptionalObservable;
import ca.usherbrooke.domus.conq.commons.fx.internal.PresentationBody;
import ca.usherbrooke.domus.conq.commons.fx.util.ValidatorHelper;
import ca.usherbrooke.domus.conq.db.model.ContactEntity;
import ca.usherbrooke.domus.conq.db.model.ContactsResidentsEntity;
import ca.usherbrooke.domus.conq.db.model.RelationTypeEntity;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import ca.usherbrooke.domus.conq.uapps.dossresid.DossierResidents;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import lombok.NonNull;
import lombok.extern.java.Log;

/**
 * Classe qui s'occupe du comportement lors de l'ajout d'un contact existant à un résident
 * <br />
 *
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class ExistingContactBody extends PresentationBody {

    @FXML
    ContactListView existingContactsView;
    @FXML
    ComboBox<RelationTypeEntity> relationTypeComboBox ;

    private ContactListView residentContactsView;
    private final OptionalObservable<ResidentEntity> selectedResident = new OptionalObservable<>();
    private final OptionalObservable<ContactsResidentsEntity> selectedAssociation = new OptionalObservable<>();
    private SeeContactBody seeContactBody;

    public ExistingContactBody() {
        bindToFXML();
        buttonsTemplates.add(GlyphHelper.CANCEL_HELPER.newButtonTemplteBuildr()
                .tooltip("Annuler")
                .onAction(() -> {
                    if (!residentContactsView.getSelectedPerson().getOptional().isPresent() || (selectedAssociation.getValue() == null)) {
                        hide();
                    } else {
                        showNext(seeContactBody);
                    }
                })
                .build());
        buttonsTemplates.add(GlyphHelper.CHOOSE_HELPER.newButtonTemplteBuildr()
                .tooltip("Choisir ce contact")
                .onAction(this::chooseExistingContact)
                .build());
        isShowingProperty.addListener((observable, oldValue, newValue) -> {
            ObservableList<ContactEntity> allContacts = FXCollections.observableArrayList(ContactEntity.DAO.getAll());
            existingContactsView.setData(allContacts.filtered(selectedCont -> !residentContactsView.getPersons().contains(selectedCont)));
            relationTypeComboBox.getSelectionModel().clearSelection();
        });
    }

    @FXML protected final void initialize(){
        Platform.runLater(()-> ComboBoxWrapper.wrapRelationTypeCB(relationTypeComboBox));
    }

    @Override
    public String getFXMLPath() {
        return "ExistingContactBody.fxml";
    }


    public void bind(@NonNull ContactsBox contactsBox) {
        this.residentContactsView = contactsBox.getResidentContactsView();
        this.selectedResident.bind(contactsBox.getSelectedResident());
        this.selectedAssociation.bindBidirectional(contactsBox.getSelectedAssociation());
        this.seeContactBody = contactsBox.getSeeContactMenu();
    }


    /**
     * Méthode appelée quand on clique sur "Choisir" dans le pannel des contacts existants
     */
    public void chooseExistingContact() {
        log.info("Choisir contact existant");
        if(relationTypeComboBox.getSelectionModel().getSelectedItem()!=null) {
            ContactEntity selectedExistingContact = existingContactsView.getSelectedPerson().get();
            if (selectedExistingContact != null) {
                ContactsResidentsEntity newAssociation = new ContactsResidentsEntity();
                newAssociation.setContactEntity(selectedExistingContact);
                newAssociation.setResidentEntity(selectedResident.get());
                newAssociation.setRelationType(relationTypeComboBox.getValue());
                ContactsResidentsEntity.DAO.save(newAssociation);
                ResidentEntity.DAO.refresh(selectedResident.get());
                selectedResident.get().getContactEntities().add(newAssociation.getContactEntity());
                selectedAssociation.set(newAssociation);
                residentContactsView.getPersons().add(selectedExistingContact);
                showNext(seeContactBody);
            } else {
                getBoundApp().getMsgFactory().warner("Aucun contact n'est sélectionné. \nVeuillez sélectionner un contact ou cliquer sur 'Annuler'").show();
            }
        } else {
            getBoundApp().getMsgFactory().warner("Vous devez sélectionner un type de relation.").show();
        }
    }


    public GraphicalLCApp<GraphicalContext<?>> getBoundApp() {
        return SupportApp.get().getUApps().get(DossierResidents.class.getSimpleName());
    }

    /**
     * Validation visuelle avec comme paramètre NOT_EMPTY (les champs sont valides si le textfield n'est pas vide)
     * @return le tableau de validatorHelper
     */
    @Override
    public ValidatorHelper[] getValidatorHelpers() {
        return new ValidatorHelper[]{
                ValidatorHelper.NOT_EMPTY
        };
    }
}
