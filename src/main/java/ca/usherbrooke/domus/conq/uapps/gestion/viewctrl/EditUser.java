package ca.usherbrooke.domus.conq.uapps.gestion.viewctrl;

import ca.usherbrooke.domus.conq.db.model.PersonnelEntity;
import ca.usherbrooke.domus.conq.db.model.PersonnelTypeEntity;
import ca.usherbrooke.domus.conq.db.model.UserEntity;
import lombok.extern.java.Log;

import java.util.List;

/**
 * S'occupe de la modification du personnel. Cette classe permet de remplir les champs d'édition avec les informations appropriées
 * et d'enregistrer en base les modifications faite au personnel.
 *
 * @author Claire
 */
@Log
public class EditUser extends WriteUserBody {

    /**
     * Enregistre la modification d'un personnel (+ l'ajout/modification/suppression d'un utilisateur)
     */
    @Override
    public void createUpdateUser() {
        log.info("Updating User...");

        if(checkUserInput()) {
            PersonnelEntity personUpdate = usersTableView.getSelectionModel().getSelectedItem();
            if(personUpdate != null) {
                personUpdate = setPerson(personUpdate);
                PersonnelEntity.DAO.update(personUpdate);

                if(isUserCheckBox.isSelected() && userExistsAlready()) { // Update user
                    UserEntity userUpdate = setUser(userEntities.filtered(value-> value.getPseudo().equals(login.getText())).get(0));
                    UserEntity.DAO.update(userUpdate);
                }
                else if(isUserCheckBox.isSelected() && !userExistsAlready()){ // New user
                    UserEntity newUser = setUser(new UserEntity());
                    newUser.setId(personUpdate.getId());
                    UserEntity.DAO.persist(newUser);
                    userEntities.add(newUser);
                }
                else if(!isUserCheckBox.isSelected() && userExistsAlready()) { // Removing user
                    UserEntity userUpdate = setUser(userEntities.filtered(value-> value.getPseudo().equals(login.getText())).get(0));
                    UserEntity.DAO.delete(userUpdate);
                    userEntities.remove(userUpdate);
                }
            }
            refreshView();
            hide();
        }
    }


    /**
     * Remplis les textFields pour l'édition du personnel
     * @param existingUsers la liste d'utilisateurs correspondant au personnel à updater
     * @param personnelEntity le personnel à updater
     */
    protected void fillFields(List<UserEntity> existingUsers, PersonnelEntity personnelEntity) {
        labelPersonnel.setText("Modifier du personnel");

        firstName.setText(personnelEntity.getFirstName());
        lastName.setText(personnelEntity.getLastName());
        phone.setText(personnelEntity.getPhoneNumber());
        email.setText(personnelEntity.getEmailAddress());

        List<PersonnelTypeEntity> typePersonnel = typePersonnels.filtered(type -> type.getId().toString().equals(personnelEntity.getPersonnelTypeId()));
        if(typePersonnel != null && !typePersonnel.isEmpty()) {
            comboType.getSelectionModel().select(typePersonnel.get(0));
        }

        if((existingUsers != null) && (!existingUsers.isEmpty())) {
            isUserCheckBox.setSelected(true);
            login.setText(existingUsers.get(0).getPseudo());
            password.setText(existingUsers.get(0).getPassword());
        } else {
            isUserCheckBox.setSelected(false);
            login.setText("");
            password.setText("");
        }
    }
}
