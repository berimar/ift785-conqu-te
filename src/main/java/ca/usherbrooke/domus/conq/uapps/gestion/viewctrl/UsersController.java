package ca.usherbrooke.domus.conq.uapps.gestion.viewctrl;

import ca.usherbrooke.domus.conq.commons.app.AppController;
import ca.usherbrooke.domus.conq.commons.fx.SlidingPresentation;
import ca.usherbrooke.domus.conq.db.model.*;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import ca.usherbrooke.domus.conq.uapps.gestion.Gestion;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.extern.java.Log;

import java.util.List;

/**
 * Contrôle la partie Utilisateurs de l'application Gestion. S'occupe du comportement principal.
 * <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class UsersController extends AppController<Gestion> {

    @FXML private TableColumn<PersonnelEntity, String> firstNameColumn;
    @FXML private TableColumn<PersonnelEntity, String> lastNameColumn;
    @FXML private TableColumn<PersonnelEntity, String> emailColumn;
    @FXML private TableColumn<PersonnelEntity, String> phoneColumn;
    @FXML private TableColumn<PersonnelEntity, String> typeColumn;

    @FXML private TableColumn<PersonnelEntity, Boolean> isUserColumn;
    @FXML private TableColumn<PersonnelEntity, String> loginColumn;
    @FXML private TableColumn<PersonnelEntity, String> passwordColumn;

    @FXML private TableView<PersonnelEntity> usersTableView;

    @FXML private SlidingPresentation contextualLeftMenu;
    private AddUser addUserPane = new AddUser();
    private EditUser editUserPane = new EditUser();

    private final List<PersonnelEntity> personnelEntities = PersonnelEntity.DAO.getAll();
    private final ObservableList<UserEntity> userEntities = FXCollections.observableArrayList(UserEntity.DAO.getAll());

    @FXML
    public void initialize() {
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<>("emailAddress"));
        phoneColumn.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        typeColumn.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getDisplayableType()));

        isUserColumn.setCellValueFactory( cell -> {
            if(!userEntities.filtered(value -> (cell.getValue().getId() == value.getId())).isEmpty()) {
                return new SimpleBooleanProperty(true);
            }
            else {
                return new SimpleBooleanProperty(false);
            }
        });
        isUserColumn.setCellFactory(cell -> {
            CheckBoxTableCell checkBox = new CheckBoxTableCell<>();
            checkBox.setDisable(true);
            return checkBox;
        });
        loginColumn.setCellValueFactory( cell -> {
            List<UserEntity> correspondingUser = userEntities.filtered(value -> (cell.getValue().getId() == value.getId()));
            if((correspondingUser != null) && !correspondingUser.isEmpty()) {
                return new SimpleStringProperty(correspondingUser.get(0).getPseudo());
            }
            else {
                return new SimpleStringProperty("");
            }
        });
        passwordColumn.setCellValueFactory(cell -> {
            List<UserEntity> correspondingUser = userEntities.filtered(value -> (cell.getValue().getId() == value.getId()));
            if ((correspondingUser != null) && !correspondingUser.isEmpty()) {
                return new SimpleStringProperty(correspondingUser.get(0).getPassword());
            } else {
                return new SimpleStringProperty("");
            }
        });
        usersTableView.getItems().addAll(personnelEntities);

        addUserPane.associate(usersTableView, userEntities);
        editUserPane.associate(usersTableView, userEntities);
    }


    /**
     * Méthode appelée lorsqu'on clique sur le bouton "Modifier"
     * <br />
     * Fait apparaître le SlidingPresentation d'ajout/édition. Remplis les textFields avec les informations de l'élément
     * sélectionnné
     *
     * @param actionEvent l'évènement déclenché par le clic
     */
    @FXML
    public void add(ActionEvent actionEvent) {
        addUserPane.getLabelPersonnel().setText("Ajouter du personnel");
        addUserPane.clearFields();
        contextualLeftMenu.show(addUserPane);
    }


    /**
     * Méthode appelée lorsqu'on clique sur le bouton "Modifier"
     * <br />
     * Fait apparaître le SlidingPresentation d'ajout/édition. Remplis les textFields avec les informations de l'élément
     * sélectionnné
     *
     * @param actionEvent l'évènement déclenché par le clic
     */
    @FXML
    public void edit(ActionEvent actionEvent) {
        if(usersTableView.getSelectionModel().getSelectedItem() != null) {
            PersonnelEntity personnelEntity = usersTableView.getSelectionModel().getSelectedItem();
            editUserPane.fillFields(userEntities.filtered(value -> value.getId().equals(personnelEntity.getId())), personnelEntity);
            contextualLeftMenu.show(editUserPane);
        }
    }


    /**
     * Méthode appelée lorsqu'on clique sur "Supprimer".
     * <br />
     * Supprime le personnel sélectionné et le profil utilisateur associé si applicable
     *
     * @param actionEvent l'évènement déclenché par le clic sur le bouton
     */
    @FXML
    public void delete(ActionEvent actionEvent) {
        final PersonnelEntity selectedElement = usersTableView.getSelectionModel().getSelectedItem();

        if(selectedElement != null) {
            UserEntity associatedUser = null;
            List<UserEntity> filteredUser = userEntities.filtered(value -> (selectedElement.getId() == value.getId()));
            if(!filteredUser.isEmpty()) {
                associatedUser = filteredUser.get(0);
                UserEntity.DAO.delete(associatedUser);
            }

            // TODO : delete en cascade automatiquement ?
            PersonnelEntity.DAO.delete(selectedElement);
            usersTableView.getItems().remove(selectedElement);

            final UserEntity userEntity = associatedUser;

            getBoundApp().getMsgFactory().canceller("Le personnel a bien été supprimé", () -> {
                selectedElement.setId(0);
                PersonnelEntity.DAO.save(selectedElement);

                if (userEntity != null) {
                    userEntity.setId(selectedElement.getId());
                    UserEntity.DAO.save(userEntity);
                }

                usersTableView.getItems().add(selectedElement);
                SupportApp.get().getMsgFactory().confirmer("Le contact " + selectedElement.getFirstName() + " " + selectedElement.getLastName() +
                        " a bien été restauré").show();
            }).show();
        }
    }
}
