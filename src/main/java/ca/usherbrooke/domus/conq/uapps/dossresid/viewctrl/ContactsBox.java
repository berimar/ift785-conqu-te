package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.commons.fx.util.CoherantUXHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.commons.fx.SlidingPresentation;
import ca.usherbrooke.domus.conq.commons.fx.internal.FxmlBindable;
import ca.usherbrooke.domus.conq.commons.fx.util.OptionalObservable;
import ca.usherbrooke.domus.conq.db.model.ContactEntity;
import ca.usherbrooke.domus.conq.db.model.ContactsResidentsEntity;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import lombok.Getter;
import lombok.extern.java.Log;

/**
 * Représente le composant qui permet de visualiser les contacts dans la page du résident
 *
 * date 21/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log
public class ContactsBox extends VBox implements FxmlBindable {

    CoherantUXHelper coherantUXHelper = new CoherantUXHelper(SupportApp.get().getPrimaryStage(), this);
    @Getter
    @FXML
    private ContactListView residentContactsView;
    @FXML private Button createNewContact;
    @FXML private Button addExistingContact;
    @Getter
    private final SeeContactBody seeContactMenu = new SeeContactBody();
    @Getter
    private final EditContactBody editContactMenu = new EditContactBody();
    @Getter
    private final ExistingContactBody existingContactMenu = new ExistingContactBody();
    private final NewContactBody newContactMenu = new NewContactBody();

    @Getter
    private final OptionalObservable<ContactsResidentsEntity> selectedAssociation = new OptionalObservable<>();
    @Getter
    private final OptionalObservable<ResidentEntity> selectedResident = new OptionalObservable<>();

    @Getter
    private SlidingPresentation slidingPresentation;

    @FXML
    void initialize() {
        Platform.runLater(() -> {
            coherantUXHelper.applyTooltipToId("#addExistingContact", "Ratacher un contact\n" +
                    "existant à ce résident");
            coherantUXHelper.applyTooltipToId("#addContact", "Ajouter et créer\nun nouveau contact");
            coherantUXHelper.addGlyphToButtonClasses(
                    GlyphHelper.ADD_HELPER,
                    GlyphHelper.CHOOSE_EXISTING_HELPER);
            // Listener pour écouter le changement de sélection dans la liste de contacts
            residentContactsView.setOnSelectionCallback(this::showContactDetails);
            residentContactsView.setOnDeleteCallback(deletedEntity -> selectedAssociation.set(null));
            selectedResident.addListener((observable, oldValue, newValue) -> {
                residentContactsView.setData(newValue.getContactEntities());
                selectedAssociation.set(null);
            });
            residentContactsView.getSelectedPerson().addListener((observable, oldValue, newValue) -> {
                if (newValue == null)
                    selectedAssociation.set(null);
            }); //TODO à faire plus propre
            // binding de la propriété "disabled" des boutons
            createNewContact.disableProperty().bind(isEditingProperty());
            addExistingContact.disableProperty().bind(isEditingProperty());
            residentContactsView.disableProperty().bind(isEditingProperty());
        });


    }

    public void bind(DossierResidentsController dossierResidentsController) {
        dossierResidentsController.getSelectedContactAssociation().bind(selectedAssociation);
        selectedResident.bindBidirectional(dossierResidentsController.getSelectedResident());
        slidingPresentation = dossierResidentsController.getSlidingPresentation();
        seeContactMenu.bind(residentContactsView, editContactMenu, selectedAssociation, selectedResident);
        editContactMenu.bind(this);
        newContactMenu.bind(this);
        existingContactMenu.bind(this);
    }

    /**
     * Méthode appelée lorsqu'on clique sur le bouton "Nouveau"
     */
    @FXML
    public void newContact() {
        log.info("Nouveau Contact");
        slidingPresentation.show(newContactMenu);
    }

    public ContactsBox() {
        bindToFXML();
    }

    @FXML
    public void openExistingContactPane() {
        slidingPresentation.show(existingContactMenu);
    }

    /**
     * Méthode appelée quand un contact est sélectionné dans la liste. Affiche ses infos et le panneau à droite
     * @param contactEntity le contact à afficher
     */
    private void showContactDetails(ContactEntity contactEntity) {
        if (contactEntity != null) {
            ContactsResidentsEntity association = ContactsResidentsEntity.DAO.getByCriteriae("contactEntity", contactEntity, "residentEntity", selectedResident.get());
            selectedAssociation.set(association);
            // Affichage du panneau de droite
            slidingPresentation.show(seeContactMenu);
        }
    }

    @Override
    public String getFXMLPath() {
        return "ContactsBox.fxml";
    }

    public BooleanBinding isEditingProperty(){
        return existingContactMenu.getIsShowingReadOnlyProperty()
                .or(newContactMenu.getIsShowingReadOnlyProperty())
                .or(editContactMenu.getIsShowingReadOnlyProperty());
    }

}
