package ca.usherbrooke.domus.conq.uapps.jfxtagenda.viewctrl;

import ca.usherbrooke.domus.conq.commons.fx.util.ButtonTemplate;
import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.supp.msg.FeedbackMsgHelper;
import ca.usherbrooke.domus.conq.uapps.jfxtagenda.JFXActivity;

/**
 * Classe qui s'occupe du comportement de la modification d'activité
 * <br />
 * date 07/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class EditActivityBody extends WriteActivityBody {

    private FeedbackMsgHelper succes = getBoundApp().getMsgFactory().informer("L'activité a bien été modifiée.");

    public EditActivityBody(JFXActivity activity) {
        super(activity);
    }

    @Override
    protected void runPersistingStrategy() {
        activity.update();
    }

    @Override
    protected FeedbackMsgHelper getSucces() {
        return succes;
    }

    @Override
    protected ButtonTemplate.Builder getCancelTemplate() {
        return GlyphHelper.CANCEL_HELPER.newButtonTemplteBuildr()
                .label("Annuler l'édition");
    }

}
