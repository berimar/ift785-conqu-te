package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.commons.fx.util.GlyphHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.OptionalObservable;
import ca.usherbrooke.domus.conq.commons.fx.model.SeePersonBody;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import lombok.Getter;
import lombok.NonNull;

/**
 * Classe qui s'occupe du comportement lors de la visualisation d'un résident
 * <br />
 * date 20/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class SeeResidentBody extends SeePersonBody {
    @Getter
    private final EditResidentBody editResidentBody = new EditResidentBody();

    @Getter
    @FXML
    private ContactsBox contactsBox ;

    @FXML
    private Label apartNumberLabel;
    @FXML
    private Label birthDateLabel;
    private final OptionalObservable<ResidentEntity> selectedResident = new OptionalObservable<>();
    private ResidentListView residentListView;

    public SeeResidentBody() {
        bindToFXML();
        buttonsTemplates.add(GlyphHelper.DELETE_HELPER.newButtonTemplteBuildr()
                .label("Supprimer Résident")
                .cssId("deleteResident")
                .tooltip("Supprimer définitivement le résident")
                .onAction(() -> residentListView.getPersons().remove(selectedResident.get()))
                .build());
        buttonsTemplates.add(GlyphHelper.EDIT_HELPER.newButtonTemplteBuildr()
                .label("Modifier Résident")
                .cssId("editResident")
                .tooltip("Editer le profil du résident")
                .onAction(() -> showNext(editResidentBody))
                .build());
        selectedResident.addListener((observable, oldValue, newValue) -> {
            updateResidentFields(newValue);
        });
        isShowingProperty.addListener((obs,old,neu)->{
            if(neu&&!old){
                selectedResident.getOptional().ifPresent(this::updateResidentFields);
            }
        });
    }

    /**
     * Affiche les informations du résident donné en argument dans les labels
     * @param residentEntity le résident dont les informations sont à afficher
     */
    private void updateResidentFields(ResidentEntity residentEntity){
        updatePersonFields(residentEntity);
        apartNumberLabel.setText(String.valueOf(residentEntity.getApartmentEntity().getApartmentNumber()));
        birthDateLabel.setText(residentEntity.getBirthday());
    }


    public void bind(@NonNull DossierResidentsController controller) {
        this.selectedResident.bind(controller.getSelectedResident());
        residentListView = controller.getResidentListView();
        editResidentBody.bind(controller);
        contactsBox.bind(controller);
    }

    @Override
    public String getFXMLPath() {
        return "SeeResidentBody.fxml";
    }


    @Override
    public void onMappedButtonUpdate(){
        getButtonById("deleteResident").disableProperty().bind(contactsBox.isEditingProperty());
        getButtonById("editResident").disableProperty().bind(contactsBox.isEditingProperty());
    }

}
