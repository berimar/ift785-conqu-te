package ca.usherbrooke.domus.conq.uapps.jfxtagenda;

import ca.usherbrooke.domus.conq.db.model.ActivityEntity;
import ca.usherbrooke.domus.conq.db.model.PersonnelEntity;
import ca.usherbrooke.domus.conq.db.model.ResidentEntity;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import jfxtras.scene.control.agenda.Agenda;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;


/**
 * date 26/03/15 <br/>
 * Une classe dont les instances représentent des évènements enregistrés dans l'Agenda.
 *
 * @author sv3inburn3
 * @implNote Implémentation du pattern Adapter pour interfacer une entité hibernate à un élément graphique de la lib jfxtras
 */
@SuppressWarnings("ALL")
@Log
public class JFXActivity extends Agenda.AppointmentImpl {

    @Getter
    @Setter
    private ActivityEntity persistentActivity;

    private final ObjectProperty<List<ResidentEntity>> residents = new SimpleObjectProperty<>(new ArrayList<>());
    private final ObjectProperty<List<PersonnelEntity>> personnels = new SimpleObjectProperty<>(new ArrayList<>());


    @Override
    public void setStartLocalDateTime(LocalDateTime startLocalDateTime) {
        ZoneId zone = startZonedDateTime != null ? startZonedDateTime.getZone() : ZoneId.systemDefault();
        startZonedDateTime = ZonedDateTime.of(startLocalDateTime, zone);
    }

    @Override
    public LocalDateTime getStartLocalDateTime() {
        return startZonedDateTime.toLocalDateTime();
    }

    @Override
    public void setEndLocalDateTime(LocalDateTime startLocalDateTime) {
        ZoneId zone = endZonedDateTime != null ? endZonedDateTime.getZone() : ZoneId.systemDefault();
        endZonedDateTime = ZonedDateTime.of(startLocalDateTime, zone);
    }

    @Override
    public LocalDateTime getEndLocalDateTime() {
        return endZonedDateTime.toLocalDateTime();
    }

    @Getter
    @Setter
    private ZonedDateTime startZonedDateTime;

    @Getter
    @Setter
    private ZonedDateTime endZonedDateTime;

    public ObjectProperty<List<ResidentEntity>> residentProperty() {
        return this.residents;
    }

    public List<ResidentEntity> getResidents() {
        return (List<ResidentEntity>) this.residents.getValue();
    }

    public void setResident(List<ResidentEntity> collection) {
        this.residents.setValue(collection);
    }

    public ObjectProperty<List<PersonnelEntity>> personnelProperty() {
        return this.personnels;
    }

    public List<PersonnelEntity> getPersonnels() {
        return (List<PersonnelEntity>) this.personnels.getValue();
    }

    public JFXActivity withResidents(List<ResidentEntity> residents) {
        this.residents.set(residents);
        return this;
    }

    public JFXActivity withPersonnels(List<PersonnelEntity> personnels) {
        this.personnels.set(personnels);
        return this;
    }

    public void setPersonnel(List<PersonnelEntity> collection) {
        this.personnels.setValue(collection);
    }

    private JFXActivity(ActivityEntity persistentActivity) {
        this.persistentActivity = persistentActivity;
    }

    private JFXActivity() {
    }

    public void bindAndPersist() {
        if (!isBound()) {
            persistentActivity = ActivityEntity.bind(this);
            log.info("Activité sauvée en base");
        }
    }

    /**
     * Envoie les modifications en base de données
     */
    public void update() {
        ActivityEntity.DAO.update(persistentActivity);
        log.info("Activité mise à jour");
    }

    /**
     * Retourne une activité de la base de donnée correspondant à celle donnée en argument
     * @param activityEntity l'activité à rechercher en base
     * @return l'activité recherchée
     */
    public static JFXActivity fromDB(ActivityEntity activityEntity) {
        return new JFXActivity(activityEntity);
    }

    /**
     * Crée une nouvelle activité vide
     * @return la nouvelle activité
     */
    public static JFXActivity empty() {
        JFXActivity activity = (JFXActivity) new JFXActivity()
                .withLocation("")
                .withSummary("")
                .withAppointmentGroup(new Agenda.AppointmentGroupImpl().withStyleClass("group0").withDescription("Personnel"));
        activity.setStartLocalDateTime(LocalDateTime.now());
        activity.setEndLocalDateTime(LocalDateTime.now().plusHours(1));
        return activity;
    }

    public boolean isBound() {
        return persistentActivity != null;
    }

    /**
     * Supprime l'activité de la BD
     */
    public void delete() {
        if (isBound()) {
            log.info("Activité retirée de la base");
            ActivityEntity.DAO.delete(persistentActivity);
            persistentActivity.setBoundJFXActivity(null);
        }
    }

    @Override
    public String toString() {
        return "JFXActivity{" +
                "persistentActivity=" + (persistentActivity == null ? "null" : persistentActivity.toString()) +
                '}';
    }

    public void rebind() {
        persistentActivity.setId(null);
        persistentActivity.setBoundJFXActivity(this);

        ActivityEntity.DAO.persist(persistentActivity);

    }
}
