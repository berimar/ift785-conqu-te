package ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl;

import ca.usherbrooke.domus.conq.commons.fx.util.FXUtils;
import ca.usherbrooke.domus.conq.commons.fx.util.OptionalObservable;
import ca.usherbrooke.domus.conq.db.HibernateDAO;
import ca.usherbrooke.domus.conq.db.model.Person;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import ca.usherbrooke.domus.conq.supp.msg.FeedbackMsgHelper;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.StackPane;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Classe qui rassemble le comportement commun aux deux listView ({@link ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl.ContactListView}
 * et ({@link ca.usherbrooke.domus.conq.uapps.dossresid.viewctrl.ResidentListView} (par exemple les listeners, le rafraichissement, etc)
 * <br />
 * date 11/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Log

public abstract class PersonListView<T extends Person> extends StackPane {
    @RequiredArgsConstructor
    public enum SelectionStrategy {
        DO_NOTHING(StrategyBinding::doNothingStrategy),
        CALL_DAO(StrategyBinding::callDaoStrategy);
        private final Function<PersonListView, StrategyBinding> binder;

        public <T extends Person> StrategyBinding<T> bind(PersonListView<T> personView) {
            return binder.apply(personView);
        }

    }

    @RequiredArgsConstructor
    private static class StrategyBinding<T extends Person> {

        private static <V extends Person> StrategyBinding<V> callDaoStrategy(PersonListView<V> personView) {
            return new StrategyBinding<>(personView,
                    deletedPerson -> {
                        personView.getHibernateDAO().delete(deletedPerson);
                        FeedbackMsgHelper msg = SupportApp.get().getMsgFactory().canceller(
                                "Le " + deletedPerson.getDisplayableType() + " " + deletedPerson.toDisplayableName() + " a été supprimé",
                                () -> {
                                    if (!personView.personTable.getItems().contains(deletedPerson)) {
                                        deletedPerson.setId(null); //efface la référence pour pouvoir réinsérer en base
                                        personView.personTable.getItems().add(deletedPerson);
                                    }
                                    SupportApp.get().getMsgFactory().confirmer("Le " + deletedPerson.getDisplayableType() +
                                            deletedPerson.toDisplayableName() + " a bien été réstauré").show();
                                });
                        msg.show();
                    },
                    addedPerson -> personView.getHibernateDAO().save(addedPerson));
        }

        private static <V extends Person> StrategyBinding<V> doNothingStrategy(PersonListView<V> personView) {
            return new StrategyBinding<>(personView, StrategyBinding::doNothing, StrategyBinding::doNothing);
        }


        protected final PersonListView<T> personView;

        @Getter
        private final Consumer<T> onDelete;
        @Getter
        private final Consumer<T> onAdding;

        private static <T> void doNothing(T entity) {/**/}
    }

    @FXML
    protected TableColumn<T, String> lastNameColumn;

    @FXML
    protected TableColumn<T, String> firstNameColumn;

    @Getter
    @FXML
    protected TableView<T> personTable;

    public ObjectProperty<SelectionStrategy> strategyProperty = new SimpleObjectProperty<>(SelectionStrategy.DO_NOTHING);
    private StrategyBinding<T> strategyBinding;

    private OptionalObservable<T> selectedPerson = new OptionalObservable<>();


    @FXML
    public SelectionStrategy getStrategy() {
        return strategyProperty.get();
    }

    @FXML
    public void setStrategy(SelectionStrategy selectionStrategy) {
        this.strategyProperty.set(selectionStrategy);
    }


    private Optional<Consumer<T>> onSelectionCallback = Optional.empty();

    private Optional<Consumer<T>> onDeleteCallback = Optional.empty();

    private Optional<Consumer<T>> onAddingCallback = Optional.empty();


    //listener pour mettre à jour la sélection après ajout/ suppression
    private final ListChangeListener<? super T> listener = c -> {
        while (c.next()) {
            if (c.getRemovedSize() > 0) {
                c.getRemoved().forEach(strategyBinding.getOnDelete());
                this.onDeleteCallback.ifPresent(calbk -> c.getAddedSubList().forEach(calbk::accept));
                selectFirstIfPresent();
            }
            if (c.getAddedSize() > 0) {
                c.getAddedSubList().forEach(strategyBinding.getOnAdding());
                this.onAddingCallback.ifPresent(calbk -> c.getAddedSubList().forEach(calbk::accept));
                personTable.getSelectionModel().select(c.getAddedSubList().get(0));
            }
        }
    };

    private void selectFirstIfPresent() {
        personTable.getSelectionModel().selectFirst();
        if(personTable.getSelectionModel().getSelectedItem()==null){
            selectedPerson.set(null);
        }
    }


    public PersonListView() {
        FXUtils.bindToFXML(this, "PersonListView.fxml");
    }

    /**
     * Callback appellé lorsqu'une entité est sélectionnée
     */
    public void setOnSelectionCallback(@NonNull Consumer<T> onSelectionCallback) {
        this.onSelectionCallback = Optional.of(onSelectionCallback);
    }

    /**
     * Callback appellé après qu'une  entité est retirée de la liste
     */
    public void setOnDeleteCallback(@NonNull Consumer<T> onDeleteCallback) {
        this.onDeleteCallback = Optional.of(onDeleteCallback);
    }

    /**
     * Callback appellé après qu'une entité est ajoutée à la liste
     */
    public void setOnAddingCallback(@NonNull Consumer<T> onAddingCallback) {
        this.onAddingCallback = Optional.of(onAddingCallback);
    }


    @FXML
    private void initialize() {
        Platform.runLater(() -> {
            strategyBinding = strategyProperty.get().bind(this);
            // Listener pour écouter le changement de sélection dans la liste de résidents
            personTable.getSelectionModel().selectedItemProperty().addListener(
                    (observable, oldValue, newValue) -> {
                        if (newValue != null && newValue.getId() != null) {
                            refreshPerson(newValue);
                            onSelectionCallback.ifPresent(calbk -> calbk.accept(newValue));
                            selectedPerson.set(newValue);
                        }
                    });
            onInitialize();
        });
    }

    protected abstract void onInitialize();

    /**
     * @param personData
     */
    public void setData(ObservableList<T> personData) {
        getPersons().removeListener(listener);
        personTable.setItems(personData);
        personTable.getSelectionModel().clearSelection();
        personData.addListener(listener);
    }

    /**
     * @param personData
     */
    public void setData(Set<T> personData) {
        getPersons().removeListener(listener);
        personTable.setItems(FXCollections.observableArrayList(personData));
        personTable.getSelectionModel().clearSelection();
        getPersons().addListener(listener);
    }

    public void setData(List<T> personData) {
        getPersons().removeListener(listener);
        personTable.setItems(FXCollections.observableList(personData));
        personTable.getSelectionModel().clearSelection();
        getPersons().addListener(listener);
    }

    /**
     * Force le rafraichissement de la {@code TableView} associée.
     * Utile lorsque le firstName et lastName sont modifiés, car les Entities ne sont pas observables
     */
    public void refreshView() {
        // Dirty fix pour que les noms de résidents dans la liste (personTable) soient updatés.
        // Si ça avait été des StringProperty, l'update aurait été automatique...
        personTable.getColumns().get(0).setVisible(false);
        personTable.getColumns().get(0).setVisible(true);
    }


    protected void refreshPerson(T personEntity) {
        if (personEntity.getId() != null) {
            getHibernateDAO().refresh(personEntity);
            log.info("Rafraichissement du " + personEntity.getDisplayableType());
        }
    }

    /**
     * @return la liste observable d'entités contenues dans la {@code TableView}
     */
    public ObservableList<T> getPersons() {
        return personTable.getItems();
    }

    public OptionalObservable<T> getSelectedPerson() {
        return this.selectedPerson;
    }


    protected abstract HibernateDAO<T> getHibernateDAO();

    /**
     * Retire l'entité silencieusement (sans faire intervenir les listeners)
     *
     * @param person l'entité à retirer
     */
    public void detachPerson(T person) {
        getPersons().removeListener(listener);
        getPersons().remove(person);
        refreshView();
        selectFirstIfPresent();
        getPersons().addListener(listener);
    }

}
