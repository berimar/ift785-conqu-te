package ca.usherbrooke.domus.conq.uapps;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * date 20/03/15 <br/>
 * Annotation "flag" pour que l'{@link UApp} ne soit pas instanciée au démarrage de l'application.
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Ignore {
}
