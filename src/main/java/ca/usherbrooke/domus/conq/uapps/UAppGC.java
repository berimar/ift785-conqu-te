package ca.usherbrooke.domus.conq.uapps;

import ca.usherbrooke.domus.conq.commons.app.FocusableApp;
import ca.usherbrooke.domus.conq.commons.app.GraphicalContext;
import ca.usherbrooke.domus.conq.commons.app.LCApp;
import ca.usherbrooke.domus.conq.commons.fx.util.CoherantUXHelper;
import ca.usherbrooke.domus.conq.commons.fx.util.FAGlyphs;
import ca.usherbrooke.domus.conq.commons.fx.util.FXUtils;
import ca.usherbrooke.domus.conq.supp.SupportAppGC;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.java.Log;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

import java.util.function.Consumer;

/**
 * date 12/03/15 <br/>
 * Tout le contexte graphique d'une UApp est contenu dans cette classe. <br/>
 * Par contexte graphique, on entend interraction graphique avec l'application support.
 * <strong>Une {@code UApp} concrète doit obligatoirement être paramétrée avec une classe concrête de {@link ca.usherbrooke.domus.conq.uapps.UAppGC}</strong>
 *
 * @author sv3inburn3
 * @implSpec L'implémentation concrête doit <strong>obligatoirement</strong> avoir un  constructeur <emphasis>public</emphasis> à deux arguments {@code (UApp uapp, SupportAppGC supportAppGC)} ou bien l'UApp rencontrera une erreur d'instanciation.
 * @implNote C'est une implémentation du pattern Mediator entre une {@link UApp} et la {@link ca.usherbrooke.domus.conq.supp.SupportApp} <br>
 * <strong>A noter aussi que l'utilisation de la "lazy instanciation" permet un chargement du contexte graphique au moment du premier appel, évitant ainsi un démarrage lourd de l'application.</strong>
 */
@SuppressWarnings("MethodParameterOfConcreteClass")
@ToString
public abstract class UAppGC implements GraphicalContext<AnchorPane>, FocusableApp {

    private static final String LOADED_STYLECLASS = "loaded";
    private static final String LOADING_STYLECLASS = "loading";
    private static final String SELECTED_STYLECLASS = "selected";

    /**
     * Implémentation par défaut. Dépréciée : il est fortement recommandé de faire une implémentation propre à l'{@link UApp}
     */
    @Deprecated
    @Log
    public static class DummyConcrete extends UAppGC {

        @Getter
        private final Glyph glyphApp = FAGlyphs.getIcon(FontAwesome.Glyph.APPLE, Color.BEIGE);

        /**
         * Instancie le contexte graphique de la micro application.
         *
         * @param uapp         la micro application
         * @param supportAppGC le contexte graphique de l'application support
         * @throws IllegalStateException si le nom du fichier .fxml attaché à l'{@link ca.usherbrooke.domus.conq.uapps.UApp} ne respecte pas les conventions de nommage ou si son contenu est erronné.
         */
        public DummyConcrete(UApp<?> uapp, SupportAppGC supportAppGC) {
            super(uapp, supportAppGC);
        }

        @Override
        public void onRootPaneLoading(AnchorPane rootPane) {
            log.warning("Il est préférable d'implémenter cette classe soi même pour lui déléguer toute" +
                    "la gestion graphique de l'UApp et avoir une bonne séparation modèle/vue/contrôleur.");
        }
    }

    /**
     * Un utilitaire pour faire des opérations sur l'arborescence graphique
     */
    @Getter
    protected CoherantUXHelper coherantUXHelper;

    /**
     * L'UApp liée à ce contexte graphique
     */
    protected final UApp<?> uapp;

    /**
     * L'interface de communication avec le contexte graphique de l'application support
     */
    @Getter
    private final SupportAppGC supportAppGC;
    /**
     * l'{@link javafx.scene.layout.AnchorPane} constituant l'ancre de la micro-application dans le contexte de l'application support.
     */
    @Getter(lazy = true)
    private final AnchorPane rootPane = loadRootPane();
    @Getter(AccessLevel.PRIVATE)
    private Button button;
    @Getter(lazy = true)
    private final ObservableList<String> buttonStyles = button.getStyleClass();
    private State state = State.HIDDEN;


    /**
     * Renvoie le Glyph associé à l'uApp
     *
     * @return le Glyph associé à l'uApp
     */
    public abstract Glyph getGlyphApp();


    /**
     * Instancie le contexte graphique de la micro application.
     *
     * @param uapp         la micro application
     * @param supportAppGC le contexte graphique de l'application support
     * @throws java.lang.IllegalStateException si le nom du fichier .fxml attaché à l'{@link UApp} ne respecte pas les conventions de nommage ou si son contenu est erronné.
     */
    public UAppGC(UApp<?> uapp, SupportAppGC supportAppGC) {
        this.uapp = uapp;
        this.supportAppGC = supportAppGC;
    }

    private AnchorPane loadRootPane() {
        AnchorPane pane = FXUtils.loadFXML(uapp.getClass(), "viewctrl/" + uapp.getName() + "Layout.fxml");
        this.coherantUXHelper = new CoherantUXHelper(getSupportAppGC().getStage(), pane);
        onRootPaneLoading(pane);
        button.getStyleClass().add(uapp.getName());
        return pane;
    }

    @Override
    public void show() {
        getButtonStyles().add(SELECTED_STYLECLASS);
        supportAppGC.showUApp(this);
        state = State.SHOWN;
    }

    @Override
    public void hide() {
        getButtonStyles().remove(SELECTED_STYLECLASS);
        supportAppGC.hideUApp(this);
        state = State.HIDDEN;
    }

    /**
     * Méthode appellée juste après le chargement "paresseux" de l'ancre de l'{@code UApp}.
     *
     * @param rootPane l'ancre javafx de l'{@link UApp}
     * @implNote il est strictement interdit d'appeller getRootPane() depuis cette méthode, ou bien une {@code StackOverflowException} sera levée.
     */
    public abstract void onRootPaneLoading(AnchorPane rootPane);

    /**
     * Change l'état du contexte graphique de la micro application.
     * Si elle était câchée, elle sera affichée et inversement.
     */
    public void switchGC() {
        if (uapp.getState() == LCApp.State.UNLOADED)
            uapp.start();
        state.swap(this);
    }

    /**
     * Relie un bouton aux contrôles de l'{@code UApp}
     *
     * @param button Le bouton à associer
     */
    public void bindButton(Button button) {
        this.button = button;
        button.setText(uapp.getDisplayedName());
        button.setOnAction(event -> switchGC());
        uapp.addStateListener((observable, oldValue, newValue) -> {
            if (oldValue == LCApp.State.LOADING && newValue == LCApp.State.LOADED) {
                getButtonStyles().add(LOADED_STYLECLASS);
            } else if (oldValue == LCApp.State.LOADED && newValue == LCApp.State.UNLOADED) {
                getButtonStyles().remove(LOADED_STYLECLASS);
            }
        });
    }

    /**
     * Etat "graphique" de l'application
     */
    public enum State {
        HIDDEN(UAppGC::show),
        SHOWN(UAppGC::hide);
        private final Consumer<UAppGC> switcher;

        State(Consumer<UAppGC> switcher) {
            this.switcher = switcher;
        }

        /**
         * Inverse l'état de micro application dans le contexte graphique de l'application support.
         *
         * @param ugc le contexte graphique de la micro application
         */
        private void swap(UAppGC ugc) {
            switcher.accept(ugc);
        }
    }


}
