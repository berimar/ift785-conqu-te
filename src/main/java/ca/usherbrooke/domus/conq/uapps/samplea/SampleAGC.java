package ca.usherbrooke.domus.conq.uapps.samplea;

import ca.usherbrooke.domus.conq.commons.fx.util.FAGlyphs;
import ca.usherbrooke.domus.conq.supp.SupportAppGC;
import ca.usherbrooke.domus.conq.uapps.UApp;
import ca.usherbrooke.domus.conq.uapps.UAppGC;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import lombok.Getter;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

/**
 * date 08/04/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class SampleAGC extends UAppGC {

    @Getter
    private Glyph glyphApp = FAGlyphs.getIcon(FontAwesome.Glyph.BEER, Color.AQUA);

    /**
     * Instancie le contexte graphique de la micro application.
     *
     * @param uapp         la micro application
     * @param supportAppGC le contexte graphique de l'application support
     * @throws IllegalStateException si le nom du fichier .fxml attaché à l'{@link ca.usherbrooke.domus.conq.uapps.UApp} ne respecte pas les conventions de nommage ou si son contenu est erronné.
     */
    public SampleAGC(UApp<?> uapp, SupportAppGC supportAppGC) {
        super(uapp, supportAppGC);
    }

    @Override
    public void onRootPaneLoading(AnchorPane rootPane) {
        //traitements au chargement de l'Ancre graphique de l'UApp
    }
}
