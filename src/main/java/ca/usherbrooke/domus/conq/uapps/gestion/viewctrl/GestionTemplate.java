package ca.usherbrooke.domus.conq.uapps.gestion.viewctrl;

import ca.usherbrooke.domus.conq.commons.app.AppController;
import ca.usherbrooke.domus.conq.commons.fx.util.GraphicsUtils;
import ca.usherbrooke.domus.conq.db.model.GestionTypeEntity;
import ca.usherbrooke.domus.conq.uapps.gestion.Gestion;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 * -- Template Method Pattern --
 * <br/>
 * Cette classe est utilisée pour factoriser les deux méthodes identiques (ou presque) de ApartmentsController et ContactsController.
 * Elle définie une méthode saveGestionElement() (l'algorithme) et des méthodes abstraites (étapes de l'algorithme)
 * <br/>
 * Cette classe redéfinie aussi la méthode AppController.getBoundApp() ({@see AppController}) pour permettre aux controllers
 * qui héritent de cette classe d'atteindre la bound app et ainsi afficher des notifications.
 *
 * @author Claire
 */
public abstract class GestionTemplate extends AppController<Gestion> {

    public static final double SLIDING_DURATION = 200.0;

    /**
     * Variable utilisée pour aller chercher getBoundApp() pour les notifications
     */
    private final String superUAppName = "Gestion";

    @Override
    protected final Gestion getBoundApp() {
        return getBoundApp(superUAppName);
    }

    /**
     * Affiche la partie pour entrer la nouvelle valeur
     * @param slidingPane le HBox à slider
     */
    protected void showPane(HBox slidingPane) {
        GraphicsUtils.showHidePane(slidingPane.minWidthProperty(), 600, SLIDING_DURATION);
        slidingPane.setVisible(true);
    }

    /**
     * Cache la partie pour entrer la nouvelle valeur
     * @param slidingPane le HBox à slider
     */
    protected void hidePane(HBox slidingPane) {
        GraphicsUtils.showHidePane(slidingPane.minWidthProperty(), 0, SLIDING_DURATION);
        slidingPane.setVisible(false);
    }

    /**
     * Sauvegarde dans la DB et met à jour le TableView en ajoutant l'élément donné en argument dedans
     * @param newRelation l'élément à ajouter
     */
    abstract protected void dbSaveAndUpdateView(GestionTypeEntity newRelation);

    /**
     * Le nom du champs à mettre dans les messages d'erreur
     * @return le nom du champs à mettre dans les messages d'erreur
     */
    abstract protected String getNameOfGestion();

    /**
     * Retourne une liste filtrée d'éléments. Le filtrage se fait en fonction de l'élément donné en argument.
     * Cette liste est générée pour vérifier que l'élément n'est pas déjà présent dans la DB avant l'ajout.
     * @param newRelation l'élément par lequel filtrer
     * @return la liste filtrée
     */
    abstract protected ObservableList getExistingGestionList(GestionTypeEntity newRelation);

    /**
     * Crée l'entité, la remplie avec les champs nécessaires
     * @param textFieldToSave le textfield où se trouve l'information
     * @return l'entité remplie
     */
    abstract protected GestionTypeEntity createTypeEntity(TextField textFieldToSave);

    /**
     * Template Method qui sauve un appartement ou un contact dans Gestion.
     * @param textFieldToSave le textfield où est entré le nouveau nom
     * @param slidingPane le HBox à afficher ou cacher
     * @param regex la regex avec laquelle on s'assure d'avoir un nom correct (soit des nombres, soit des lettres, etc)
     */
    protected void saveGestionElement(TextField textFieldToSave, HBox slidingPane, String regex) {
        if(!textFieldToSave.getText().isEmpty() && textFieldToSave.getText().matches(regex)) {
            GestionTypeEntity newRelation = createTypeEntity(textFieldToSave);

            ObservableList filteredRelations = getExistingGestionList(newRelation);

            if(filteredRelations.size() == 0) { // Si la relation n'existe pas déjà, la sauvegarder dans la BD
                hidePane(slidingPane);

                dbSaveAndUpdateView(newRelation);

                textFieldToSave.clear();

                getBoundApp().getMsgFactory().confirmer(getNameOfGestion() + " a bien été ajoutée.").show();

            } else {
                getBoundApp().getMsgFactory().warner(getNameOfGestion() + " existe déjà.").show();
            }
        } else {
            getBoundApp().getMsgFactory().warner(getErrorMessageForGestion()).show();
        }
    }

    /**
     * Retourne le message d'erreur pour le type de sous-classe
     * @return le message d'erreur pour le type de sous-classe
     */
    abstract protected String getErrorMessageForGestion();

    /**
     * Enlève l'élément donné en argument dans la BD
     * @param selectedApartment l'élément à enlever
     */
    abstract protected void deleteInDB(GestionTypeEntity selectedApartment);

    /**
     * Supprime l'élément sélectionné dans le tableView donné en argument
     * @param tableView
     */
    protected void deleteElement(GestionTypeEntity selectedElement, TableView<? extends GestionTypeEntity> tableView) {
        if(selectedElement != null) {
            deleteInDB(selectedElement);
            tableView.getItems().remove(selectedElement);
            getBoundApp().getMsgFactory().confirmer(getNameOfGestion() + " a bien été supprimé").show();
        }
    }

    /**
     * Renvoie l'élément sélectionné du tableView donné en argument
     * @param tableView le tableView où l'élément est sélectionné
     * @return l'élément sélectionné
     */
    protected GestionTypeEntity getListSelectedItem(TableView<? extends GestionTypeEntity> tableView) {
        return tableView.getSelectionModel().getSelectedItem();
    }
}
