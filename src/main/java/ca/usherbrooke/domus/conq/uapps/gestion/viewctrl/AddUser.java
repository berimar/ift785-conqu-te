package ca.usherbrooke.domus.conq.uapps.gestion.viewctrl;

import ca.usherbrooke.domus.conq.db.model.PersonnelEntity;
import ca.usherbrooke.domus.conq.db.model.UserEntity;
import ca.usherbrooke.domus.conq.supp.SupportApp;
import lombok.extern.java.Log;

/**
 * S'occupe de l'ajout de personnel. Cette classe permet d'enregistrer en base un nouveau personnel.
 *
 * @author Claire
 */
@Log
public class AddUser extends WriteUserBody {

    /**
     * Valide le nouveau Personnel et le crée en base
     */
    @Override
    public void createUpdateUser() {
        log.info("Creating User...");

        if(userExistsAlready() && isUserCheckBox.isSelected()) {
            SupportApp.get().getMsgFactory().warner("Le nom d'utilisateur existe déjà.").show();
        }
        else if(checkUserInput()) {
            PersonnelEntity newPerson = setPerson(new PersonnelEntity());
            PersonnelEntity.DAO.persist(newPerson);
            usersTableView.getItems().add(newPerson);

            if(isUserCheckBox.isSelected()) {
                UserEntity newUser = setUser(new UserEntity());
                newUser.setId(newPerson.getId());
                UserEntity.DAO.persist(newUser);
                userEntities.add(newUser);
            }

            hide();
        }
    }
}
