package ca.usherbrooke.domus.conq.sqlite;

import lombok.ToString;
import lombok.extern.java.Log;
import org.sqlite.JDBC;

import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * date 05/04/15 <br/>
 * Driver sqlite supportant les contraintes sur clés étrangères.
 *
 * @author sv3inburn3
 */
@Log
@ToString
public class PragmaDriverDecorator implements Driver {

    private JDBC originalDriver;

    public PragmaDriverDecorator() {
        try {
            Class.forName("org.sqlite.JDBC");
            final Driver driver = DriverManager.getDriver(JDBC.PREFIX);
            this.originalDriver = (JDBC) driver;
            DriverManager.deregisterDriver(driver);
            DriverManager.registerDriver(this);
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Impossible d'enregistrer la base de données.", e);
        } catch (ClassNotFoundException e) {
            log.log(Level.SEVERE, "Impossible de trouver le driver sqlite.", e);
        }
    }

    @Override
    public Connection connect(String url, Properties info) throws SQLException {
        final Connection connection = originalDriver.connect(url, info);
        initPragmas(connection);
        return connection;
    }

    @Override
    public boolean acceptsURL(String url) {
        return originalDriver.acceptsURL(url);
    }

    @Override
    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
        return originalDriver.getPropertyInfo(url, info);
    }

    @Override
    public int getMajorVersion() {
        return originalDriver.getMajorVersion();
    }

    @Override
    public int getMinorVersion() {
        return originalDriver.getMinorVersion();
    }

    @Override
    public boolean jdbcCompliant() {
        return originalDriver.jdbcCompliant();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return originalDriver.getParentLogger();
    }

    private void initPragmas(Connection connection) throws SQLException {
        connection.prepareStatement("PRAGMA foreign_keys = ON;").execute();
    }
}
