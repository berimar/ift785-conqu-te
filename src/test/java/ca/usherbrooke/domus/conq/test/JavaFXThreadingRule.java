package ca.usherbrooke.domus.conq.test;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import lombok.extern.java.Log;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import javax.swing.*;
import java.util.concurrent.CountDownLatch;

/**
 * <a href="https://gist.github.com/andytill/3835914">cf ce projet</a> </br> /
 * <a href="http://stackoverflow.com/questions/28245555/how-do-you-mock-a-javafx-toolkit-initialization">cf ce stackoverflow</a> </br>
 * A JUnit {@link Rule} for running tests on the JavaFX thread and performing
 * JavaFX initialisation. To include in your test case, add the following code:
 * <pre>
 * {@literal @}Rule
 * public JavaFXThreadingRule jfxRule = new JavaFXThreadingRule();
 * </pre>
 * @author Andy Till
 *
 */
@Log
public class JavaFXThreadingRule implements TestRule {
    /**
     * Flag for setting up the JavaFX, we only need to do this once for all tests.
     */
    private static boolean jfxIsSetup;

    @Override
    public Statement apply(Statement statement, Description description) {
        return new OnJFXThreadStatement(statement);
    }

    private static class OnJFXThreadStatement extends Statement {
        private final Statement statement;

        public OnJFXThreadStatement(Statement aStatement) {
            statement = aStatement;
        }

        private Throwable rethrownException = null;
        @Override
        public void evaluate() throws Throwable {
            if(!jfxIsSetup) {
                setupJavaFX();
                jfxIsSetup = true;
            }
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            Platform.runLater(() -> {
                try {
                    statement.evaluate();
                } catch (Throwable e) {
                    rethrownException = e;
                }
                countDownLatch.countDown();
            });
            countDownLatch.await();
            // if an exception was thrown by the statement during evaluation,
            // then re-throw it to fail the test
            if(rethrownException != null) {
                throw rethrownException;
            }
        }

        protected void setupJavaFX() throws InterruptedException {
            long timeMillis = System.currentTimeMillis();
            final CountDownLatch latch = new CountDownLatch(1);
            SwingUtilities.invokeLater(() -> {
                // initializes JavaFX environment
                new JFXPanel();
                latch.countDown();
            });
            log.info("Test framework : javafx initialising...");
            latch.await();
            log.info("Test framework : javafx is initialised in " + (System.currentTimeMillis() - timeMillis) + "ms");
        }
    }
}
