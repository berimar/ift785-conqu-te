package ca.usherbrooke.domus.conq.supp.msg;

import ca.usherbrooke.domus.conq.commons.fx.util.FAGlyphs;
import lombok.extern.java.Log;
import org.junit.BeforeClass;
import org.junit.Test;

@Log
public class FeedbackMsgReactorTest {

    private static FeedbackMsgReactor reactor ;
    @BeforeClass
    public static void init(){
        FAGlyphs.init();
        reactor= new FeedbackMsgReactor();
    }

    @Test
    public void testPush() throws Exception {
        log.info("Hello buddy");
        reactor.push(new FeedbackMsgHelper(FeedbackMsgHelper.Severity.INFORM_USR, "Hello world", reactor));
        Thread t = new Thread(()-> {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ie){
                //lol
            }
        });
        t.start();
        t.join();
    }
}