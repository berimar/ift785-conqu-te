package ca.usherbrooke.domus.conq.supp;

import ca.usherbrooke.domus.conq.test.JavaFXThreadingRule;
import javafx.application.Application;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


@Log
public class SupportAppTest extends Application  {
    @Rule
    public JavaFXThreadingRule jfxRule = new JavaFXThreadingRule();

    private static Stage primaryStage ;



    @Test
    public void testGet() throws Exception {

        //TODO apprendre mockito!
    }

    @Test
    public void testInit() throws Exception {
        //TODO apprendre mockito!
    }



    @Override
    public void start(Stage primaryStage) throws Exception {
        assertNotNull(primaryStage);
        SupportAppTest.primaryStage=primaryStage;
    }
}