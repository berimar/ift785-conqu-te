package ca.usherbrooke.domus.conq.supp;

import ca.usherbrooke.domus.conq.uapps.UApp;
import javafx.stage.Stage;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class UAppsFinderTest {

    @Test
    public void testLookup() throws Exception {
        // Avec Mockito, pas besoin de fake classe :
        SupportAppGC supportAppGCMock = mock(SupportAppGC.class);

        // On définit le comportment qu'on veut avoir
        doReturn(mock(Stage.class)).when(supportAppGCMock).getStage();
        doNothing().when(supportAppGCMock).setStage(any());

        assertNotEquals(0, new UAppsFinder(supportAppGCMock).lookup(UApp.class.getPackage()));

//        assertNotEquals(0,new UAppsFinder(new SupportAppGCMock()).lookup(UApp.class.getPackage()));
    }
}