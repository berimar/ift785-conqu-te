package ca.usherbrooke.domus.conq.commons;

import lombok.extern.java.Log;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import static org.junit.Assert.*;

@Log
public class TimeUtilsTest {

    @Test
    public void testOperationsAreReversible() throws Exception {
        ZonedDateTime originalInstance = ZonedDateTime.now();
        String formatCalendar = TimeUtils.formatZonedDateTime(originalInstance);
        ZonedDateTime extractedInstance = TimeUtils.extractLocalDateTime(formatCalendar);
        assertEquals(TimeUtils.formatZonedDateTime(originalInstance), TimeUtils.formatZonedDateTime(extractedInstance));
        log.info(TimeUtils.formatZonedDateTime(originalInstance));
    }

}