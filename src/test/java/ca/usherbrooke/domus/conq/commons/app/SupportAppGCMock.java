package ca.usherbrooke.domus.conq.commons.app;

import ca.usherbrooke.domus.conq.supp.SupportAppGC;

/**
 * date 19/03/15 <br/>
 *
 * @author sv3inburn3 | jules.randolph@reseau.eseo.fr
 */
public class SupportAppGCMock extends SupportAppGC {
    public SupportAppGCMock() {
        super(null);
    }
}
