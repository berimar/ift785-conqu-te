package ca.usherbrooke.domus.conq.commons.app;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LCAppTest {
    private static LCApp application ;
    private static int sideEffectBeacon ;


    @BeforeClass
    public static void init() {
        application = new LCApp() {
            @Override
            protected void onStart() {
                sideEffectBeacon = 1;
            }

            @Override
            protected void onStop() {
                sideEffectBeacon = -1;
            }

            @Override
            protected String getName() {
                return "TestApp";
            }
        };
    }

    @Test
    public void testLifeCycle() throws Exception {
        application.stop();
        assertEquals(LCApp.State.UNLOADED,application.getState() );
        application.start();
        assertEquals( LCApp.State.LOADED,application.getState());
        application.stop();
    }

    @Test
    public void testOnStart() throws Exception {
        application.stop();
        sideEffectBeacon = 0;
        application.start();
        assertEquals(1, sideEffectBeacon);
    }

    @Test
    public void testOnStop() throws Exception {
        application.start();
        sideEffectBeacon = 0;
        application.stop();
        assertEquals(-1,sideEffectBeacon);
    }
}