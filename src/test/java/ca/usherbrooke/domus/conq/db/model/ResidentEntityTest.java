package ca.usherbrooke.domus.conq.db.model;

import junit.framework.TestCase;
import lombok.extern.java.Log;
import org.junit.Test;

import java.util.Collection;

@Log
public class ResidentEntityTest extends TestCase {

    @Test
    public void testContactAccess() throws Exception{
        Collection<ResidentEntity> residents = ResidentEntity.DAO.getAll();
        residents.forEach((resident) -> {
            assertNotNull(resident.getContactEntities());
            log.info(resident.getContactEntities().toString());
        });
    }

//    @Test
//    public void testRecursiveResidentAccess() throws Exception {
//        Collection<ResidentEntity> residents = ResidentEntity.RELATION_TYPE_DAO.getAll();
//        residents.forEach((resident) -> {
//            assertNotNull(resident.getContactEntities());
//            resident.getContactEntities().forEach((contact) -> {
//                assertNotNull(contact.getResidentEntities());
//                log.info(contact.getResidentEntities().toString());
//            });
//        });
//    }

//    @Test
//    public void testAddContact() throws Exception{
//        ContactEntity entity = new ContactEntity();
//        entity.setFirstName("TEST");
//        entity.setLastName("TEST");
//        ContactEntity.RELATION_TYPE_DAO.persist(entity);
//    }
}