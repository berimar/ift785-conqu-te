package ca.usherbrooke.domus.conq.db.model;

import junit.framework.TestCase;
import lombok.extern.java.Log;
import org.junit.Test;

import java.util.Collection;

@Log
public class ContactEntityTest extends TestCase {

    @Test
    public void testResidentAccess() throws Exception{
        Collection<ContactEntity> residents = ContactEntity.DAO.getAll();
        residents.forEach(TestCase::assertNotNull);
    }

//    @Test
//    public void testRecursiveContactAccess() throws Exception{
//        Collection<ContactEntity> residents = ContactEntity.RELATION_TYPE_DAO.getAll();
//        residents.forEach((contactEntity) -> {
//            contactEntity.getResidentEntities().forEach((resident) -> {
//                assertNotNull(resident.getContactEntities());
//                log.info(resident.getContactEntities().toString());
//            });
//            log.info(contactEntity.getResidentEntities().toString());
//        });
//    }

    @Test
    public void buildEntity () throws Exception{
        ContactEntity entity = new ContactEntity();
        assertNull(entity.getId());
//        assertNotNull(entity.getResidentEntities());
    }

    @Test
    public void testAddAndRemoveContact() throws Exception{
        ContactEntity entity = new ContactEntity();
        entity.setFirstName("TEST");
        entity.setLastName("TEST");
        ContactEntity.DAO.persist(entity);
        assertNotNull(entity.getId());
        ContactEntity.DAO.delete(entity);
    }

}