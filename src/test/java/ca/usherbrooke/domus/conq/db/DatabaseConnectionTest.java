package ca.usherbrooke.domus.conq.db;

import lombok.extern.java.Log;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Imad
 */
@Log
public class DatabaseConnectionTest {

    private SessionFactory factory;

    @Before
    public void setUp() {
        factory = DatabaseConnection.getConnection(DatabaseConfig.MODEL);
    }

    @Test
    public void getConnectionIsNotNull() {
        assertNotEquals("Session Factory is NULL", null,factory);
    }

    @After
    public void tearDown() {
        log.info("Finished");
    }

}
