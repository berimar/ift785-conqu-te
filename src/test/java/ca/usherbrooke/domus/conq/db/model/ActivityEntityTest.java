package ca.usherbrooke.domus.conq.db.model;

import ca.usherbrooke.domus.conq.uapps.jfxtagenda.JFXActivity;
import lombok.extern.java.Log;
import org.junit.Test;

@Log
public class ActivityEntityTest {

    @Test
    public void testGetAll(){
        ActivityEntity.DAO.getAll().forEach((activity)->log.info(activity.toString()));
    }

    @Test
    public void testInsert(){
        ActivityEntity activityEntity = ActivityEntity.bind(JFXActivity.empty());
        ActivityEntity.DAO.save(activityEntity);
    }

}